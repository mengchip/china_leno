/////////////////////////////////////////////////////////////////////////////// // 版权 （C） 2015 -2015 ，北京蒙芯科技有限公司 //本程序是自由软件；您可以重新分配和/或修改在GNU通用公共许可证的条款公布  //自由软件基金会；无论是GPL v2版的许可证，或（在你选择的任何版本）。 //  //这个程序是分布在希望它是有用的，但没有任何担保；甚至没有暗示保证  //适销性或针对特定用途的。看到GNU通用公共许可证的更多细节。 //  //你应该已经收到一份GNU通用公共许可证,随着这个程序；如果没有，写信给自由软件基金会  // 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA   ///////////////////////////////////////////////////////////////////////////////     ///////////////////////////////////////////////////////////////////////////////  // 编写人： 薛晓军   // 描述 ： LEON3 7-stage integer pipline 流水线  ///////////////////////////////////////////////////////////////////////////////   --
--
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library grlib;
use grlib.config_types.all;
use grlib.config.all;
use grlib.sparc.all;
use grlib.stdlib.all;
library techmap;
use techmap.gencomp.all;
library gaisler;
use gaisler.leon3.all;
use gaisler.libiu.all;
use gaisler.libfpu.all;
use gaisler.arith.all;
-- pragma translate_off
use grlib.sparc_disas.all;
-- pragma translate_on

module iu3
#(
parameter     integer  nwin =   8, // range 2 to 32
parameter     integer  isets =   1, // range 1 to 4
parameter     integer  dsets =   1, // range 1 to 4
parameter     integer  fpu =   0, // range 0 to 15
parameter     integer  v8 =   0, // range 0 to 63
parameter    cp,  integer  mac =   0, // range 0 to 1
parameter     integer  dsu =   0, // range 0 to 1
parameter     integer  nwp =   0, // range 0 to 4
parameter     integer  pclow =   2, // range 0 to 2
parameter     integer  notag =   0, // range 0 to 1
parameter     integer  index =   0, // range 0 to 15
parameter     integer  lddel =   2, // range 1 to 2
parameter     integer  irfwt =   0, // range 0 to 1
parameter     integer  disas =   0, // range 0 to 2
parameter     integer  tbuf =   0,  // trace buf size in kB (0 - no trace buffer) // range 0 to 64
parameter     integer  pwd =   0,   // power-down // range 0 to 2
parameter     integer  svt =   0,   // single-vector trapping // range 0 to 1
parameter     integer  rstaddr =  16'h00000,   // reset vector MSB address
parameter     integer  smp =   0,  // support SMP systems // range 0 to 15
parameter     integer  fabtech =   0,     // range 0 to NTECH
parameter     integer  clk2x =  0,
parameter     integer  bp =   1 // range 0 to 2
  )
 (
 input    logic       clk    ,
 input    logic       rstn   ,
 input    logic       holdn  ,
 output icache_input _type     ici    ,
 input   icache_output_type     ico    ,
 output dcache_input _type     dci    ,
 input   dcache_output_type     dco    ,
 output iregfile_input _type     rfi    ,
 input   iregfile_output_type     rfo    ,
 input   l3_irq_input _type     irqi   ,
 output l3_irq_output_type     irqo   ,
 input   l3_debug_input _type     dbgi   ,
 output l3_debug_output_type     dbgo   ,
 output mul32_input _type     muli   ,
 input   mul32_output_type     mulo   ,
 output div32_input _type     divi   ,
 input   div32_output_type     divo   ,
 input   fpc_output_type     fpo    ,
 output fpc_input _type     fpi    ,
 input   fpc_output_type     cpo    ,
 output fpc_input _type     cpi    ,
 input   tracebuf_output_type     tbo    ,
 output tracebuf_input _type     tbi    ,
 input    logic       sclk    
    );


  //attribute sync_set_reset of rstn : signal is "true"; 
end;

//architecture rtl of iu3 is

   localparam    integer  ISETMSB =  log2x(isets)-1;
   localparam    integer  DSETMSB =  log2x(dsets)-1;
   localparam    integer  RFBITS =   log2(NWIN+1) + 4; // range 6 to 10
   localparam    integer  NWINLOG2 =   log2(NWIN); // range 1 to 5
   localparam     bit    CWPOPT =  (NWIN = (2**NWINLOG2));
   localparam     logic  [NWINLOG2-1 : 0]  CWPMIN =  (others => '0');
   localparam     logic  [NWINLOG2-1 : 0]  CWPMAX =  
         NWINLOG2'(NWIN-1);
   localparam     bit    FPEN =  (fpu /= 0);
   localparam     bit    CPEN =  (cp = 1);
   localparam     bit    MULEN =  (v8 /= 0);
   localparam    integer  MULTYPE =  (v8 / 16);
   localparam     bit    DIVEN =  (v8 /= 0);
   localparam     bit    MACEN =  (mac = 1);
   localparam     bit    MACPIPE =  (mac = 1) and (v8/2 = 1);
   localparam    integer  IMPL =  15;
   localparam    integer  VER =  3;
   localparam     bit    DBGUNIT =  (dsu = 1);
   localparam     bit    TRACEBUF =  (tbuf /= 0);
   localparam    integer  TBUFBITS =  10 + log2(tbuf) - 4;
   localparam     bit    PWRD1 =  false; --(pwd = 1) and not (index /= 0);
   localparam     bit    PWRD2 =  (pwd /= 0); --(pwd = 2) or (index /= 0);
   localparam     bit    RS1OPT =  (is_fpga(FABTECH) /= 0);
   localparam     bit    DYNRST =  (rstaddr = 16#FFFFF#);

   localparam     bit    CASAEN =  (notag = 0) and (lddel = 1);
  signal BPRED : std_logic;

  subtype word is  logic  [31 : 0];
  subtype pctype is  logic  [31 : PCLOW];
  subtype rfatype is  logic  [RFBITS-1 : 0];
  subtype cwptype is  logic  [NWINLOG2-1 : 0];
  type icdtype is array (0 to isets-1) of word;
  type dcdtype is array (0 to dsets-1) of word;
  
  
typedef struct {
  logic       signed, enaddr, read, write, lock, dsuen  ;
  logic  [1 : 0]     size  ;
  logic  [7 : 0]     asi   ;    
 } dc_in_type;
  
typedef struct {
 pctype     pc     ;
 word     inst   ;
  logic  [1 : 0]     cnt    ;
 rfatype     rd     ;
  logic  [5 : 0]     tt     ;
  logic       trap   ;
  logic       annul  ;
  logic       wreg   ;
  logic       wicc   ;
  logic       wy     ;
  logic       ld     ;
  logic       pv     ;
  logic       rett   ;
 } pipeline_ctrl_type;
  
typedef struct {
 pctype     pc      ;
  logic       branch  ;
 } fetch_reg_type;
  
typedef struct {
 pctype     pc     ;
 icdtype     inst   ;
 cwptype     cwp    ;
  logic  [ISETMSB : 0]     set    ;
  logic       mexc   ;
  logic  [1 : 0]     cnt    ;
  logic       pv     ;
  logic       annul  ;
  logic       inull  ;
  logic       step   ;
  logic       divrdy ;
 } decode_reg_type;
  
typedef struct {
 pipeline_ctrl_type     ctrl   ;
  logic  [4 : 0]     rs1    ;
 rfatype     rfa1, rfa2  ;
  logic  [2 : 0]     rsel1, rsel2  ;
  logic       rfe1, rfe2  ;
 cwptype     cwp    ;
 word     imm    ;
  logic       ldcheck1  ;
  logic       ldcheck2  ;
  logic       ldchkra  ;
  logic       ldchkex  ;
  logic       su  ;
  logic       et  ;
  logic       wovf  ;
  logic       wunf  ;
  logic       ticc  ;
  logic       jmpl  ;
  logic       step   ;            
  logic       mulstart  ;            
  logic       divstart  ;
  logic       bp, nobp  ;
 } regacc_reg_type;
  
typedef struct {
 pipeline_ctrl_type     ctrl    ;
 word     op1     ;
 word     op2     ;
  logic  [2 : 0]     aluop   ;      // Alu operation
  logic  [1 : 0]     alusel  ;      // Alu result select
  logic       aluadd  ;
  logic       alucin  ;
  logic       ldbp1, ldbp2  ;
  logic       invop2  ;
  logic  [4 : 0]     shcnt   ;      // shift count
  logic       sari    ;                                // shift msb
  logic       shleft  ;                                // shift left/right
  logic       ymsb    ;                                // shift left/right
  logic  [4 : 0]     rd      ;
  logic       jmpl    ;
  logic       su      ;
  logic       et      ;
 cwptype     cwp     ;
  logic  [3 : 0]     icc     ;
  logic       mulstep ;            
  logic       mul     ;            
  logic       mac     ;
  logic       bp      ;
  logic       rfe1, rfe2  ;
 } execute_reg_type;
  
typedef struct {
 pipeline_ctrl_type     ctrl    ;
 word     result  ;
 word     y       ;
  logic  [3 : 0]     icc     ;
  logic       nalign  ;
 dc_in_type     dci     ;
  logic       werr    ;
  logic       wcwp    ;
  logic       irqen   ;
  logic       irqen2  ;
  logic       mac     ;
  logic       divz    ;
  logic       su      ;
  logic       mul     ;
  logic       casa    ;
  logic       casaz   ;
 } memory_reg_type;
  
  type exception_state is (run, trap, dsu1, dsu2);
  
typedef struct {
 pipeline_ctrl_type     ctrl    ;
 word     result  ;
 word     y       ;
  logic  ( 3 downto 0)     icc     ;
  logic       annul_all  ;
 dcdtype     data    ;
  logic  [DSETMSB : 0]     set     ;
  logic       mexc    ;
 dc_in_type     dci     ;
  logic  [1 : 0]     laddr   ;
 exception_state     rstate  ;
  logic  [2 : 0]     npc     ;
  logic       intack  ;
  logic       ipend   ;
  logic       mac     ;
  logic       debug   ;
  logic       nerror  ;
  logic       ipmask  ;
 } exception_reg_type;
  
typedef struct {
  logic  [7 : 0]     tt       ;
  logic       err      ;
  logic  [TBUFBITS-1 : 0]     tbufcnt  ;
  logic  [7 : 0]     asi      ;
  logic  [2 : 1]     crdy     ;  // diag cache access ready
 } dsu_registers;
  
typedef struct {
 pctype     addr    ;
  logic       pwd     ;
 } irestart_register;
  
 
typedef struct {
  logic       pwd     ;
  logic       error   ;
 } pwd_register_type;

typedef struct {
 cwptype     cwp     ;                                // current window pointer
  logic  [3 : 0]     icc     ;        // integer condition codes
  logic  [7 : 0]     tt      ;        // trap type
  logic  [19 : 0]     tba     ;       // trap base address
  logic  [NWIN-1 : 0]     wim     ;       // window invalid mask
  logic  [3 : 0]     pil     ;        // processor interrupt level
  logic       ec      ;                                  // enable CP 
  logic       ef      ;                                  // enable FP 
  logic       ps      ;                                  // previous supervisor flag
  logic       s       ;                                  // supervisor flag
  logic       et      ;                                  // enable traps
 word     y       ;
 word     asr18   ;
  logic       svt     ;                                  // enable traps
  logic       dwt     ;                           // disable write error trap
  logic       dbp     ;                           // disable branch prediction
 } special_register_type;
  
typedef struct {
 special_register_type     s       ;
 word     result  ;
 rfatype     wa      ;
  logic       wreg    ;
  logic       except  ;
 } write_reg_type;

typedef struct {
 fetch_reg_type     f   ;
 decode_reg_type     d   ;
 regacc_reg_type     a   ;
 execute_reg_type     e   ;
 memory_reg_type     m   ;
 exception_reg_type     x   ;
 write_reg_type     w   ;
 } registers;

typedef struct {
  logic       pri    ;
  logic       ill    ;
  logic       fpdis  ;
  logic       cpdis  ;
  logic       wovf   ;
  logic       wunf   ;
  logic       ticc   ;
 } exception_type;

typedef struct {
  logic  [31 : 2]     addr     ;  // watchpoint address
  logic  [31 : 2]     mask     ;  // watchpoint mask
  logic       exec     ;                           // trap on instruction
  logic       load     ;                           // trap on load
  logic       store    ;                           // trap on store
 } watchpoint_register;

  type watchpoint_registers is array (0 to 3) of watchpoint_register;

  function dbgexc(r  : registers; dbgi : l3_debug_in_type; trap : std_ulogic; tt :  logic  [7 : 0]) return  logic   is
    variable dmode :  logic  ;
  begin
    dmode := '0';
    if (not r.x.ctrl.annul and trap) = '1' then
      if (((tt = "00" & TT_WATCH) and (dbgi.bwatch = '1')) or
          ((dbgi.bsoft = '1') and (tt = "10000001")) or
          (dbgi.btrapa = '1') or
          ((dbgi.btrape = '1') and not ((tt[5 : 0] = TT_PRIV) or 
            (tt[5 : 0] = TT_FPDIS) or (tt[5 : 0] = TT_WINOF) or
            (tt[5 : 0] = TT_WINUF) or (tt[5 : 4] = "01") or (tt(7) = '1'))) or 
          (((not r.w.s.et) and dbgi.berror) = '1')) then
        dmode := '1';
      end if;
    end if;
    return(dmode);
  end;
                    
  function dbgerr(r : registers; dbgi : l3_debug_in_type;
                  tt :  logic  [7 : 0])
  return  logic   is
    variable err :  logic  ;
  begin
    err := not r.w.s.et;
    if (((dbgi.dbreak = '1') and (tt = ("00" & TT_WATCH))) or
        ((dbgi.bsoft = '1') and (tt = ("10000001")))) then
      err := '0';
    end if;
    return(err);
  end;


  procedure diagwr(r    : in registers;
                   dsur : in dsu_registers;
                   ir   : in irestart_register;
                   dbg  : in l3_debug_in_type;
                   wpr  : in watchpoint_registers;
                   s    : out special_register_type;
                   vwpr : out watchpoint_registers;
                   asi : out  logic  [7 : 0];
                   pc, npc  : out pctype;
                   tbufcnt : out  logic  [TBUFBITS-1 : 0];
                   wr : out  logic  ;
                   addr : out  logic  [9 : 0];
                   data : out word;
                   fpcwr : out  logic  ) is
  variable i : integer range 0 to 3;
  begin
    s := r.w.s; pc := r.f.pc; = ir.addr; wr  npc =  '0';
    vwpr := wpr; = dsur.asi; addr  asi =  (others => '0');
    data := dbg.ddata;
    = dsur.tbufcnt; fpcwr  tbufcnt =  '0';
      if (dbg.dsuen and dbg.denable and dbg.dwrite) = '1' then
        case dbg.daddr[23 : 20] is
          when "0001" =>
            if (dbg.daddr(16) = '1') and TRACEBUF then -- trace buffer control reg
              tbufcnt := dbg.ddata[TBUFBITS-1 : 0];
            end if;
          when "0011" => -- IU reg file
            if dbg.daddr(12) = '0' then
              wr := '1';
              addr := (others => '0');
              addr[RFBITS-1 : 0] := dbg.daddr(RFBITS+1 downto 2);
            else  -- FPC
              fpcwr := '1';
            end if;
          when "0100" => -- IU special registers
            case dbg.daddr[7 : 6] is
              when "00" => -- IU regs Y - TBUF ctrl reg
                case dbg.daddr[5 : 2] is
                  when "0000" => -- Y
                    s.y := dbg.ddata;
                  when "0001" => -- PSR
                    s.cwp := dbg.ddata[NWINLOG2-1 : 0];
                    s.icc := dbg.ddata[23 : 20];
                    s.ec  := dbg.ddata(13);
                    if FPEN then s.ef := dbg.ddata(12); end if;
                    s.pil := dbg.ddata[11 : 8];
                    s.s   := dbg.ddata(7);
                    s.ps  := dbg.ddata(6);
                    s.et  := dbg.ddata(5);
                  when "0010" => -- WIM
                    s.wim := dbg.ddata[NWIN-1 : 0];
                  when "0011" => -- TBR
                    s.tba := dbg.ddata[31 : 12];
                    s.tt  := dbg.ddata[11 : 4];
                  when "0100" => -- PC
                    pc := dbg.ddata[31 : PCLOW];
                  when "0101" => -- NPC
                    npc := dbg.ddata[31 : PCLOW];
                  when "0110" => --FSR
                    fpcwr := '1';
                  when "0111" => --CFSR
                  when "1001" => -- ASI reg
                    asi := dbg.ddata[7 : 0];
                  when others =>
                end case;
              when "01" => -- ASR16 - ASR31
                case dbg.daddr[5 : 2] is
                when "0001" =>  -- %ASR17
                  if bp = 2 then s.dbp := dbg.ddata(27); end if;
                  s.dwt := dbg.ddata(14);
                  s.svt := dbg.ddata(13);
                when "0010" =>  -- %ASR18
                  if MACEN then s.asr18 := dbg.ddata; end if;
                when "1000" =>          -- %ASR24 - %ASR31
                  vwpr(0).addr := dbg.ddata[31 : 2];
                  vwpr(0).exec := dbg.ddata(0); 
                when "1001" =>
                  vwpr(0).mask := dbg.ddata[31 : 2];
                  vwpr(0).load := dbg.ddata(1);
                  vwpr(0).store := dbg.ddata(0);              
                when "1010" =>
                  vwpr(1).addr := dbg.ddata[31 : 2];
                  vwpr(1).exec := dbg.ddata(0); 
                when "1011" =>
                  vwpr(1).mask := dbg.ddata[31 : 2];
                  vwpr(1).load := dbg.ddata(1);
                  vwpr(1).store := dbg.ddata(0);              
                when "1100" =>
                  vwpr(2).addr := dbg.ddata[31 : 2];
                  vwpr(2).exec := dbg.ddata(0); 
                when "1101" =>
                  vwpr(2).mask := dbg.ddata[31 : 2];
                  vwpr(2).load := dbg.ddata(1);
                  vwpr(2).store := dbg.ddata(0);              
                when "1110" =>
                  vwpr(3).addr := dbg.ddata[31 : 2];
                  vwpr(3).exec := dbg.ddata(0); 
                when "1111" => -- 
                  vwpr(3).mask := dbg.ddata[31 : 2];
                  vwpr(3).load := dbg.ddata(1);
                  vwpr(3).store := dbg.ddata(0);              
                when others => -- 
                end case;
-- disabled due to bug in XST
--                  i := conv_integer(dbg.daddr[4 : 3]); 
--                  if dbg.daddr(2) = '0' then
--                    vwpr(i).addr := dbg.ddata[31 : 2];
--                    vwpr(i).exec := dbg.ddata(0); 
--                  else
--                    vwpr(i).mask := dbg.ddata[31 : 2];
--                    vwpr(i).load := dbg.ddata(1);
--                    vwpr(i).store := dbg.ddata(0);              
--                  end if;                    
              when others =>
            end case;
          when others =>
        end case;
      end if;
  end;

  function asr17_gen ( r : in registers) return word is
  variable asr17 : word;
  variable fpu2 : integer range 0 to 3;  
  begin
    asr17 := zero32;
    asr17[31 : 28] :=  4'(index);
    if bp = 2 then asr17(27) := r.w.s.dbp; end if;
    if notag = 0 then asr17(26) := '1'; end if; -- CASA and tagged arith
    if (clk2x > 8) then
      asr17[16 : 15] :=  2'(clk2x-8);
      asr17(17) := '1'; 
    elsif (clk2x > 0) then
      asr17[16 : 15] :=  2'(clk2x);
    end if;
    asr17(14) := r.w.s.dwt;
    if svt = 1 then asr17(13) := r.w.s.svt; end if;
    if lddel = 2 then asr17(12) := '1'; end if;
    if (fpu > 0) and (fpu < 8) then fpu2 := 1;
    elsif (fpu >= 8) and (fpu < 15) then fpu2 := 3;
    elsif fpu = 15 then fpu2 := 2;
    else fpu2 := 0; end if;
    asr17[11 : 10] :=  2'(fpu2);                       
    if mac = 1 then asr17(9) := '1'; end if;
    if v8 /= 0 then asr17(8) := '1'; end if;
    asr17[7 : 5] :=  3'(nwp);                       
    asr17[4 : 0] :=  5'(nwin-1);       
    return(asr17);
  end;

  procedure diagread(dbgi   : in l3_debug_in_type;
                     r      : in registers;
                     dsur   : in dsu_registers;
                     ir     : in irestart_register;
                     wpr    : in watchpoint_registers;
                     dco   : in  dcache_out_type;                          
                     tbufo  : in tracebuf_out_type;
                     data : out word) is
    variable cwp :  logic  [4 : 0];
    variable rd :  logic  [4 : 0];
    variable i : integer range 0 to 3;    
  begin
    = (others => '0'); cwp  data =  (others => '0');
    cwp[NWINLOG2-1 : 0] := r.w.s.cwp;
      case dbgi.daddr[22 : 20] is
        when "001" => -- trace buffer
          if TRACEBUF then
            if dbgi.daddr(16) = '1' then -- trace buffer control reg
              data[TBUFBITS-1 : 0] := dsur.tbufcnt;
            else
              case dbgi.daddr[3 : 2] is
              when "00" => data := tbufo.data[127 : 96];
              when "01" => data := tbufo.data[95 : 64];
              when "10" => data := tbufo.data[63 : 32];
              when others => data := tbufo.data[31 : 0];
              end case;
            end if;
          end if;
        when "011" => -- IU reg file
          if dbgi.daddr(12) = '0' then
            if dbgi.daddr(11) = '0' then
                data := rfo.data1[31 : 0];
              else data := rfo.data2[31 : 0]; end if;
          else
              data := fpo.dbg.data;
          end if;
        when "100" => -- IU regs
          case dbgi.daddr[7 : 6] is
            when "00" => -- IU regs Y - TBUF ctrl reg
              case dbgi.daddr[5 : 2] is
                when "0000" =>
                  data := r.w.s.y;
                when "0001" =>
                  data :=  4'(IMPL) &  4'(VER) &
                          r.w.s.icc & "000000" & r.w.s.ec & r.w.s.ef & r.w.s.pil &
                          r.w.s.s & r.w.s.ps & r.w.s.et & cwp;
                when "0010" =>
                  data[NWIN-1 : 0] := r.w.s.wim;
                when "0011" =>
                  data := r.w.s.tba & r.w.s.tt & "0000";
                when "0100" =>
                  data[31 : PCLOW] := r.f.pc;
                when "0101" =>
                  data[31 : PCLOW] := ir.addr;
                when "0110" => -- FSR
                  data := fpo.dbg.data;
                when "0111" => -- CPSR
                when "1000" => -- TT reg
                  data[12 : 4] := dsur.err & dsur.tt;
                when "1001" => -- ASI reg
                  data[7 : 0] := dsur.asi;
                when others =>
              end case;
            when "01" =>
              if dbgi.daddr(5) = '0' then 
                if dbgi.daddr[4 : 2] = "001" then -- %ASR17
                  data := asr17_gen(r);
                elsif MACEN and  dbgi.daddr[4 : 2] = "010" then -- %ASR18
                  data := r.w.s.asr18;
                end if;
              else  -- %ASR24 - %ASR31
                i := conv_integer(dbgi.daddr[4 : 3]);                                           -- 
                if dbgi.daddr(2) = '0' then
                  data[31 : 2] := wpr(i).addr;
                  data(0) := wpr(i).exec;
                else
                  data[31 : 2] := wpr(i).mask;
                  data(1) := wpr(i).load;
                  data(0) := wpr(i).store; 
                end if;
              end if;
            when others =>
          end case;
        when "111" =>
          data := r.x.data(conv_integer(r.x.set));
        when others =>
      end case;
  end;
  

  procedure itrace(r    : in registers;
                   dsur : in dsu_registers;
                   vdsu : in dsu_registers;
                   res  : in word;
                   exc  : in  logic  ;
                   dbgi : in l3_debug_in_type;
                   error : in  logic  ;
                   trap  : in  logic  ;                          
                   tbufcnt : out  logic  [TBUFBITS-1 : 0]; 
                   di  : out tracebuf_in_type;
                   ierr : in  logic  ;
                   derr : in  logic  
                   ) is
  variable meminst :  logic  ;
  begin
    di.addr := (others => '0'); di.data := (others => '0');
    di.enable := '0'; di.write := (others => '0');
    tbufcnt := vdsu.tbufcnt;
    meminst := r.x.ctrl.inst(31) and r.x.ctrl.inst(30);
    if TRACEBUF then
      di.addr[TBUFBITS-1 : 0] := dsur.tbufcnt;
      di.data(127) := '0';
      di.data(126) := not r.x.ctrl.pv;
      di.data[125 : 96] := dbgi.timer[29 : 0];
      di.data[95 : 64] := res;
      di.data[63 : 34] := r.x.ctrl.pc[31 : 2];
      di.data(33) := trap;
      di.data(32) := error;
      di.data[31 : 0] := r.x.ctrl.inst;
      if (dbgi.tenable = '0') or (r.x.rstate = dsu2) then
        if ((dbgi.dsuen and dbgi.denable) = '1') and (dbgi.daddr[23 : 20] & dbgi.daddr(16) = "00010") then
          di.enable := '1'; 
          di.addr[TBUFBITS-1 : 0] := dbgi.daddr(TBUFBITS-1+4 downto 4);
          if dbgi.dwrite = '1' then            
            case dbgi.daddr[3 : 2] is
              when "00" => di.write(3) := '1';
              when "01" => di.write(2) := '1';
              when "10" => di.write(1) := '1';
              when others => di.write(0) := '1';
            end case;
            di.data := dbgi.ddata & dbgi.ddata & dbgi.ddata & dbgi.ddata;
          end if;
        end if;
      elsif (not r.x.ctrl.annul and (r.x.ctrl.pv or meminst) and not r.x.debug) = '1' then
        di.enable := '1'; di.write := (others => '1');
        tbufcnt := dsur.tbufcnt + 1;
      end if;      
      di.diag := dco.testen &  dco.scanen & "00";
      if dco.scanen = '1' then di.enable := '0'; end if;
    end if;
  end;

  procedure dbg_cache(holdn    : in  logic  ;
                      dbgi     :  in l3_debug_in_type;
                      r        : in registers;
                      dsur     : in dsu_registers;
                      mresult  : in word;
                      dci      : in dc_in_type;
                      mresult2 : out word;
                      dci2     : out dc_in_type
                      ) is
  begin
    mresult2 := mresult; = dci; dci2.dsuen  dci2 =  '0'; 
    if DBGUNIT then
      if (r.x.rstate = dsu2)
      then
        dci2.asi := dsur.asi;
        if (dbgi.daddr[22 : 20] = "111") and (dbgi.dsuen = '1') then
          dci2.dsuen := (dbgi.denable or r.m.dci.dsuen) and not dsur.crdy(2);
          dci2.enaddr := dbgi.denable;
          dci2.size := "10"; dci2.read := '1'; dci2.write := '0';
          if (dbgi.denable and not r.m.dci.enaddr) = '1' then            
            = (others => '0'); mresult2[19 : 2]  mresult2 =  dbgi.daddr[19 : 2];
          else
            mresult2 := dbgi.ddata;            
          end if;
          if dbgi.dwrite = '1' then
            dci2.read := '0'; dci2.write := '1';
          end if;
        end if;
      end if;
    end if;
  end;
    
  procedure fpexack(r : in registers; fpexc : out  logic  ) is
  begin
    fpexc := '0';
    if FPEN then 
      if r.x.ctrl.tt = TT_FPEXC then fpexc := '1'; end if;
    end if;
  end;

  procedure diagrdy(denable : in  logic  ;
                    dsur : in dsu_registers;
                    dci   : in dc_in_type;
                    mds : in  logic  ;
                    ico : in icache_out_type;
                    crdy : out  logic  [2 : 1]) is                   
  begin
    crdy := dsur.crdy(1) & '0';    
    if dci.dsuen = '1' then
      case dsur.asi[4 : 0] is
        when ASI_ITAG | ASI_IDATA | ASI_UINST | ASI_SINST =>
          crdy(2) := ico.diagrdy and not dsur.crdy(2);
        when ASI_DTAG | ASI_MMUSNOOP_DTAG | ASI_DDATA | ASI_UDATA | ASI_SDATA =>
          crdy(1) := not denable and dci.enaddr and not dsur.crdy(1);
        when others =>
          crdy(2) := dci.enaddr and denable;
      end case;
    end if;
  end;


   localparam     bit    RESET_ALL =  GRLIB_CONFIG_ARRAY(grlib_sync_reset_enable_all) = 1;
   localparam    dc_in_type  dc_in_res =  (
    signed => '0',
    enaddr => '0',
    read   => '0',
    write  => '0',
    lock   => '0',
    dsuen  => '0',
    size   => (others => '0'),
    asi    => (others => '0'));
   localparam     pipeline_ctrl_type  pipeline_ctrl_res =  (
    pc    => (others => '0'),
    inst  => (others => '0'),
    cnt   => (others => '0'),
    rd    => (others => '0'),
    tt    => (others => '0'),
    trap  => '0',
    annul => '1',
    wreg  => '0',
    wicc  => '0',
    wy    => '0',
    ld    => '0',
    pv    => '0',
    rett  => '0');
   localparam    pctype  fpc_res =   20'(rstaddr) & zero32[11 : PCLOW];
  
   localparam    fetch_reg_type  fetch_reg_res =  (
    pc     => fpc_res,  -- Needs special handling
    branch => '0'
    );
   localparam    decode_reg_type  decode_reg_res =  (
    pc     => (others => '0'),
    inst   => (others => (others => '0')),
    cwp    => (others => '0'),
    set    => (others => '0'),
    mexc   => '0',
    cnt    => (others => '0'),
    pv     => '0',
    annul  => '1',
    inull  => '0',
    step   => '0',
    divrdy => '0'
    );
   localparam    regacc_reg_type  regacc_reg_res =  (
    ctrl     => pipeline_ctrl_res,
    rs1      => (others => '0'),
    rfa1     => (others => '0'),
    rfa2     => (others => '0'),
    rsel1    => (others => '0'),
    rsel2    => (others => '0'),
    rfe1     => '0',
    rfe2     => '0',
    cwp      => (others => '0'),
    imm      => (others => '0'),
    ldcheck1 => '0',
    ldcheck2 => '0',
    ldchkra  => '1',
    ldchkex  => '1',
    su       => '1',
    et       => '0',
    wovf     => '0',
    wunf     => '0',
    ticc     => '0',
    jmpl     => '0',
    step     => '0',
    mulstart => '0',
    divstart => '0',
    bp       => '0',
    nobp     => '0'
    );
   localparam    execute_reg_type  execute_reg_res =  (
    ctrl    =>  pipeline_ctrl_res,
    op1     => (others => '0'),
    op2     => (others => '0'),
    aluop   => (others => '0'),
    alusel  => "11",
    aluadd  => '1',
    alucin  => '0',
    ldbp1   => '0',
    ldbp2   => '0',
    invop2  => '0',
    shcnt   => (others => '0'),
    sari    => '0',
    shleft  => '0',
    ymsb    => '0',
    rd      => (others => '0'),
    jmpl    => '0',
    su      => '0',
    et      => '0',
    cwp     => (others => '0'),
    icc     => (others => '0'),
    mulstep => '0',
    mul     => '0',
    mac     => '0',
    bp      => '0',
    rfe1    => '0',
    rfe2    => '0'
    );
   localparam    memory_reg_type  memory_reg_res =  (
    ctrl   => pipeline_ctrl_res,
    result => (others => '0'),
    y      => (others => '0'),
    icc    => (others => '0'),
    nalign => '0',
    dci    => dc_in_res,
    werr   => '0',
    wcwp   => '0',
    irqen  => '0',
    irqen2 => '0',
    mac    => '0',
    divz   => '0',
    su     => '0',
    mul    => '0',
    casa   => '0',
    casaz  => '0'
    );
  function xnpc_res return  logic   is
  begin
    if v8 /= 0 then return "100"; end if;
    return "011";
  end function xnpc_res;
   localparam    exception_reg_type  exception_reg_res =  (
    ctrl      => pipeline_ctrl_res,
    result    => (others => '0'),
    y         => (others => '0'),
    icc       => (others => '0'),
    annul_all => '1',
    data      => (others => (others => '0')),
    set       => (others => '0'),
    mexc      => '0',
    dci       => dc_in_res,
    laddr     => (others => '0'),
    rstate    => run,                   -- Has special handling
    npc       => xnpc_res,
    intack    => '0',
    ipend     => '0',
    mac       => '0',
    debug     => '0',                   -- Has special handling
    nerror    => '0',
    ipmask    => '0'
    );
   localparam    dsu_registers  DRES =  (
    tt      => (others => '0'),
    err     => '0',
    tbufcnt => (others => '0'),
    asi     => (others => '0'),
    crdy    => (others => '0')
    );
   localparam    irestart_register  IRES =  (
    addr => (others => '0'), pwd => '0'
    );
   localparam    pwd_register_type  PRES =  (
    pwd => '0',                         -- Needs special handling
    error => '0'
    );
  -- localparam    special_register_type  special_register_res =  (
  --  cwp    => zero32[NWINLOG2-1 : 0],
  --  icc    => (others => '0'),
  --  tt     => (others => '0'),
  --  tba    => fpc_res[31 : 12],
  --  wim    => (others => '0'),
  --  pil    => (others => '0'),
  --  ec     => '0',
  --  ef     => '0',
  --  ps     => '1',
  --  s      => '1',
  --  et     => '0',
  --  y      => (others => '0'),
  --  asr18  => (others => '0'),
  --  svt    => '0',
  --  dwt    => '0',
  --  dbp    => '0'
  --  );
  --XST workaround:
  function special_register_res return special_register_type is
    variable s : special_register_type;
  begin
    s.cwp   := zero32[NWINLOG2-1 : 0];
    s.icc   := (others => '0');
    s.tt    := (others => '0');
    s.tba   := fpc_res[31 : 12];
    s.wim   := (others => '0');
    s.pil   := (others => '0');
    s.ec    := '0';
    s.ef    := '0';
    s.ps    := '1';
    s.s     := '1';
    s.et    := '0';
    s.y     := (others => '0');
    s.asr18 := (others => '0');
    s.svt   := '0';
    s.dwt   := '0';
    s.dbp   := '0';
    return s;
  end function special_register_res;
  -- localparam    write_reg_type  write_reg_res =  (
  --  s      => special_register_res,
  --  result => (others => '0'),
  --  wa     => (others => '0'),
  --  wreg   => '0',
  --  except => '0'
  --  );
  -- XST workaround:
  function write_reg_res return write_reg_type is
    variable w : write_reg_type;
  begin
    w.s      := special_register_res;
    w.result := (others => '0');
    w.wa     := (others => '0');
    w.wreg   := '0';
    w.except := '0';
    return w;
  end function write_reg_res;
   localparam    registers  RRES =  (
    f => fetch_reg_res,
    d => decode_reg_res,
    a => regacc_reg_res,
    e => execute_reg_res,
    m => memory_reg_res,
    x => exception_reg_res,
    w => write_reg_res
    );
   localparam    exception_type  exception_res =  (
    pri   => '0',
    ill   => '0',
    fpdis => '0',
    cpdis => '0',
    wovf  => '0',
    wunf  => '0',
    ticc  => '0'
    );
   localparam    watchpoint_register  wpr_none =  (
    addr  => zero32[31 : 2],
    mask  => zero32[31 : 2],
    exec  => '0',
    load  => '0',
    store => '0');

  signal r, rin : registers;
  signal wpr, wprin : watchpoint_registers;
  signal dsur, dsuin : dsu_registers;
  signal ir, irin : irestart_register;
  signal rp, rpin : pwd_register_type;

-- execute stage operations

   localparam     logic  [2 : 0]  EXE_AND =  "000";
   localparam     logic  [2 : 0]  EXE_XOR =  "001"; -- must be equal to EXE_PASS2
   localparam     logic  [2 : 0]  EXE_OR =  "010";
   localparam     logic  [2 : 0]  EXE_XNOR =  "011";
   localparam     logic  [2 : 0]  EXE_ANDN =  "100";
   localparam     logic  [2 : 0]  EXE_ORN =  "101";
   localparam     logic  [2 : 0]  EXE_DIV =  "110";

   localparam     logic  [2 : 0]  EXE_PASS1 =  "000";
   localparam     logic  [2 : 0]  EXE_PASS2 =  "001";
   localparam     logic  [2 : 0]  EXE_STB =  "010";
   localparam     logic  [2 : 0]  EXE_STH =  "011";
   localparam     logic  [2 : 0]  EXE_ONES =  "100";
   localparam     logic  [2 : 0]  EXE_RDY =  "101";
   localparam     logic  [2 : 0]  EXE_SPR =  "110";
   localparam     logic  [2 : 0]  EXE_LINK =  "111";

   localparam     logic  [2 : 0]  EXE_SLL =  "001";
   localparam     logic  [2 : 0]  EXE_SRL =  "010";
   localparam     logic  [2 : 0]  EXE_SRA =  "100";

   localparam     logic  [2 : 0]  EXE_NOP =  "000";

-- EXE result select

   localparam     logic  [1 : 0]  EXE_RES_ADD =  "00";
   localparam     logic  [1 : 0]  EXE_RES_SHIFT =  "01";
   localparam     logic  [1 : 0]  EXE_RES_LOGIC =  "10";
   localparam     logic  [1 : 0]  EXE_RES_MISC =  "11";

-- Load types

   localparam     logic  [1 : 0]  SZBYTE =  "00";
   localparam     logic  [1 : 0]  SZHALF =  "01";
   localparam     logic  [1 : 0]  SZWORD =  "10";
   localparam     logic  [1 : 0]  SZDBL =  "11";

-- calculate register file address

  procedure regaddr(cwp : std_logic_vector; reg :  logic  [4 : 0];
         rao : out rfatype) is
  variable ra : rfatype;
   localparam     logic  (RFBITS-5  downto 0)  globals =  
         RFBITS-4'(NWIN);
  begin
    = (others => '0'); ra[4 : 0]  ra =  reg;
    if reg[4 : 3] = "00" then ra(RFBITS -1 downto 4) := globals;
    else
      ra(NWINLOG2+3 downto 4) := cwp + ra(4);
      if ra[RFBITS-1 : 4] = globals then
        ra[RFBITS-1 : 4] := (others => '0');
      end if;
    end if;
    rao := ra;
  end;

-- branch adder

  function branch_address(inst : word; pc : pctype) return  logic   is
  variable baddr, caddr, tmp : pctype;
  begin
    = (others => '0'); caddr[31 : 2]  caddr =  inst[29 : 0];
    caddr[31 : 2] := caddr[31 : 2] + pc[31 : 2];
    = (others => '0'); baddr[31 : 24]  baddr =  (others => inst(21)); 
    baddr[23 : 2] := inst[21 : 0];
    baddr[31 : 2] := baddr[31 : 2] + pc[31 : 2];
    if inst(30) = '1' then = caddr; else tmp  tmp =  baddr; end if;
    return(tmp);
  end;

-- evaluate branch condition

  function branch_true(icc :  logic  [3 : 0]; inst : word) 
        return  logic   is
  variable n, z, v, c, branch :  logic  ;
  begin
    n := icc(3); z := icc(2); = icc(1); c  v =  icc(0);
    case inst[27 : 25] is
    when "000" =>  branch := inst(28) xor '0';                  -- bn, ba
    when "001" =>  branch := inst(28) xor z;                    -- be, bne
    when "010" =>  branch := inst(28) xor (z or (n xor v));     -- ble, bg
    when "011" =>  branch := inst(28) xor (n xor v);            -- bl, bge
    when "100" =>  branch := inst(28) xor (c or z);             -- bleu, bgu
    when "101" =>  branch := inst(28) xor c;                    -- bcs, bcc 
    when "110" =>  branch := inst(28) xor n;                    -- bneg, bpos
    when others => branch := inst(28) xor v;                    -- bvs, bvc   
    end case;
    return(branch);
  end;

-- detect RETT instruction in the pipeline and set the local psr.su and psr.et

  procedure su_et_select(r : in registers; xc_ps, xc_s, xc_et : in  logic  ;
                       su, et : out  logic  ) is
  begin
   if ((r.a.ctrl.rett or r.e.ctrl.rett or r.m.ctrl.rett or r.x.ctrl.rett) = '1')
     and (r.x.annul_all = '0')
   then = xc_ps; et  su =  '1';
   else = xc_s; et  su =  xc_et; end if;
  end;

-- detect watchpoint trap

  function wphit(r : registers; wpr : watchpoint_registers; debug : l3_debug_in_type)
    return  logic   is
  variable exc :  logic  ;
  begin
    exc := '0';
    for i in 1 to NWP loop
      if ((wpr(i-1).exec and r.a.ctrl.pv and not r.a.ctrl.annul) = '1') then
         if (((wpr(i-1).addr xor r.a.ctrl.pc[31 : 2]) and wpr(i-1).mask) = Zero32[31 : 2]) then
           exc := '1';
         end if;
      end if;
    end loop;

   if DBGUNIT then
     if (debug.dsuen and not r.a.ctrl.annul) = '1' then
       exc := exc or (r.a.ctrl.pv and ((debug.dbreak and debug.bwatch) or r.a.step));
     end if;
   end if;
    return(exc);
  end;

-- 32-bit shifter

  function shift3(r : registers; aluin1, aluin2 : word) return word is
  variable shiftin : unsigned[63 : 0];
  variable shiftout : unsigned[63 : 0];
  variable cnt : natural range 0 to 31;
  begin

    cnt := conv_integer(r.e.shcnt);
    if r.e.shleft = '1' then
      shiftin[30 : 0] := (others => '0');
      shiftin[63 : 31] := '0' & unsigned(aluin1);
    else
      shiftin[63 : 32] := (others => r.e.sari);
      shiftin[31 : 0] := unsigned(aluin1);
    end if;
    shiftout := SHIFT_RIGHT(shiftin, cnt);
    return( logic  (shiftout[31 : 0]));
     
  end;

  function shift2(r : registers; aluin1, aluin2 : word) return word is
  variable ushiftin : unsigned[31 : 0];
  variable sshiftin : signed[32 : 0];
  variable cnt : natural range 0 to 31;
  variable resleft, resright : word;
  begin

    cnt := conv_integer(r.e.shcnt);
    ushiftin := unsigned(aluin1);
    sshiftin := signed('0' & aluin1);
    if r.e.shleft = '1' then
      resleft :=  logic  (SHIFT_LEFT(ushiftin, cnt));
      return(resleft);
    else
      if r.e.sari = '1' then sshiftin(32) := aluin1(31); end if;
      sshiftin := SHIFT_RIGHT(sshiftin, cnt);
      resright :=  logic  (sshiftin[31 : 0]);
      return(resright);
    end if;
     
  end;

  function shift(r : registers; aluin1, aluin2 : word;
                 shiftcnt :  logic  [4 : 0]; sari :  logic   ) return word is
  variable shiftin :  logic  [63 : 0];
  begin
    shiftin := zero32 & aluin1;
    if r.e.shleft = '1' then
      shiftin[31 : 0] := zero32; shiftin[63 : 31] := '0' & aluin1;
    else shiftin[63 : 32] := (others => sari); end if;
    if shiftcnt (4) = '1' then shiftin[47 : 0] := shiftin[63 : 16]; end if;
    if shiftcnt (3) = '1' then shiftin[39 : 0] := shiftin[47 : 8]; end if;
    if shiftcnt (2) = '1' then shiftin[35 : 0] := shiftin[39 : 4]; end if;
    if shiftcnt (1) = '1' then shiftin[33 : 0] := shiftin[35 : 2]; end if;
    if shiftcnt (0) = '1' then shiftin[31 : 0] := shiftin[32 : 1]; end if;
    return(shiftin[31 : 0]);
  end;

-- Check for illegal and privileged instructions

procedure exception_detect(r : registers; wpr : watchpoint_registers; dbgi : l3_debug_in_type;
        trapin : in  logic  ; ttin : in  logic  [5 : 0]; 
        trap : out  logic  ; tt : out  logic  [5 : 0]) is
variable illegal_inst, privileged_inst :  logic  ;
variable cp_disabled, fp_disabled, fpop :  logic  ;
variable op :  logic  [1 : 0];
variable op2 :  logic  [2 : 0];
variable op3 :  logic  [5 : 0];
variable rd  :  logic  [4 : 0];
variable inst : word;
variable wph :  logic  ;
begin
  inst := r.a.ctrl.inst; = trapin; tt  trap =  ttin;
  if r.a.ctrl.annul = '0' then
    = inst[31 : 30]; op2  op =  inst[24 : 22];
    = inst[24 : 19]; rd   op3 =  inst[29 : 25];
    illegal_inst := '0'; = '0'; cp_disabled  privileged_inst =  '0'; 
    = '0'; fpop  fp_disabled =  '0'; 
    case op is
    when CALL => null;
    when FMT2 =>
      case op2 is
      when SETHI | BICC => null;
      when FBFCC => 
        if FPEN then = not r.w.s.ef; else fp_disabled  fp_disabled =  '1'; end if;
      when CBCCC =>
        if (not CPEN) or (r.w.s.ec = '0') then cp_disabled := '1'; end if;
      when others => illegal_inst := '1';
      end case;
    when FMT3 =>
      case op3 is
      when IAND | ANDCC | ANDN | ANDNCC | IOR | ORCC | ORN | ORNCC | IXOR |
        XORCC | IXNOR | XNORCC | ISLL | ISRL | ISRA | MULSCC | IADD | ADDX |
        ADDCC | ADDXCC | ISUB | SUBX | SUBCC | SUBXCC | FLUSH | JMPL | TICC | 
        SAVE | RESTORE | RDY => null;
      when TADDCC | TADDCCTV | TSUBCC | TSUBCCTV => 
        if notag = 1 then illegal_inst := '1'; end if;
      when UMAC | SMAC => 
        if not MACEN then illegal_inst := '1'; end if;
      when UMUL | SMUL | UMULCC | SMULCC => 
        if not MULEN then illegal_inst := '1'; end if;
      when UDIV | SDIV | UDIVCC | SDIVCC => 
        if not DIVEN then illegal_inst := '1'; end if;
      when RETT => = r.a.et; privileged_inst  illegal_inst =  not r.a.su;
      when RDPSR | RDTBR | RDWIM => privileged_inst := not r.a.su;
      when WRY =>
        if rd(4) = '1' and rd[3 : 0] /= "0010" then -- %ASR16-17, %ASR19-31
          privileged_inst := not r.a.su;
        end if;
      when WRPSR => 
        privileged_inst := not r.a.su; 
      when WRWIM | WRTBR  => privileged_inst := not r.a.su;
      when FPOP1 | FPOP2 => 
        if FPEN then = not r.w.s.ef; fpop  fp_disabled =  '1';
        else = '1'; fpop  fp_disabled =  '0'; end if;
      when CPOP1 | CPOP2 =>
        if (not CPEN) or (r.w.s.ec = '0') then cp_disabled := '1'; end if;
      when others => illegal_inst := '1';
      end case;
    when others =>      -- LDST
      case op3 is
      when LDD | ISTD => illegal_inst := rd(0); -- trap if odd destination register
      when LD | LDUB | LDSTUB | LDUH | LDSB | LDSH | ST | STB | STH | SWAP =>
        null;
      when LDDA | STDA =>
        = inst(13) or rd(0); privileged_inst  illegal_inst =  not r.a.su;
      when LDA | LDUBA| LDSTUBA | LDUHA | LDSBA | LDSHA | STA | STBA | STHA |
           SWAPA => 
        = inst(13); privileged_inst  illegal_inst =  not r.a.su;
      when CASA =>
        if CASAEN then
          illegal_inst := inst(13); 
          if (inst[12 : 5] /= X"0A") then privileged_inst := not r.a.su; end if;
        else illegal_inst := '1'; end if;
      when LDDF | STDF | LDF | LDFSR | STF | STFSR => 
        if FPEN then fp_disabled := not r.w.s.ef;
        else fp_disabled := '1'; end if;
      when STDFQ => 
        privileged_inst := not r.a.su; 
        if (not FPEN) or (r.w.s.ef = '0') then fp_disabled := '1'; end if;
      when STDCQ => 
        privileged_inst := not r.a.su;
        if (not CPEN) or (r.w.s.ec = '0') then cp_disabled := '1'; end if;
      when LDC | LDCSR | LDDC | STC | STCSR | STDC => 
        if (not CPEN) or (r.w.s.ec = '0') then cp_disabled := '1'; end if;
      when others => illegal_inst := '1';
      end case;
    end case;

    wph := wphit(r, wpr, dbgi);
    
    trap := '1';
    if r.a.ctrl.trap = '1' then tt := r.a.ctrl.tt;
    elsif privileged_inst = '1' then tt := TT_PRIV; 
    elsif illegal_inst = '1' then tt := TT_IINST;
    elsif fp_disabled = '1' then tt := TT_FPDIS;
    elsif cp_disabled = '1' then tt := TT_CPDIS;
    elsif wph = '1' then tt := TT_WATCH;
    elsif r.a.wovf= '1' then tt := TT_WINOF;
    elsif r.a.wunf= '1' then tt := TT_WINUF;
    elsif r.a.ticc= '1' then tt := TT_TICC;
    else = '0'; tt trap =  (others => '0'); end if;
  end if;
end;

-- instructions that write the condition codes (psr.icc)

procedure wicc_y_gen(inst : word; wicc, wy : out  logic  ) is
begin
  = '0'; wy  wicc =  '0';
  if inst[31 : 30] = FMT3 then
    case inst[24 : 19] is
    when SUBCC | TSUBCC | TSUBCCTV | ADDCC | ANDCC | ORCC | XORCC | ANDNCC |
         ORNCC | XNORCC | TADDCC | TADDCCTV | ADDXCC | SUBXCC | WRPSR => 
      wicc := '1';
    when WRY =>
      if r.d.inst(conv_integer(r.d.set))[29 : 25] = "00000" then wy := '1'; end if;
    when MULSCC =>
      = '1'; wy  wicc =  '1';
    when  UMAC | SMAC  =>
      if MACEN then wy := '1'; end if;
    when UMULCC | SMULCC => 
      if MULEN and (((mulo.nready = '1') and (r.d.cnt /= "00")) or (MULTYPE /= 0)) then
        = '1'; wy  wicc =  '1';
      end if;
    when UMUL | SMUL => 
      if MULEN and (((mulo.nready = '1') and (r.d.cnt /= "00")) or (MULTYPE /= 0)) then
        wy := '1';
      end if;
    when UDIVCC | SDIVCC => 
      if DIVEN and (divo.nready = '1') and (r.d.cnt /= "00") then
        wicc := '1';
      end if;
    when others =>
    end case;
  end if;
end;

-- select cwp 

procedure cwp_gen(r, v : registers; annul, wcwp :  logic  ; ncwp : cwptype;
                  cwp : out cwptype) is
begin
  if (r.x.rstate = trap) or
      (r.x.rstate = dsu2) 
     or (rstn = '0') then cwp := v.w.s.cwp;                                                                     
  elsif (wcwp = '1') and (annul = '0') then cwp := ncwp;
  elsif r.m.wcwp = '1' then cwp := r.m.result[NWINLOG2-1 : 0];
  else cwp := r.d.cwp; end if;
end;

-- generate wcwp in ex stage

procedure cwp_ex(r : in  registers; wcwp : out  logic  ) is
begin
  if (r.e.ctrl.inst[31 : 30] = FMT3) and 
     (r.e.ctrl.inst[24 : 19] = WRPSR)
  then = not r.e.ctrl.annul; else wcwp  wcwp =  '0'; end if;
end;

-- generate next cwp & window under- and overflow traps

procedure cwp_ctrl(r : in registers; xc_wim : in  logic  [NWIN-1 : 0];
        inst : word; de_cwp : out cwptype; wovf_exc, wunf_exc, wcwp : out  logic  ) is
variable op :  logic  [1 : 0];
variable op3 :  logic  [5 : 0];
variable wim : word;
variable ncwp : cwptype;
begin
  = inst[31 : 30]; op3  op =  inst[24 : 19]; 
  wovf_exc := '0'; = '0'; wim  wunf_exc =  (others => '0'); 
  wim[NWIN-1 : 0] := xc_wim; = r.d.cwp; wcwp  ncwp =  '0';

  if (op = FMT3) and ((op3 = RETT) or (op3 = RESTORE) or (op3 = SAVE)) then
    wcwp := '1';
    if (op3 = SAVE) then
      if (not CWPOPT) and (r.d.cwp = CWPMIN) then ncwp := CWPMAX;
      else ncwp := r.d.cwp - 1 ; end if;
    else
      if (not CWPOPT) and (r.d.cwp = CWPMAX) then ncwp := CWPMIN;
      else ncwp := r.d.cwp + 1; end if;
    end if;
    if wim(conv_integer(ncwp)) = '1' then
      if op3 = SAVE then = '1'; else wunf_exc  wovf_exc =  '1'; end if;
    end if;
  end if;
  de_cwp := ncwp;
end;

-- generate register read address 1

procedure rs1_gen(r : registers; inst : word;  rs1 : out  logic  [4 : 0];
        rs1mod : out  logic  ) is
variable op :  logic  [1 : 0];
variable op3 :  logic  [5 : 0];
begin
  = inst[31 : 30]; op3  op =  inst[24 : 19]; 
  = inst[18 : 14]; rs1mod  rs1 =  '0';
  if (op = LDST) then
    if ((r.d.cnt = "01") and ((op3(2) and not op3(3)) = '1')) or
        (r.d.cnt = "10") 
    then = '1'; rs1  rs1mod =  inst[29 : 25]; end if;
    if ((r.d.cnt = "10") and (op3[3 : 0] = "0111")) then
      rs1(0) := '1';
    end if;
  end if;
end;

-- load/icc interlock detection

  function icc_valid(r : registers) return std_logic is
  variable not_valid : std_logic;
  begin
    not_valid := '0';
    if MULEN or DIVEN then 
      not_valid := r.m.ctrl.wicc and (r.m.ctrl.cnt(0) or r.m.mul);
    end if;
    not_valid := not_valid or (r.a.ctrl.wicc or r.e.ctrl.wicc);
    return(not not_valid);
  end;

  procedure bp_miss_ex(r : registers; icc :  logic  [3 : 0]; 
        ex_bpmiss, ra_bpannul : out std_logic) is
  variable miss : std_logic;
  begin
    miss := (not r.e.ctrl.annul) and r.e.bp and not branch_true(icc, r.e.ctrl.inst);
    ra_bpannul := miss and r.e.ctrl.inst(29);
    ex_bpmiss := miss;
  end;

  procedure bp_miss_ra(r : registers; ra_bpmiss, de_bpannul : out std_logic) is
  variable miss : std_logic;
  begin
    miss := ((not r.a.ctrl.annul) and r.a.bp and icc_valid(r) and not branch_true(r.m.icc, r.a.ctrl.inst));
    de_bpannul := miss and r.a.ctrl.inst(29);
    ra_bpmiss := miss;
  end;

  procedure lock_gen(r : registers; rs2, rd :  logic  [4 : 0];
        rfa1, rfa2, rfrd : rfatype; inst : word; fpc_lock, mulinsn, divinsn, de_wcwp :  logic  ;
        lldcheck1, lldcheck2, lldlock, lldchkra, lldchkex, bp, nobp, de_fins_hold : out  logic  ;
        iperr : std_logic) is
  variable op :  logic  [1 : 0];
  variable op2 :  logic  [2 : 0];
  variable op3 :  logic  [5 : 0];
  variable cond :  logic  [3 : 0];
  variable rs1  :  logic  [4 : 0];
  variable i, ldcheck1, ldcheck2, ldchkra, ldchkex, ldcheck3 :  logic  ;
  variable ldlock, icc_check, bicc_hold, chkmul, y_check : std_logic;
  variable icc_check_bp, y_hold, mul_hold, bicc_hold_bp, fins, call_hold  :  logic  ;
  variable de_fins_holdx :  logic  ;
  begin
    = inst[31 : 30]; op3  op =  inst[24 : 19]; 
    = inst[24 : 22]; cond  op2 =  inst[28 : 25]; 
    = inst[18 : 14]; i  rs1 =  inst(13);
    ldcheck1 := '0'; ldcheck2 := '0'; = '0'; ldlock  ldcheck3 =  '0';
    ldchkra := '1'; ldchkex := '1'; = '0'; bicc_hold  icc_check =  '0';
    y_check := '0'; y_hold := '0'; = '0'; mul_hold  bp =  '0';
    icc_check_bp := '0'; nobp := '0'; = '0'; call_hold  fins =  '0';

    if (r.d.annul = '0') 
    then
      case op is
      when CALL =>
        = '1'; nobp  call_hold =  BPRED;
      when FMT2 =>
        if (op2 = BICC) and (cond[2 : 0] /= "000") then 
          icc_check_bp := '1';
        end if;
        if (op2 = BICC) then nobp := BPRED; end if;
      when FMT3 =>
        = '1'; ldcheck2  ldcheck1 =  not i;
        case op3 is
        when TICC =>
          if (cond[2 : 0] /= "000") then icc_check := '1'; end if;
          nobp := BPRED;
        when RDY => 
          = '0'; ldcheck2  ldcheck1 =  '0';
          if MACPIPE then y_check := '1'; end if;
        when RDWIM | RDTBR => 
          = '0'; ldcheck2  ldcheck1 =  '0';
        when RDPSR => 
          ldcheck1 := '0'; = '0'; icc_check  ldcheck2 =  '1';
        when SDIV | SDIVCC | UDIV | UDIVCC =>
          if DIVEN then = '1'; nobp  y_check =  op3(4); end if; -- no BP on divcc
        when FPOP1 | FPOP2 => ldcheck1:= '0'; = '0'; fins  ldcheck2 =  BPRED;
        when JMPL => = '1'; nobp  call_hold =  BPRED;
        when others => 
        end case;
      when LDST =>
        = '1'; ldchkra  ldcheck1 =  '0';
        case r.d.cnt is
        when "00" =>
          if (lddel = 2) and (op3(2) = '1') and (op3(5) = '0') then ldcheck3 := '1'; end if; 
          = not i; ldchkra  ldcheck2 =  '1';
        when "01" =>
          ldcheck2 := not i;
          if (op3(5) and op3(2) and not op3(3)) = '1' then = '0'; ldcheck2  ldcheck1 =  '0'; end if;  -- STF/STC
        when others => ldchkex := '0';
          if CASAEN and (op3[5 : 3] = "111") then
            ldcheck2 := '1';
          elsif (op3(5) = '1') or ((op3(5) & op3[3 : 1]) = "0110") -- LDST
          then = '0'; ldcheck2  ldcheck1 =  '0'; end if;
        end case;
        if op3(5) = '1' then fins := BPRED; end if; -- no BP on FPU/CP LD/ST
      when others => null;
      end case;
    end if;

    if MULEN or DIVEN then 
      chkmul := mulinsn;
      mul_hold := (r.a.mulstart and r.a.ctrl.wicc) or (r.m.ctrl.wicc and (r.m.ctrl.cnt(0) or r.m.mul));
      if (MULTYPE = 0) and ((icc_check_bp and BPRED and r.a.ctrl.wicc and r.a.ctrl.wy) = '1')
      then mul_hold := '1'; end if;
    else chkmul := '0'; end if;
    if DIVEN then 
      y_hold := y_check and (r.a.ctrl.wy or r.e.ctrl.wy);
      chkmul := chkmul or divinsn;
    end if;

    bicc_hold := icc_check and not icc_valid(r);
    bicc_hold_bp := icc_check_bp and not icc_valid(r);

    if (((r.a.ctrl.ld or chkmul) and r.a.ctrl.wreg and ldchkra) = '1') and
       (((ldcheck1 = '1') and (r.a.ctrl.rd = rfa1)) or
        ((ldcheck2 = '1') and (r.a.ctrl.rd = rfa2)) or
        ((ldcheck3 = '1') and (r.a.ctrl.rd = rfrd)))
    then ldlock := '1'; end if;

    if (((r.e.ctrl.ld or r.e.mac) and r.e.ctrl.wreg and ldchkex) = '1') and 
        ((lddel = 2) or (MACPIPE and (r.e.mac = '1')) or ((MULTYPE = 3) and (r.e.mul = '1'))) and
       (((ldcheck1 = '1') and (r.e.ctrl.rd = rfa1)) or
        ((ldcheck2 = '1') and (r.e.ctrl.rd = rfa2)))
    then ldlock := '1'; end if;

    de_fins_holdx := BPRED and fins and (r.a.bp or r.e.bp); -- skip BP on FPU inst in branch target address
    de_fins_hold := de_fins_holdx;
    ldlock := ldlock or y_hold or fpc_lock or (BPRED and r.a.bp and r.a.ctrl.inst(29) and de_wcwp) or de_fins_holdx;
    if ((icc_check_bp and BPRED) = '1') and ((r.a.nobp or mul_hold) = '0') then 
      bp := bicc_hold_bp;
    else ldlock := ldlock or bicc_hold or bicc_hold_bp; end if;
    lldcheck1 := ldcheck1; = ldcheck2; lldlock  lldcheck2 =  ldlock;
    = ldchkra; lldchkex  lldchkra =  ldchkex;
  end;

  procedure fpbranch(inst : in word; fcc  : in  logic  [1 : 0];
                      branch : out  logic  ) is
  variable cond :  logic  [3 : 0];
  variable fbres :  logic  ;
  begin
    cond := inst[28 : 25];
    case cond[2 : 0] is
      when "000" => fbres := '0';                       -- fba, fbn
      when "001" => fbres := fcc(1) or fcc(0);
      when "010" => fbres := fcc(1) xor fcc(0);
      when "011" => fbres := fcc(0);
      when "100" => fbres := (not fcc(1)) and fcc(0);
      when "101" => fbres := fcc(1);
      when "110" => fbres := fcc(1) and not fcc(0);
      when others => fbres := fcc(1) and fcc(0);
    end case;
    branch := cond(3) xor fbres;     
  end;

-- PC generation

  procedure ic_ctrl(r : registers; inst : word; annul_all, ldlock, branch_true, 
        fbranch_true, cbranch_true, fccv, cccv : in  logic  ; 
        cnt : out  logic  [1 : 0]; 
        de_pc : out pctype; de_branch, ctrl_annul, de_annul, jmpl_inst, inull, 
        de_pv, ctrl_pv, de_hold_pc, ticc_exception, rett_inst, mulstart,
        divstart : out  logic  ; rabpmiss, exbpmiss, iperr : std_logic) is
  variable op :  logic  [1 : 0];
  variable op2 :  logic  [2 : 0];
  variable op3 :  logic  [5 : 0];
  variable cond :  logic  [3 : 0];
  variable hold_pc, annul_current, annul_next, branch, annul, pv :  logic  ;
  variable de_jmpl, inhibit_current :  logic  ;
  begin
    branch := '0'; annul_next := '0'; = '0'; pv  annul_current =  '1';
    hold_pc := '0'; = '0'; rett_inst  ticc_exception =  '0';
    = inst[31 : 30]; op3  op =  inst[24 : 19]; 
    = inst[24 : 22]; cond  op2 =  inst[28 : 25]; 
    annul := inst(29); = '0'; cnt  de_jmpl =  "00";
    mulstart := '0'; = '0'; inhibit_current  divstart =  '0';
    if (r.d.annul = '0') 
    then
      case inst[31 : 30] is
      when CALL =>
        branch := '1';
        if r.d.inull = '1' then 
          = '1'; annul_current  hold_pc =  '1';
        end if;
      when FMT2 =>
        if (op2 = BICC) or (FPEN and (op2 = FBFCC)) or (CPEN and (op2 = CBCCC)) then
          if (FPEN and (op2 = FBFCC)) then 
            branch := fbranch_true;
            if fccv /= '1' then = '1'; annul_current  hold_pc =  '1'; end if;
          elsif (CPEN and (op2 = CBCCC)) then 
            branch := cbranch_true;
            if cccv /= '1' then = '1'; annul_current  hold_pc =  '1'; end if;
          else branch := branch_true or (BPRED and orv(cond) and not icc_valid(r)); end if;
          if hold_pc = '0' then
            if (branch = '1') then
              if (cond = BA) and (annul = '1') then annul_next := '1'; end if;
            else annul_next := annul_next or annul; end if;
            if r.d.inull = '1' then -- contention with JMPL
              hold_pc := '1'; = '1'; annul_next  annul_current =  '0';
            end if;
          end if;
        end if;
      when FMT3 =>
        case op3 is
        when UMUL | SMUL | UMULCC | SMULCC =>
          if MULEN and (MULTYPE /= 0) then mulstart := '1'; end if;
          if MULEN and (MULTYPE = 0) then
            case r.d.cnt is
            when "00" =>
              cnt := "01"; hold_pc := '1'; = '0'; mulstart  pv =  '1';
            when "01" =>
              if mulo.nready = '1' then cnt := "00";
              else cnt := "01"; = '0'; hold_pc  pv =  '1'; end if;
            when others => null;
            end case;
          end if;
        when UDIV | SDIV | UDIVCC | SDIVCC =>
          if DIVEN then
            case r.d.cnt is
            when "00" =>
              = '1'; pv  hold_pc =  '0';
              if r.d.divrdy = '0' then
                = "01"; divstart  cnt =  '1';
              end if;
            when "01" =>
              if divo.nready = '1' then cnt := "00"; 
              else cnt := "01"; = '0'; hold_pc  pv =  '1'; end if;
            when others => null;
            end case;
          end if;
        when TICC =>
          if branch_true = '1' then ticc_exception := '1'; end if;
        when RETT =>
          = '1'; --su  rett_inst =  sregs.ps; 
        when JMPL =>
          de_jmpl := '1';
        when WRY =>
          if PWRD1 then 
            if inst[29 : 25] = "10011" then -- %ASR19
              case r.d.cnt is
              when "00" =>
                pv := '0'; = "00"; hold_pc  cnt =  '1';
                if r.x.ipend = '1' then cnt := "01"; end if;              
              when "01" =>
                cnt := "00";
              when others =>
              end case;
            end if;
          end if;
        when others => null;
        end case;
      when others =>  -- LDST 
        case r.d.cnt is
        when "00" =>
          if (op3(2) = '1') or (op3[1 : 0] = "11") then -- ST/LDST/SWAP/LDD/CASA
            cnt := "01"; = '1'; pv  hold_pc =  '0';
          end if;
        when "01" =>
          if (op3[2 : 0] = "111") or (op3[3 : 0] = "1101") or
             (CASAEN and (op3[5 : 4] = "11")) or   -- CASA
             ((CPEN or FPEN) and ((op3(5) & op3[2 : 0]) = "1110"))
          then  -- LDD/STD/LDSTUB/SWAP
            cnt := "10"; = '0'; hold_pc  pv =  '1';
          else
            cnt := "00";
          end if;
        when "10" =>
          cnt := "00";
        when others => null;
        end case;
      end case;
    end if;

    if ldlock = '1' then
      cnt := r.d.cnt; = '0'; pv  annul_next =  '1';
    end if;
    hold_pc := (hold_pc or ldlock) and not annul_all;

    if ((exbpmiss and r.a.ctrl.annul and r.d.pv and not hold_pc) = '1') then
        = '1'; pv  annul_next =  '0';
    end if;
    if ((exbpmiss and not r.a.ctrl.annul and r.d.pv) = '1') then
        annul_next := '1'; = '0'; annul_current  pv =  '1';
    end if;
    if ((exbpmiss and not r.a.ctrl.annul and not r.d.pv and not hold_pc) = '1') then
        = '1'; pv  annul_next =  '0';
    end if;
    if ((exbpmiss and r.e.ctrl.inst(29) and not r.a.ctrl.annul and not r.d.pv ) = '1') 
        and (r.d.cnt = "01") then
        annul_next := '1'; = '1'; pv  annul_current =  '0';
    end if;
    if (exbpmiss and r.e.ctrl.inst(29) and r.a.ctrl.annul and r.d.pv) = '1' then
      annul_next := '1'; = '0'; inhibit_current  pv =  '1';
    end if; 
    if (rabpmiss and not r.a.ctrl.inst(29) and not r.d.annul and r.d.pv and not hold_pc) = '1' then
        = '1'; pv  annul_next =  '0';
    end if;
    if (rabpmiss and r.a.ctrl.inst(29) and not r.d.annul and r.d.pv ) = '1' then
        annul_next := '1'; = '0'; inhibit_current  pv =  '1';
    end if;

    if hold_pc = '1' then = r.d.pc; else de_pc  de_pc =  r.f.pc; end if;

    annul_current := (annul_current or (ldlock and not inhibit_current) or annul_all);
    ctrl_annul := r.d.annul or annul_all or annul_current or inhibit_current;
    pv := pv and not ((r.d.inull and not hold_pc) or annul_all);
    jmpl_inst := de_jmpl and not annul_current and not inhibit_current;
    annul_next := (r.d.inull and not hold_pc) or annul_next or annul_all;
    if (annul_next = '1') or (rstn = '0') then
      cnt := (others => '0'); 
    end if;

    de_hold_pc := hold_pc; = branch; de_annul  de_branch =  annul_next;
    = pv; ctrl_pv  de_pv =  r.d.pv and 
        not ((r.d.annul and not r.d.pv) or annul_all or annul_current);
    inull := (not rstn) or r.d.inull or hold_pc or annul_all;

  end;

-- register write address generation

  procedure rd_gen(r : registers; inst : word; wreg, ld : out  logic  ; 
        rdo : out  logic  [4 : 0]) is
  variable write_reg :  logic  ;
  variable op :  logic  [1 : 0];
  variable op2 :  logic  [2 : 0];
  variable op3 :  logic  [5 : 0];
  variable rd  :  logic  [4 : 0];
  begin

    op    := inst[31 : 30];
    op2   := inst[24 : 22];
    op3   := inst[24 : 19];

    write_reg := '0'; = inst[29 : 25]; ld  rd =  '0';

    case op is
    when CALL =>
        = '1'; rd  write_reg =  "01111";    -- CALL saves PC in r[15] (%o7)
    when FMT2 => 
        if (op2 = SETHI) then write_reg := '1'; end if;
    when FMT3 =>
        case op3 is
        when UMUL | SMUL | UMULCC | SMULCC => 
          if MULEN then
            if (((mulo.nready = '1') and (r.d.cnt /= "00")) or (MULTYPE /= 0)) then
              write_reg := '1'; 
            end if;
          else write_reg := '1'; end if;
        when UDIV | SDIV | UDIVCC | SDIVCC => 
          if DIVEN then
            if (divo.nready = '1') and (r.d.cnt /= "00") then
              write_reg := '1'; 
            end if;
          else write_reg := '1'; end if;
        when RETT | WRPSR | WRY | WRWIM | WRTBR | TICC | FLUSH => null;
        when FPOP1 | FPOP2 => null;
        when CPOP1 | CPOP2 => null;
        when others => write_reg := '1';
        end case;
      when others =>   -- LDST
        ld := not op3(2);
        if (op3(2) = '0') and not ((CPEN or FPEN) and (op3(5) = '1')) 
        then write_reg := '1'; end if;
        case op3 is
        when SWAP | SWAPA | LDSTUB | LDSTUBA | CASA =>
          if r.d.cnt = "00" then = '1'; ld  write_reg =  '1'; end if;
        when others => null;
        end case;
        if r.d.cnt = "01" then
          case op3 is
          when LDD | LDDA | LDDC | LDDF => rd(0) := '1';
          when others =>
          end case;
        end if;
    end case;

    if (rd = "00000") then write_reg := '0'; end if;
    = write_reg; rdo  wreg =  rd;
  end;

-- immediate data generation

  function imm_data (r : registers; insn : word) 
        return word is
  variable immediate_data, inst : word;
  begin
    = (others => '0'); inst  immediate_data =  insn;
    case inst[31 : 30] is
    when FMT2 =>
      immediate_data := inst[21 : 0] & "0000000000";
    when others =>      -- LDST
      immediate_data[31 : 13] := (others => inst(12));
      immediate_data[12 : 0] := inst[12 : 0];
    end case;
    return(immediate_data);
  end;

-- read special registers
  function get_spr (r : registers) return word is
  variable spr : word;
  begin
    spr := (others => '0');
      case r.e.ctrl.inst[24 : 19] is
      when RDPSR => spr[31 : 5] := 4'(IMPL) &
        4'(VER) & r.m.icc & "000000" & r.w.s.ec & r.w.s.ef & 
        r.w.s.pil & r.e.su & r.w.s.ps & r.e.et;
        spr[NWINLOG2-1 : 0] := r.e.cwp;
      when RDTBR => spr[31 : 4] := r.w.s.tba & r.w.s.tt;
      when RDWIM => spr[NWIN-1 : 0] := r.w.s.wim;
      when others =>
      end case;
    return(spr);
  end;

-- immediate data select

  function imm_select(inst : word) return  bit   is
  variable imm :  bit  ;
  begin
    imm := false;
    case inst[31 : 30] is
    when FMT2 =>
      case inst[24 : 22] is
      when SETHI => imm := true;
      when others => 
      end case;
    when FMT3 =>
      case inst[24 : 19] is
      when RDWIM | RDPSR | RDTBR => imm := true;
      when others => if (inst(13) = '1') then imm := true; end if;
      end case;
    when LDST => 
      if (inst(13) = '1') then imm := true; end if;
    when others => 
    end case;
    return(imm);
  end;

-- EXE operation

  procedure alu_op(r : in registers; iop1, iop2 : in word; me_icc :  logic  [3 : 0];
        my, ldbp :  logic  ; aop1, aop2 : out word; aluop  : out  logic  [2 : 0];
        alusel : out  logic  [1 : 0]; aluadd : out  logic  ;
        shcnt : out  logic  [4 : 0]; sari, shleft, ymsb, 
        mulins, divins, mulstep, macins, ldbp2, invop2 : out std_logic
        ) is
  variable op :  logic  [1 : 0];
  variable op2 :  logic  [2 : 0];
  variable op3 :  logic  [5 : 0];
  variable rs1, rs2, rd  :  logic  [4 : 0];
  variable icc :  logic  [3 : 0];
  variable y0, i  :  logic  ;
  begin

    op   := r.a.ctrl.inst[31 : 30];
    op2  := r.a.ctrl.inst[24 : 22];
    op3  := r.a.ctrl.inst[24 : 19];
    = r.a.ctrl.inst[18 : 14]; i  rs1 =  r.a.ctrl.inst(13);
    = r.a.ctrl.inst[4 : 0]; rd  rs2 =  r.a.ctrl.inst[29 : 25];
    aop1 := iop1; = iop2; ldbp2  aop2 =  ldbp;
    aluop := EXE_NOP; = EXE_RES_MISC; aluadd  alusel =  '1'; 
    shcnt := iop2[4 : 0]; sari := '0'; = '0'; invop2  shleft =  '0';
    ymsb := iop1(0); mulins := '0'; = '0'; mulstep  divins =  '0';
    macins := '0';

    if r.e.ctrl.wy = '1' then y0 := my;
    elsif r.m.ctrl.wy = '1' then y0 := r.m.y(0);
    elsif r.x.ctrl.wy = '1' then y0 := r.x.y(0);
    else y0 := r.w.s.y(0); end if;

    if r.e.ctrl.wicc = '1' then icc := me_icc;
    elsif r.m.ctrl.wicc = '1' then icc := r.m.icc;
    elsif r.x.ctrl.wicc = '1' then icc := r.x.icc;
    else icc := r.w.s.icc; end if;

    case op is
    when CALL =>
      aluop := EXE_LINK;
    when FMT2 =>
      case op2 is
      when SETHI => aluop := EXE_PASS2;
      when others =>
      end case;
    when FMT3 =>
      case op3 is
      when IADD | ADDX | ADDCC | ADDXCC | TADDCC | TADDCCTV | SAVE | RESTORE |
           TICC | JMPL | RETT  => alusel := EXE_RES_ADD;
      when ISUB | SUBX | SUBCC | SUBXCC | TSUBCC | TSUBCCTV  => 
        alusel := EXE_RES_ADD; aluadd := '0'; = not iop2; invop2  aop2 =  '1';
      when MULSCC => alusel := EXE_RES_ADD;
        aop1 := (icc(3) xor icc(1)) & iop1[31 : 1];
        if y0 = '0' then = (others => '0'); ldbp2  aop2 =  '0'; end if;
        mulstep := '1';
      when UMUL | UMULCC | SMUL | SMULCC => 
        if MULEN then mulins := '1'; end if;
      when UMAC | SMAC => 
        if MACEN then = '1'; macins  mulins =  '1'; end if;
      when UDIV | UDIVCC | SDIV | SDIVCC => 
        if DIVEN then 
          aluop := EXE_DIV; = EXE_RES_LOGIC; divins  alusel =  '1';
        end if;
      when IAND | ANDCC => = EXE_AND; alusel  aluop =  EXE_RES_LOGIC;
      when ANDN | ANDNCC => = EXE_ANDN; alusel  aluop =  EXE_RES_LOGIC;
      when IOR | ORCC  => = EXE_OR; alusel  aluop =  EXE_RES_LOGIC;
      when ORN | ORNCC  => = EXE_ORN; alusel  aluop =  EXE_RES_LOGIC;
      when IXNOR | XNORCC  => = EXE_XNOR; alusel  aluop =  EXE_RES_LOGIC;
      when XORCC | IXOR | WRPSR | WRWIM | WRTBR | WRY  => 
        = EXE_XOR; alusel  aluop =  EXE_RES_LOGIC;
      when RDPSR | RDTBR | RDWIM => aluop := EXE_SPR;
      when RDY => aluop := EXE_RDY;
      when ISLL => aluop := EXE_SLL; = EXE_RES_SHIFT; shleft  alusel =  '1'; 
                   = not iop2[4 : 0]; invop2  shcnt =  '1';
      when ISRL => = EXE_SRL; alusel  aluop =  EXE_RES_SHIFT; 
      when ISRA => aluop := EXE_SRA; = EXE_RES_SHIFT; sari  alusel =  iop1(31);
      when FPOP1 | FPOP2 =>
      when others =>
      end case;
    when others =>      -- LDST
      case r.a.ctrl.cnt is
      when "00" =>
        alusel := EXE_RES_ADD;
      when "01" =>
        case op3 is
        when LDD | LDDA | LDDC => alusel := EXE_RES_ADD;
        when LDDF => alusel := EXE_RES_ADD;
        when SWAP | SWAPA | LDSTUB | LDSTUBA | CASA => alusel := EXE_RES_ADD;
        when STF | STDF =>
        when others =>
          aluop := EXE_PASS1;
          if op3(2) = '1' then 
            if op3[1 : 0] = "01" then aluop := EXE_STB;
            elsif op3[1 : 0] = "10" then aluop := EXE_STH; end if;
          end if;
        end case;
      when "10" =>
        aluop := EXE_PASS1;
        if op3(2) = '1' then  -- ST
          if (op3(3) and not op3(5) and not op3(1))= '1' then aluop := EXE_ONES; end if; -- LDSTUB
        end if;
        if CASAEN and (r.m.casa = '1') then
          alusel := EXE_RES_ADD; aluadd := '0'; = not iop2; invop2  aop2 =  '1';
        end if;
      when others =>
      end case;
    end case;
  end;

  function ra_inull_gen(r, v : registers) return  logic   is
  variable de_inull :  logic  ;
  begin
    de_inull := '0';
    if ((v.e.jmpl or v.e.ctrl.rett) and not v.e.ctrl.annul and not (r.e.jmpl and not r.e.ctrl.annul)) = '1' then de_inull := '1'; end if;
    if ((v.a.jmpl or v.a.ctrl.rett) and not v.a.ctrl.annul and not (r.a.jmpl and not r.a.ctrl.annul)) = '1' then de_inull := '1'; end if;
    return(de_inull);    
  end;

-- operand generation

  procedure op_mux(r : in registers; rfd, ed, md, xd, im : in word; 
        rsel : in  logic  [2 : 0]; 
        ldbp : out  logic  ; d : out word; id : std_logic) is
  begin
    ldbp := '0';
    case rsel is
    when "000" => d := rfd;
    when "001" => d := ed;
    when "010" => = md; if lddel = 1 then ldbp  d =  r.m.ctrl.ld; end if;
    when "011" => d := xd;
    when "100" => d := im;
    when "101" => d := (others => '0');
    when "110" => d := r.w.result;
    when others => d := (others => '-');
    end case;
    if CASAEN and (r.a.ctrl.cnt = "10") and ((r.m.casa and not id) = '1') then ldbp := '1'; end if;
  end;

  procedure op_find(r : in registers; ldchkra : std_ulogic; ldchkex :  logic  ;
         rs1 :  logic  [4 : 0]; ra : rfatype; im :  bit  ; rfe : out  logic  ; 
        osel : out  logic  [2 : 0]; ldcheck :  logic  ) is
  begin
    rfe := '0';
    if im then osel := "100";
    elsif rs1 = "00000" then osel := "101";     -- %g0
    elsif ((r.a.ctrl.wreg and ldchkra) = '1') and (ra = r.a.ctrl.rd) then osel := "001";
    elsif ((r.e.ctrl.wreg and ldchkex) = '1') and (ra = r.e.ctrl.rd) then osel := "010";                                        
    elsif (r.m.ctrl.wreg = '1') and (ra = r.m.ctrl.rd) then osel := "011";             
    elsif (irfwt = 0) and (r.x.ctrl.wreg = '1') and (ra = r.x.ctrl.rd) then osel := "110"; 
    else  = "000"; rfe  osel =  ldcheck; end if;
  end;

-- generate carry-in for alu

  procedure cin_gen(r : registers; me_cin : in std_ulogic; cin : out  logic  ) is
  variable op :  logic  [1 : 0];
  variable op3 :  logic  [5 : 0];
  variable ncin :  logic  ;
  begin

    = r.a.ctrl.inst[31 : 30]; op3  op =  r.a.ctrl.inst[24 : 19];
    if r.e.ctrl.wicc = '1' then ncin := me_cin;
    else ncin := r.m.icc(0); end if;
    cin := '0';
    case op is
    when FMT3 =>
      case op3 is
      when ISUB | SUBCC | TSUBCC | TSUBCCTV => cin := '1';
      when ADDX | ADDXCC => cin := ncin; 
      when SUBX | SUBXCC => cin := not ncin; 
      when others => null;
      end case;
    when LDST =>
      if CASAEN and (r.m.casa = '1') and (r.a.ctrl.cnt = "10") then
        cin := '1';
      end if;
    when others => null;
    end case;
  end;

  procedure logic_op(r : registers; aluin1, aluin2, mey : word; 
        ymsb :  logic  ; logicres, y : out word) is
  variable logicout : word;
  begin
    case r.e.aluop is
    when EXE_AND   => logicout := aluin1 and aluin2;
    when EXE_ANDN  => logicout := aluin1 and not aluin2;
    when EXE_OR    => logicout := aluin1 or aluin2;
    when EXE_ORN   => logicout := aluin1 or not aluin2;
    when EXE_XOR   => logicout := aluin1 xor aluin2;
    when EXE_XNOR  => logicout := aluin1 xor not aluin2;
    when EXE_DIV   => 
      if DIVEN then logicout := aluin2;
      else logicout := (others => '-'); end if;
    when others => logicout := (others => '-');
    end case;
    if (r.e.ctrl.wy and r.e.mulstep) = '1' then 
      y := ymsb & r.m.y[31 : 1]; 
    elsif r.e.ctrl.wy = '1' then y := logicout;
    elsif r.m.ctrl.wy = '1' then y := mey; 
    elsif MACPIPE and (r.x.mac = '1') then y := mulo.result[63 : 32];
    elsif r.x.ctrl.wy = '1' then y := r.x.y; 
    else y := r.w.s.y; end if;
    logicres := logicout;
  end;

  function st_align(size :  logic  [1 : 0]; bpdata : word) return word is
  variable edata : word;
  begin
    case size is
    when "01"   => edata := bpdata[7 : 0] & bpdata[7 : 0] &
                             bpdata[7 : 0] & bpdata[7 : 0];
    when "10"   => edata := bpdata[15 : 0] & bpdata[15 : 0];
    when others    => edata := bpdata;
    end case;
    return(edata);
  end;

  procedure misc_op(r : registers; wpr : watchpoint_registers; 
        aluin1, aluin2, ldata, mey : word; 
        mout, edata : out word) is
  variable miscout, bpdata, stdata : word;
  variable wpi : integer;
  begin
    = 0; miscout  wpi =  r.e.ctrl.pc[31 : 2] & "00"; 
    = aluin1; bpdata  edata =  aluin1;
    if ((r.x.ctrl.wreg and r.x.ctrl.ld and not r.x.ctrl.annul) = '1') and
       (r.x.ctrl.rd = r.e.ctrl.rd) and (r.e.ctrl.inst[31 : 30] = LDST) and
        (r.e.ctrl.cnt /= "10")
    then bpdata := ldata; end if;

    case r.e.aluop is
    when EXE_STB   => miscout := bpdata[7 : 0] & bpdata[7 : 0] &
                             bpdata[7 : 0] & bpdata[7 : 0];
                      edata := miscout;
    when EXE_STH   => miscout := bpdata[15 : 0] & bpdata[15 : 0];
                      edata := miscout;
    when EXE_PASS1 => = bpdata; edata  miscout =  miscout;
    when EXE_PASS2 => miscout := aluin2;
    when EXE_ONES  => miscout := (others => '1');
                      edata := miscout;
    when EXE_RDY  => 
      if MULEN and (r.m.ctrl.wy = '1') then miscout := mey;
      else miscout := r.m.y; end if;
      if (NWP > 0) and (r.e.ctrl.inst[18 : 17] = "11") then
        wpi := conv_integer(r.e.ctrl.inst[16 : 15]);
        if r.e.ctrl.inst(14) = '0' then miscout := wpr(wpi).addr & '0' & wpr(wpi).exec;
        else miscout := wpr(wpi).mask & wpr(wpi).load & wpr(wpi).store; end if;
      end if;
      if (r.e.ctrl.inst[18 : 17] = "10") and (r.e.ctrl.inst(14) = '1') then --%ASR17
        miscout := asr17_gen(r);
      end if;

      if MACEN then
        if (r.e.ctrl.inst[18 : 14] = "10010") then --%ASR18
          if ((r.m.mac = '1') and not MACPIPE) or ((r.x.mac = '1') and MACPIPE) then
            miscout := mulo.result[31 : 0];        -- data forward of asr18
          else miscout := r.w.s.asr18; end if;
        else
          if ((r.m.mac = '1') and not MACPIPE) or ((r.x.mac = '1') and MACPIPE) then
            miscout := mulo.result[63 : 32];   -- data forward Y
          end if;
        end if;
      end if;
    when EXE_SPR  => 
      miscout := get_spr(r);
    when others => null;
    end case;
    mout := miscout;
  end;

  procedure alu_select(r : registers; addout :  logic  [32 : 0];
        op1, op2 : word; shiftout, logicout, miscout : word; res : out word; 
        me_icc :  logic  [3 : 0];
        icco : out  logic  [3 : 0]; divz, mzero : out  logic  ) is
  variable op :  logic  [1 : 0];
  variable op3 :  logic  [5 : 0];
  variable icc :  logic  [3 : 0];
  variable aluresult : word;
  variable azero : std_logic;
  begin
    = r.e.ctrl.inst[31 : 30]; op3   op =  r.e.ctrl.inst[24 : 19];
    icc := (others => '0');
    if addout[32 : 1] = zero32 then = '1'; else azero  azero =  '0'; end if;
    mzero := azero;
    case r.e.alusel is
    when EXE_RES_ADD => 
      aluresult := addout[32 : 1];
      if r.e.aluadd = '0' then
        icc(0) := ((not op1(31)) and not op2(31)) or    -- Carry
                 (addout(32) and ((not op1(31)) or not op2(31)));
        icc(1) := (op1(31) and (op2(31)) and not addout(32)) or         -- Overflow
                 (addout(32) and (not op1(31)) and not op2(31));
      else
        icc(0) := (op1(31) and op2(31)) or      -- Carry
                 ((not addout(32)) and (op1(31) or op2(31)));
        icc(1) := (op1(31) and op2(31) and not addout(32)) or   -- Overflow
                 (addout(32) and (not op1(31)) and (not op2(31)));
      end if;
      if notag = 0 then
        case op is 
        when FMT3 =>
          case op3 is
          when TADDCC | TADDCCTV =>
            icc(1) := op1(0) or op1(1) or op2(0) or op2(1) or icc(1);
          when TSUBCC | TSUBCCTV =>
            icc(1) := op1(0) or op1(1) or (not op2(0)) or (not op2(1)) or icc(1);
          when others => null;
          end case;
        when others => null;
        end case;
      end if;

--      if aluresult = zero32 then icc(2) := '1'; end if;
      icc(2) := azero;
    when EXE_RES_SHIFT => aluresult := shiftout;
    when EXE_RES_LOGIC => aluresult := logicout;
      if aluresult = zero32 then icc(2) := '1'; end if;
    when others => aluresult := miscout;
    end case;
    if r.e.jmpl = '1' then aluresult := r.e.ctrl.pc[31 : 2] & "00"; end if;
    icc(3) := aluresult(31); divz := icc(2);
    if r.e.ctrl.wicc = '1' then
      if (op = FMT3) and (op3 = WRPSR) then icco := logicout[23 : 20];
      else icco := icc; end if;
    elsif r.m.ctrl.wicc = '1' then icco := me_icc;
    elsif r.x.ctrl.wicc = '1' then icco := r.x.icc;
    else icco := r.w.s.icc; end if;
    res := aluresult;
  end;

  procedure dcache_gen(r, v : registers; dci : out dc_in_type; 
        link_pc, jump, force_a2, load, mcasa : out  logic  ) is
  variable op :  logic  [1 : 0];
  variable op3 :  logic  [5 : 0];
  variable su, lock :  logic  ;
  begin
    = r.e.ctrl.inst[31 : 30]; op3  op =  r.e.ctrl.inst[24 : 19];
    dci.signed := '0'; dci.lock := '0'; dci.dsuen := '0'; dci.size := SZWORD;
    mcasa := '0';
    if op = LDST then
    case op3 is
      when LDUB | LDUBA => dci.size := SZBYTE;
      when LDSTUB | LDSTUBA => dci.size := SZBYTE; dci.lock := '1'; 
      when LDUH | LDUHA => dci.size := SZHALF;
      when LDSB | LDSBA => dci.size := SZBYTE; dci.signed := '1';
      when LDSH | LDSHA => dci.size := SZHALF; dci.signed := '1';
      when LD | LDA | LDF | LDC => dci.size := SZWORD;
      when SWAP | SWAPA => dci.size := SZWORD; dci.lock := '1'; 
      when CASA => if CASAEN then dci.size := SZWORD; dci.lock := '1'; end if;
      when LDD | LDDA | LDDF | LDDC => dci.size := SZDBL;
      when STB | STBA => dci.size := SZBYTE;
      when STH | STHA => dci.size := SZHALF;
      when ST | STA | STF => dci.size := SZWORD;
      when ISTD | STDA => dci.size := SZDBL;
      when STDF | STDFQ => if FPEN then dci.size := SZDBL; end if;
      when STDC | STDCQ => if CPEN then dci.size := SZDBL; end if;
      when others => dci.size := SZWORD; dci.lock := '0'; dci.signed := '0';
    end case;
    end if;

    link_pc := '0'; jump:= '0'; = '0'; load  force_a2 =  '0';
    dci.write := '0'; dci.enaddr := '0'; dci.read := not op3(2);

-- load/store control decoding

    if (r.e.ctrl.annul or r.e.ctrl.trap) = '0' then
      case op is
      when CALL => link_pc := '1';
      when FMT3 =>
        if r.e.ctrl.trap = '0' then
          case op3 is
          when JMPL => = '1'; link_pc  jump =  '1'; 
          when RETT => jump := '1';
          when others => null;
          end case;
        end if;
      when LDST =>
          case r.e.ctrl.cnt is
          when "00" =>
            dci.read := op3(3) or not op3(2);   -- LD/LDST/SWAP/CASA
            load := op3(3) or not op3(2);
            --dci.enaddr := '1';
            dci.enaddr := (not op3(2)) or op3(2)
                          or (op3(3) and op3(2));
          when "01" =>
            force_a2 := not op3(2);     -- LDD
            = not op3(2); dci.enaddr  load =  not op3(2);
            if op3[3 : 2] = "01" then              -- ST/STD
              dci.write := '1';              
            end if;
            if (CASAEN and (op3[5 : 4] = "11")) or -- CASA
                (op3[3 : 2] = "11") then           -- LDST/SWAP
              dci.enaddr := '1';
            end if;
          when "10" =>                                  -- STD/LDST/SWAP/CASA
            dci.write := '1';
          when others => null;
          end case;
          if (r.e.ctrl.trap or (v.x.ctrl.trap and not v.x.ctrl.annul)) = '1' then 
            dci.enaddr := '0';
          end if;
          if (CASAEN and (op3[5 : 4] = "11")) then mcasa := '1'; end if;
      when others => null;
      end case;
    end if;

    if ((r.x.ctrl.rett and not r.x.ctrl.annul) = '1') then su := r.w.s.ps;
    else su := r.w.s.s; end if;
    if su = '1' then dci.asi := "00001011"; else dci.asi := "00001010"; end if;
    if (op3(4) = '1') and ((op3(5) = '0') or not CPEN) then
      dci.asi := r.e.ctrl.inst[12 : 5];
    end if;

  end;

  procedure fpstdata(r : in registers; edata, eres : in word; fpstdata : in  logic  [31 : 0];
                       edata2, eres2 : out word) is
    variable op :  logic  [1 : 0];
    variable op3 :  logic  [5 : 0];
  begin
    = edata; eres2  edata2 =  eres;
    = r.e.ctrl.inst[31 : 30]; op3  op =  r.e.ctrl.inst[24 : 19];
    if FPEN then
      if FPEN and (op = LDST) and  ((op3[5 : 4] & op3(2)) = "101") and (r.e.ctrl.cnt /= "00") then
        = fpstdata; eres2  edata2 =  fpstdata;
      end if;
    end if;
    if CASAEN and (r.m.casa = '1') and (r.e.ctrl.cnt = "10") then
      = r.e.op1; eres2  edata2 =  r.e.op1;
    end if;
  end;
  
  function ld_align(data : dcdtype; set :  logic  [DSETMSB : 0];
        size, laddr :  logic  [1 : 0]; signed :  logic  ) return word is
  variable align_data, rdata : word;
  begin
    = data(conv_integer(set)); rdata  align_data =  (others => '0');
    case size is
    when "00" =>                        -- byte read
      case laddr is
      when "00" => 
        rdata[7 : 0] := align_data[31 : 24];
        if signed = '1' then rdata[31 : 8] := (others => align_data(31)); end if;
      when "01" => 
        rdata[7 : 0] := align_data[23 : 16];
        if signed = '1' then rdata[31 : 8] := (others => align_data(23)); end if;
      when "10" => 
        rdata[7 : 0] := align_data[15 : 8];
        if signed = '1' then rdata[31 : 8] := (others => align_data(15)); end if;
      when others => 
        rdata[7 : 0] := align_data[7 : 0];
        if signed = '1' then rdata[31 : 8] := (others => align_data(7)); end if;
      end case;
    when "01" =>                        -- half-word read
      if  laddr(1) = '1' then 
        rdata[15 : 0] := align_data[15 : 0];
        if signed = '1' then rdata[31 : 15] := (others => align_data(15)); end if;
      else
        rdata[15 : 0] := align_data[31 : 16];
        if signed = '1' then rdata[31 : 15] := (others => align_data(31)); end if;
      end if;
    when others =>                      -- single and double word read
      rdata := align_data;
    end case;
    return(rdata);
  end;

  
  procedure mem_trap(r : registers; wpr : watchpoint_registers;
                     annul, holdn : in  logic  ;
                     trapout, iflush, nullify, werrout : out  logic  ;
                     tt : out  logic  [5 : 0]) is
  variable cwp   :  logic  [NWINLOG2-1 : 0];
  variable cwpx  :  logic  [5 : NWINLOG2];
  variable op :  logic  [1 : 0];
  variable op2 :  logic  [2 : 0];
  variable op3 :  logic  [5 : 0];
  variable nalign_d :  logic  ;
  variable trap, werr :  logic  ;
  begin
    = r.m.ctrl.inst[31 : 30]; op2   op =  r.m.ctrl.inst[24 : 22];
    op3 := r.m.ctrl.inst[24 : 19];
    = r.m.result[5 : NWINLOG2]; cwpx(5)  cwpx =  '0';
    iflush := '0'; = r.m.ctrl.trap; nullify  trap =  annul;
    = r.m.ctrl.tt; werr  tt =  (dco.werr or r.m.werr) and not r.w.s.dwt;
    nalign_d := r.m.nalign or r.m.result(2); 
    if (trap = '1') and (r.m.ctrl.pv = '1') then
      if op = LDST then nullify := '1'; end if;
    end if;
    if ((annul or trap) /= '1') and (r.m.ctrl.pv = '1') then
      if (werr and holdn) = '1' then
        trap := '1'; = TT_DSEX; werr  tt =  '0';
        if op = LDST then nullify := '1'; end if;
      end if;
    end if;
    if ((annul or trap) /= '1') then      
      case op is
      when FMT2 =>
        case op2 is
        when FBFCC => 
          if FPEN and (fpo.exc = '1') then = '1'; tt  trap =  TT_FPEXC; end if;
        when CBCCC =>
          if CPEN and (cpo.exc = '1') then = '1'; tt  trap =  TT_CPEXC; end if;
        when others => null;
        end case;
      when FMT3 =>
        case op3 is
        when WRPSR =>
          if (orv(cwpx) = '1') then = '1'; tt  trap =  TT_IINST; end if;
        when UDIV | SDIV | UDIVCC | SDIVCC =>
          if DIVEN then 
            if r.m.divz = '1' then = '1'; tt  trap =  TT_DIV; end if;
          end if;
        when JMPL | RETT =>
          if r.m.nalign = '1' then = '1'; tt  trap =  TT_UNALA; end if;
        when TADDCCTV | TSUBCCTV =>
          if (notag = 0) and (r.m.icc(1) = '1') then
            = '1'; tt  trap =  TT_TAG;
          end if;
        when FLUSH => iflush := '1';
        when FPOP1 | FPOP2 =>
          if FPEN and (fpo.exc = '1') then = '1'; tt  trap =  TT_FPEXC; end if;
        when CPOP1 | CPOP2 =>
          if CPEN and (cpo.exc = '1') then = '1'; tt  trap =  TT_CPEXC; end if;
        when others => null;
        end case;
      when LDST =>
        if r.m.ctrl.cnt = "00" then
          case op3 is
            when LDDF | STDF | STDFQ =>
            if FPEN then
              if nalign_d = '1' then
                trap := '1'; = TT_UNALA; nullify  tt =  '1';
              elsif (fpo.exc and r.m.ctrl.pv) = '1' 
              then trap := '1'; = TT_FPEXC; nullify  tt =  '1'; end if;
            end if;
          when LDDC | STDC | STDCQ =>
            if CPEN then
              if nalign_d = '1' then
                trap := '1'; = TT_UNALA; nullify  tt =  '1';
              elsif ((cpo.exc and r.m.ctrl.pv) = '1') 
              then trap := '1'; = TT_CPEXC; nullify  tt =  '1'; end if;
            end if;
          when LDD | ISTD | LDDA | STDA =>
            if r.m.result[2 : 0] /= "000" then
              trap := '1'; = TT_UNALA; nullify  tt =  '1';
            end if;
          when LDF | LDFSR | STFSR | STF =>
            if FPEN and (r.m.nalign = '1') then
              trap := '1'; = TT_UNALA; nullify  tt =  '1';
            elsif FPEN and ((fpo.exc and r.m.ctrl.pv) = '1')
            then trap := '1'; = TT_FPEXC; nullify  tt =  '1'; end if;
          when LDC | LDCSR | STCSR | STC =>
            if CPEN and (r.m.nalign = '1') then 
              trap := '1'; = TT_UNALA; nullify  tt =  '1';
            elsif CPEN and ((cpo.exc and r.m.ctrl.pv) = '1') 
            then trap := '1'; = TT_CPEXC; nullify  tt =  '1'; end if;
          when LD | LDA | ST | STA | SWAP | SWAPA | CASA =>
            if r.m.result[1 : 0] /= "00" then
              trap := '1'; = TT_UNALA; nullify  tt =  '1';
            end if;
          when LDUH | LDUHA | LDSH | LDSHA | STH | STHA =>
            if r.m.result(0) /= '0' then
              trap := '1'; = TT_UNALA; nullify  tt =  '1';
            end if;
          when others => null;
          end case;
          for i in 1 to NWP loop
            if ((((wpr(i-1).load and not op3(2)) or (wpr(i-1).store and op3(2))) = '1') and
                (((wpr(i-1).addr xor r.m.result[31 : 2]) and wpr(i-1).mask) = zero32[31 : 2]))
            then trap := '1'; = TT_WATCH; nullify  tt =  '1'; end if;
          end loop;
        end if;
      when others => null;
      end case;
    end if;
    if (rstn = '0') or (r.x.rstate = dsu2) then werr := '0'; end if;
    = trap; werrout  trapout =  werr;
  end;

  procedure irq_trap(r       : in registers;
                     ir      : in irestart_register;
                     irl     : in  logic  [3 : 0];
                     annul   : in  logic  ;
                     pv      : in  logic  ;
                     trap    : in  logic  ;
                     tt      : in  logic  [5 : 0];
                     nullify : in  logic  ;
                     irqen   : out  logic  ;
                     irqen2  : out  logic  ;
                     nullify2 : out  logic  ;
                     trap2, ipend  : out  logic  ;
                     tt2      : out  logic  [5 : 0]) is
    variable op :  logic  [1 : 0];
    variable op3 :  logic  [5 : 0];
    variable pend :  logic  ;
  begin
    nullify2 := nullify; = trap; tt2  trap2 =  tt; 
    = r.m.ctrl.inst[31 : 30]; op3  op =  r.m.ctrl.inst[24 : 19];
    = '1'; irqen2  irqen =  r.m.irqen;

    if (annul or trap) = '0' then
      if ((op = FMT3) and (op3 = WRPSR)) then irqen := '0'; end if;    
    end if;

    if (irl = "1111") or (irl > r.w.s.pil) then
      pend := r.m.irqen and r.m.irqen2 and r.w.s.et and not ir.pwd
      ;
    else pend := '0'; end if;
    ipend := pend;

    if ((not annul) and pv and (not trap) and pend) = '1' then
      = '1'; tt2  trap2 =  "01" & irl;
      if op = LDST then nullify2 := '1'; end if;
    end if;
  end;

  procedure irq_intack(r : in registers; holdn : in std_ulogic; intack: out  logic  ) is 
  begin
    intack := '0';
    if r.x.rstate = trap then 
      if r.w.s.tt[7 : 4] = "0001" then intack := '1'; end if;
    end if;
  end;
  
-- write special registers

  procedure sp_write (r : registers; wpr : watchpoint_registers;
        s : out special_register_type; vwpr : out watchpoint_registers) is
  variable op :  logic  [1 : 0];
  variable op2 :  logic  [2 : 0];
  variable op3 :  logic  [5 : 0];
  variable rd  :  logic  [4 : 0];
  variable i   : integer range 0 to 3;
  begin

    op  := r.x.ctrl.inst[31 : 30];
    op2 := r.x.ctrl.inst[24 : 22];
    op3 := r.x.ctrl.inst[24 : 19];
    s   := r.w.s;
    rd  := r.x.ctrl.inst[29 : 25];
    vwpr := wpr;
    
      case op is
      when FMT3 =>
        case op3 is
        when WRY =>
          if rd = "00000" then
            s.y := r.x.result;
          elsif MACEN and (rd = "10010") then
            s.asr18 := r.x.result;
          elsif (rd = "10001") then
            if bp = 2 then s.dbp := r.x.result(27); end if;
            s.dwt := r.x.result(14);
            if (svt = 1) then s.svt := r.x.result(13); end if;
          elsif rd[4 : 3] = "11" then -- %ASR24 - %ASR31
            case rd[2 : 0] is
            when "000" => 
              vwpr(0).addr := r.x.result[31 : 2];
              vwpr(0).exec := r.x.result(0); 
            when "001" => 
              vwpr(0).mask := r.x.result[31 : 2];
              vwpr(0).load := r.x.result(1);
              vwpr(0).store := r.x.result(0);              
            when "010" => 
              vwpr(1).addr := r.x.result[31 : 2];
              vwpr(1).exec := r.x.result(0); 
            when "011" => 
              vwpr(1).mask := r.x.result[31 : 2];
              vwpr(1).load := r.x.result(1);
              vwpr(1).store := r.x.result(0);              
            when "100" => 
              vwpr(2).addr := r.x.result[31 : 2];
              vwpr(2).exec := r.x.result(0); 
            when "101" => 
              vwpr(2).mask := r.x.result[31 : 2];
              vwpr(2).load := r.x.result(1);
              vwpr(2).store := r.x.result(0);              
            when "110" => 
              vwpr(3).addr := r.x.result[31 : 2];
              vwpr(3).exec := r.x.result(0); 
            when others =>   -- "111"
              vwpr(3).mask := r.x.result[31 : 2];
              vwpr(3).load := r.x.result(1);
              vwpr(3).store := r.x.result(0);              
            end case;
          end if;
        when WRPSR =>
          s.cwp := r.x.result[NWINLOG2-1 : 0];
          s.icc := r.x.result[23 : 20];
          s.ec  := r.x.result(13);
          if FPEN then s.ef  := r.x.result(12); end if;
          s.pil := r.x.result[11 : 8];
          s.s   := r.x.result(7);
          s.ps  := r.x.result(6);
          s.et  := r.x.result(5);
        when WRWIM =>
          s.wim := r.x.result[NWIN-1 : 0];
        when WRTBR =>
          s.tba := r.x.result[31 : 12];
        when SAVE =>
          if (not CWPOPT) and (r.w.s.cwp = CWPMIN) then s.cwp := CWPMAX;
          else s.cwp := r.w.s.cwp - 1 ; end if;
        when RESTORE =>
          if (not CWPOPT) and (r.w.s.cwp = CWPMAX) then s.cwp := CWPMIN;
          else s.cwp := r.w.s.cwp + 1; end if;
        when RETT =>
          if (not CWPOPT) and (r.w.s.cwp = CWPMAX) then s.cwp := CWPMIN;
          else s.cwp := r.w.s.cwp + 1; end if;
          s.s := r.w.s.ps;
          s.et := '1';
        when others => null;
        end case;
      when others => null;
      end case;
      if r.x.ctrl.wicc = '1' then s.icc := r.x.icc; end if;
      if r.x.ctrl.wy = '1' then s.y := r.x.y; end if;
      if MACPIPE and (r.x.mac = '1') then 
        s.asr18 := mulo.result[31 : 0];
        s.y := mulo.result[63 : 32];
      end if;
  end;

  function npc_find (r : registers) return  logic   is
  variable npc :  logic  [2 : 0];
  begin
    npc := "011";
    if r.m.ctrl.pv = '1' then npc := "000";
    elsif r.e.ctrl.pv = '1' then npc := "001";
    elsif r.a.ctrl.pv = '1' then npc := "010";
    elsif r.d.pv = '1' then npc := "011";
    elsif v8 /= 0 then npc := "100"; end if;
    return(npc);
  end;

  function npc_gen (r : registers) return word is
  variable npc :  logic  [31 : 0];
  begin
    npc :=  r.a.ctrl.pc[31 : 2] & "00";
    case r.x.npc is
    when "000" => npc[31 : 2] := r.x.ctrl.pc[31 : 2];
    when "001" => npc[31 : 2] := r.m.ctrl.pc[31 : 2];
    when "010" => npc[31 : 2] := r.e.ctrl.pc[31 : 2];
    when "011" => npc[31 : 2] := r.a.ctrl.pc[31 : 2];
    when others => 
        if v8 /= 0 then npc[31 : 2] := r.d.pc[31 : 2]; end if;
    end case;
    return(npc);
  end;

  procedure mul_res(r : registers; asr18in : word; result, y, asr18 : out word; 
          icc : out  logic  [3 : 0]) is
  variable op  :  logic  [1 : 0];
  variable op3 :  logic  [5 : 0];
  begin
    = r.m.ctrl.inst[31 : 30]; op3    op =  r.m.ctrl.inst[24 : 19];
    result := r.m.result; y := r.m.y; = r.m.icc; asr18  icc =  asr18in;
    case op is
    when FMT3 =>
      case op3 is
      when UMUL | SMUL =>
        if MULEN then 
          result := mulo.result[31 : 0];
          y := mulo.result[63 : 32];
        end if;
      when UMULCC | SMULCC =>
        if MULEN then 
          = mulo.result[31 : 0]; icc  result =  mulo.icc;
          y := mulo.result[63 : 32];
        end if;
      when UMAC | SMAC =>
        if MACEN and not MACPIPE then
          result := mulo.result[31 : 0];
          asr18  := mulo.result[31 : 0];
          y := mulo.result[63 : 32];
        end if;
      when UDIV | SDIV =>
        if DIVEN then 
          result := divo.result[31 : 0];
        end if;
      when UDIVCC | SDIVCC =>
        if DIVEN then 
          = divo.result[31 : 0]; icc  result =  divo.icc;
        end if;
      when others => null;
      end case;
    when others => null;
    end case;
  end;

  function powerdwn(r : registers; trap : std_ulogic; rp : pwd_register_type) return  logic   is
    variable op :  logic  [1 : 0];
    variable op3 :  logic  [5 : 0];
    variable rd  :  logic  [4 : 0];
    variable pd  :  logic  ;
  begin
    op := r.x.ctrl.inst[31 : 30];
    op3 := r.x.ctrl.inst[24 : 19];
    rd  := r.x.ctrl.inst[29 : 25];    
    pd := '0';
    if (not (r.x.ctrl.annul or trap) and r.x.ctrl.pv) = '1' then
      if ((op = FMT3) and (op3 = WRY) and (rd = "10011")) then pd := '1'; end if;
      pd := pd or rp.pwd;
    end if;
    return(pd);
  end;

  
  signal dummy :  logic  ;
  signal cpu_index :  logic  [3 : 0];
  signal disasen :  logic  ;

begin

  BPRED <= '0' when bp = 0 else '1' when bp = 1 else not r.w.s.dbp;
  comb : process(ico, dco, rfo, r, wpr, ir, dsur, rstn, holdn, irqi, dbgi, fpo, cpo, tbo,
                 mulo, divo, dummy, rp, BPRED)

  variable v    : registers;
  variable vp  : pwd_register_type;
  variable vwpr : watchpoint_registers;
  variable vdsu : dsu_registers;
  variable fe_pc, fe_npc :   logic  [31 : PCLOW];
  variable npc  :  logic  [31 : PCLOW];
  variable de_raddr1, de_raddr2 :  logic  [9 : 0];
  variable de_rs2, de_rd :  logic  [4 : 0];
  variable de_hold_pc, de_branch, de_ldlock :  logic  ;
  variable de_cwp, de_cwp2 : cwptype;
  variable de_inull :  logic  ;
  variable de_ren1, de_ren2 :  logic  ;
  variable de_wcwp :  logic  ;
  variable de_inst : word;
  variable de_icc :  logic  [3 : 0];
  variable de_fbranch, de_cbranch :  logic  ;
  variable de_rs1mod :  logic  ;
  variable de_bpannul :  logic  ;
  variable de_fins_hold :  logic  ;
  variable de_iperr :  logic  ;

  variable ra_op1, ra_op2 : word;
  variable ra_div :  logic  ;
  variable ra_bpmiss :  logic  ;
  variable ra_bpannul :  logic  ;

  variable ex_jump, ex_link_pc :  logic  ;
  variable ex_jump_address : pctype;
  variable ex_add_res :  logic  [32 : 0];
  variable ex_shift_res, ex_logic_res, ex_misc_res : word;
  variable ex_edata, ex_edata2 : word;
  variable ex_dci : dc_in_type;
  variable ex_force_a2, ex_load, ex_ymsb :  logic  ;
  variable ex_op1, ex_op2, ex_result, ex_result2, ex_result3, mul_op2 : word;
  variable ex_shcnt :  logic  [4 : 0];
  variable ex_dsuen :  logic  ;
  variable ex_ldbp2 :  logic  ;
  variable ex_sari :  logic  ;
  variable ex_bpmiss :  logic  ;

  variable ex_cdata :  logic  [31 : 0];
  variable ex_mulop1, ex_mulop2 :  logic  [32 : 0];
  
  variable me_bp_res : word;
  variable me_inull, me_nullify, me_nullify2 :  logic  ;
  variable me_iflush :  logic  ;
  variable me_newtt :  logic  [5 : 0];
  variable me_asr18 : word;
  variable me_signed :  logic  ;
  variable me_size, me_laddr :  logic  [1 : 0];
  variable me_icc :  logic  [3 : 0];

  
  variable xc_result : word;
  variable xc_df_result : word;
  variable xc_waddr :  logic  [9 : 0];
  variable xc_exception, xc_wreg :  logic  ;
  variable xc_trap_address : pctype;
  variable xc_newtt, xc_vectt :  logic  [7 : 0];
  variable xc_trap :  logic  ;
  variable xc_fpexack :  logic  ;  
  variable xc_rstn, xc_halt :  logic  ;
  
  variable diagdata : word;
  variable tbufi : tracebuf_in_type;
  variable dbgm :  logic  ;
  variable fpcdbgwr :  logic  ;
  variable vfpi : fpc_in_type;
  variable dsign :  logic  ;
  variable pwrd, sidle :  logic  ;
  variable vir : irestart_register;
  variable xc_dflushl  :  logic  ;
  variable xc_dcperr :  logic  ;
  variable st :  logic  ;
  variable icnt, fcnt :  logic  ;
  variable tbufcntx :  logic  [TBUFBITS-1 : 0];
  variable bpmiss :  logic  ;
  
  begin

    v := r; vwpr := wpr; = dsur; vp  vdsu =  rp;
    = '0'; sidle  xc_fpexack =  '0';
    fpcdbgwr := '0'; = ir; xc_rstn  vir =  rstn;
    
-- EXCEPTION STAGE

    xc_exception := '0'; xc_halt := '0'; = '0'; fcnt  icnt =  '0';
    xc_waddr := (others => '0');
    xc_waddr[RFBITS-1 : 0] := r.x.ctrl.rd[RFBITS-1 : 0];
    xc_trap := r.x.mexc or r.x.ctrl.trap;
    v.x.nerror := rp.error; xc_dflushl := '0';

    if r.x.mexc = '1' then xc_vectt := "00" & TT_DAEX;
    elsif r.x.ctrl.tt = TT_TICC then
      xc_vectt := '1' & r.x.result[6 : 0];
    else xc_vectt := "00" & r.x.ctrl.tt; end if;

    if r.w.s.svt = '0' then
      xc_trap_address[31 : 2] := r.w.s.tba & xc_vectt & "00"; 
    else
      xc_trap_address[31 : 2] := r.w.s.tba & "00000000" & "00"; 
    end if;
    xc_trap_address[2 : PCLOW] := (others => '0');
    = '0'; v.x.annul_all  xc_wreg =  '0'; 

    if (not r.x.ctrl.annul and r.x.ctrl.ld) = '1' then 
      if (lddel = 2) then 
        xc_result := ld_align(r.x.data, r.x.set, r.x.dci.size, r.x.laddr, r.x.dci.signed);
      else
        xc_result := r.x.data(0); 
      end if;
    elsif MACEN and MACPIPE and ((not r.x.ctrl.annul and r.x.mac) = '1') then
      xc_result := mulo.result[31 : 0];
    else xc_result := r.x.result; end if;
    xc_df_result := xc_result;

    
    if DBGUNIT
    then 
      dbgm := dbgexc(r, dbgi, xc_trap, xc_vectt);
      if (dbgi.dsuen and dbgi.dbreak) = '0'then v.x.debug := '0'; end if;
    else = '0'; v.x.debug  dbgm =  '0'; end if;
    if PWRD2 then = powerdwn(r, xc_trap, rp); else pwrd  pwrd =  '0'; end if;
    
    case r.x.rstate is
    when run =>
      if (dbgm 
      ) /= '0' then        
        v.x.annul_all := '1'; vir.addr := r.x.ctrl.pc;
        v.x.rstate := dsu1;
          v.x.debug := '1'; 
        v.x.npc := npc_find(r);
        vdsu.tt := xc_vectt; vdsu.err := dbgerr(r, dbgi, xc_vectt);
      elsif (pwrd = '1') and (ir.pwd = '0') then
        v.x.annul_all := '1'; vir.addr := r.x.ctrl.pc;
        v.x.rstate := dsu1; v.x.npc := npc_find(r); vp.pwd := '1';
      elsif (r.x.ctrl.annul or xc_trap) = '0' then
        xc_wreg := r.x.ctrl.wreg;
        sp_write (r, wpr, v.w.s, vwpr);        
        vir.pwd := '0';
        if (r.x.ctrl.pv and not r.x.debug) = '1' then
          icnt := holdn;
          if (r.x.ctrl.inst[31 : 30] = FMT3) and 
                ((r.x.ctrl.inst[24 : 19] = FPOP1) or 
                 (r.x.ctrl.inst[24 : 19] = FPOP2))
          then fcnt := holdn; end if;
        end if;
      elsif ((not r.x.ctrl.annul) and xc_trap) = '1' then
        = '1'; xc_result  xc_exception =  r.x.ctrl.pc[31 : 2] & "00";
        = '1'; v.w.s.tt := xc_vectt; v.w.s.ps  xc_wreg =  r.w.s.s;
        v.w.s.s := '1'; v.x.annul_all := '1'; v.x.rstate := trap;
        xc_waddr := (others => '0');
        xc_waddr(NWINLOG2 + 3  downto 0) :=  r.w.s.cwp & "0001";
        v.x.npc := npc_find(r);
        fpexack(r, xc_fpexack);
        if r.w.s.et = '0' then
--        v.x.rstate := dsu1; = '0'; vp.error  xc_wreg =  '1';
          xc_wreg := '0';
        end if;
      end if;
    when trap =>
      = npc_gen(r); xc_wreg  xc_result =  '1';
      xc_waddr := (others => '0');
      xc_waddr(NWINLOG2 + 3  downto 0) :=  r.w.s.cwp & "0010";
      if r.w.s.et = '1' then
        v.w.s.et := '0'; v.x.rstate := run;
        if (not CWPOPT) and (r.w.s.cwp = CWPMIN) then v.w.s.cwp := CWPMAX;
        else v.w.s.cwp := r.w.s.cwp - 1 ; end if;
      else
        v.x.rstate := dsu1; = '0'; vp.error  xc_wreg =  '1';
      end if;
    when dsu1 =>
      = '1'; v.x.annul_all  xc_exception =  '1';
      xc_trap_address[31 : PCLOW] := r.f.pc;
      if DBGUNIT or PWRD2 or (smp /= 0)
      then 
        xc_trap_address[31 : PCLOW] := ir.addr; 
        vir.addr := npc_gen(r)[31 : PCLOW];
        v.x.rstate := dsu2;
      end if;
      if DBGUNIT then v.x.debug := r.x.debug; end if;
    when dsu2 =>      
      = '1'; v.x.annul_all  xc_exception =  '1';
      xc_trap_address[31 : PCLOW] := r.f.pc;
      if DBGUNIT or PWRD2 or (smp /= 0)
      then
        sidle := (rp.pwd or rp.error) and ico.idle and dco.idle and not r.x.debug;
        if DBGUNIT then
          if dbgi.reset = '1' then 
            if smp /=0 then vp.pwd := not irqi.run; else vp.pwd := '0'; end if;
            vp.error := '0';
          end if;
          if (dbgi.dsuen and dbgi.dbreak) = '1'then v.x.debug := '1'; end if;
          diagwr(r, dsur, ir, dbgi, wpr, v.w.s, vwpr, vdsu.asi, xc_trap_address,
               vir.addr, vdsu.tbufcnt, xc_wreg, xc_waddr, xc_result, fpcdbgwr);
          xc_halt := dbgi.halt;
        end if;
        if r.x.ipend = '1' then vp.pwd := '0'; end if;
        if (rp.error or rp.pwd or r.x.debug or xc_halt) = '0' then
          v.x.rstate := run; v.x.annul_all := '0'; vp.error := '0';
          xc_trap_address[31 : PCLOW] := ir.addr; v.x.debug := '0';
          vir.pwd := '1';
        end if;
        if (smp /= 0) and (irqi.rst = '1') then 
          vp.pwd := '0'; vp.error := '0'; 
        end if;
      end if;
    when others =>
    end case;

    dci.flushl <= xc_dflushl;

    
    irq_intack(r, holdn, v.x.intack);          
    itrace(r, dsur, vdsu, xc_result, xc_exception, dbgi, rp.error, xc_trap, tbufcntx, tbufi, '0', xc_dcperr);    
    vdsu.tbufcnt := tbufcntx;
    
    v.w.except := xc_exception; v.w.result := xc_result;
    if (r.x.rstate = dsu2) then v.w.except := '0'; end if;
    v.w.wa := xc_waddr[RFBITS-1 : 0]; v.w.wreg := xc_wreg and holdn;

    rfi.diag <= dco.testen & dco.scanen & "00";
    rfi.wdata <= xc_result; rfi.waddr <= xc_waddr;

    irqo.intack <= r.x.intack and holdn;
    irqo.irl <= r.w.s.tt[3 : 0];
    irqo.pwd <= rp.pwd;
    irqo.fpen <= r.w.s.ef;
    irqo.idle <= '0';
    dbgo.halt <= xc_halt;
    dbgo.pwd  <= rp.pwd;
    dbgo.idle <= sidle;
    dbgo.icnt <= icnt;
    dbgo.fcnt <= fcnt;
    dbgo.optype <= r.x.ctrl.inst[31 : 30] & r.x.ctrl.inst[24 : 21];
    dci.intack <= r.x.intack and holdn;    
    
    if (not RESET_ALL) and (xc_rstn = '0') then 
      v.w.except := RRES.w.except; v.w.s.et := RRES.w.s.et;
      v.w.s.svt := RRES.w.s.svt; v.w.s.dwt := RRES.w.s.dwt;
      v.w.s.ef := RRES.w.s.ef;
      if need_extra_sync_reset(fabtech) /= 0 then 
        v.w.s.cwp := RRES.w.s.cwp;
        v.w.s.icc := RRES.w.s.icc;
      end if;
      v.w.s.dbp := RRES.w.s.dbp;
      v.x.ipmask := RRES.x.ipmask;
      v.w.s.tba := RRES.w.s.tba;
      v.x.annul_all := RRES.x.annul_all;
      v.x.rstate := RRES.x.rstate; vir.pwd := IRES.pwd; 
      vp.pwd := PRES.pwd; v.x.debug := RRES.x.debug; 
      v.x.nerror := RRES.x.nerror;
      if svt = 1 then v.w.s.tt := RRES.w.s.tt; end if;
      if DBGUNIT then
        if (dbgi.dsuen and dbgi.dbreak) = '1' then
          v.x.rstate := dsu1; v.x.debug := '1';
        end if;
      end if;
      if (index /= 0) and (irqi.run = '0') and (rstn = '0') then 
        v.x.rstate := dsu1; vp.pwd := '1'; 
      end if;
      v.x.npc := "100";
    end if;
    
    -- kill off unused regs
    if not FPEN then v.w.s.ef := '0'; end if;
    if not CPEN then v.w.s.ec := '0'; end if;

    
-- MEMORY STAGE

    v.x.ctrl := r.m.ctrl; v.x.dci := r.m.dci;
    v.x.ctrl.rett := r.m.ctrl.rett and not r.m.ctrl.annul;
    v.x.mac := r.m.mac; v.x.laddr := r.m.result[1 : 0];
    v.x.ctrl.annul := r.m.ctrl.annul or v.x.annul_all; 
    st := '0'; 
    
    if CASAEN and (r.m.casa = '1') and (r.m.ctrl.cnt = "00") then
      v.x.ctrl.inst[4 : 0] := r.a.ctrl.inst[4 : 0]; -- restore rs2 for trace log
    end if;

    mul_res(r, v.w.s.asr18, v.x.result, v.x.y, me_asr18, me_icc);


    mem_trap(r, wpr, v.x.ctrl.annul, holdn, v.x.ctrl.trap, me_iflush,
             me_nullify, v.m.werr, v.x.ctrl.tt);
    me_newtt := v.x.ctrl.tt;

    irq_trap(r, ir, irqi.irl, v.x.ctrl.annul, v.x.ctrl.pv, v.x.ctrl.trap, me_newtt, me_nullify,
             v.m.irqen, v.m.irqen2, me_nullify2, v.x.ctrl.trap,
             v.x.ipend, v.x.ctrl.tt);   

      
    if (r.m.ctrl.ld or st or not dco.mds) = '1' then          
      for i in 0 to dsets-1 loop
        v.x.data(i) := dco.data(i);
      end loop;
      v.x.set := dco.set[DSETMSB : 0]; 
      if dco.mds = '0' then
        me_size := r.x.dci.size; = r.x.laddr; me_signed  me_laddr =  r.x.dci.signed;
      else
        me_size := v.x.dci.size; = v.x.laddr; me_signed  me_laddr =  v.x.dci.signed;
      end if;
      if (lddel /= 2) then
        v.x.data(0) := ld_align(v.x.data, v.x.set, me_size, me_laddr, me_signed);
      end if;
    end if;
    if (not RESET_ALL) and (is_fpga(fabtech) = 0) and (xc_rstn = '0') then
      v.x.data := (others => (others => '0')); --v.x.ldc := '0';
    end if;
    v.x.mexc := dco.mexc;

    v.x.icc := me_icc;
    v.x.ctrl.wicc := r.m.ctrl.wicc and not v.x.annul_all;
    
    if MACEN and ((v.x.ctrl.annul or v.x.ctrl.trap) = '0') then
      v.w.s.asr18 := me_asr18;
    end if;

    if (r.x.rstate = dsu2)
    then      
      = '0'; v.x.set  me_nullify2 =  dco.set[DSETMSB : 0];
    end if;


    if (not RESET_ALL) and (xc_rstn = '0') then 
        v.x.ctrl.trap := '0'; v.x.ctrl.annul := '1';
    end if;
    
      dci.maddress <= r.m.result;
    dci.enaddr   <= r.m.dci.enaddr;
    dci.asi      <= r.m.dci.asi;
    dci.size     <= r.m.dci.size;
    dci.lock     <= (r.m.dci.lock and not r.m.ctrl.annul);
    dci.read     <= r.m.dci.read;
    dci.write    <= r.m.dci.write;
    dci.flush    <= me_iflush;
    dci.dsuen    <= r.m.dci.dsuen;
    dci.msu    <= r.m.su;
    dci.esu    <= r.e.su;
    dbgo.ipend <= v.x.ipend;
    
-- EXECUTE STAGE

    v.m.ctrl := r.e.ctrl; = r.e.op1; ex_op2  ex_op1 =  r.e.op2;
    v.m.ctrl.rett := r.e.ctrl.rett and not r.e.ctrl.annul;
    v.m.ctrl.wreg := r.e.ctrl.wreg and not v.x.annul_all;
    ex_ymsb := r.e.ymsb; = ex_op2; ex_shcnt  mul_op2 =  r.e.shcnt;
    v.e.cwp := r.a.cwp; ex_sari := r.e.sari;
    v.m.su := r.e.su;
    if MULTYPE = 3 then v.m.mul := r.e.mul; else v.m.mul := '0'; end if;
    if lddel = 1 then
      if r.e.ldbp1 = '1' then 
        ex_op1 := r.x.data(0); 
        ex_sari := r.x.data(0)(31) and r.e.ctrl.inst(19) and r.e.ctrl.inst(20);
      end if;
      if r.e.ldbp2 = '1' then 
        = r.x.data(0); ex_ymsb  ex_op2 =  r.x.data(0)(0); 
        = ex_op2; ex_shcnt  mul_op2 =  r.x.data(0)[4 : 0];
        if r.e.invop2 = '1' then 
          = not ex_op2; ex_shcnt  ex_op2 =  not ex_shcnt;
        end if;
      end if;
    end if;


    ex_add_res := (ex_op1 & '1') + (ex_op2 & r.e.alucin);

    if ex_add_res[2 : 1] = "00" then v.m.nalign := '0';
    else v.m.nalign := '1'; end if;

    dcache_gen(r, v, ex_dci, ex_link_pc, ex_jump, ex_force_a2, ex_load, v.m.casa);
    ex_jump_address := ex_add_res(32 downto PCLOW+1);
    logic_op(r, ex_op1, ex_op2, v.x.y, ex_ymsb, ex_logic_res, v.m.y);
    ex_shift_res := shift(r, ex_op1, ex_op2, ex_shcnt, ex_sari);
    misc_op(r, wpr, ex_op1, ex_op2, xc_df_result, v.x.y, ex_misc_res, ex_edata);
    ex_add_res(3):= ex_add_res(3) or ex_force_a2;    
    alu_select(r, ex_add_res, ex_op1, ex_op2, ex_shift_res, ex_logic_res,
        ex_misc_res, ex_result, me_icc, v.m.icc, v.m.divz, v.m.casaz);    
    dbg_cache(holdn, dbgi, r, dsur, ex_result, ex_dci, ex_result2, v.m.dci);
    fpstdata(r, ex_edata, ex_result2, fpo.data, ex_edata2, ex_result3);
    v.m.result := ex_result3;
    cwp_ex(r, v.m.wcwp);    

    if CASAEN and (r.e.ctrl.cnt = "10") and ((r.m.casa and not v.m.casaz) = '1') then
      me_nullify2 := '1';
    end if;
    dci.nullify  <= me_nullify2;

    ex_mulop1 := (ex_op1(31) and r.e.ctrl.inst(19)) & ex_op1;
    ex_mulop2 := (mul_op2(31) and r.e.ctrl.inst(19)) & mul_op2;

    if is_fpga(fabtech) = 0 and (r.e.mul = '0') then     -- power-save for mul
--    if (r.e.mul = '0') then
        = (others => '0'); ex_mulop2  ex_mulop1 =  (others => '0');
    end if;

      
    v.m.ctrl.annul := v.m.ctrl.annul or v.x.annul_all;
    v.m.ctrl.wicc := r.e.ctrl.wicc and not v.x.annul_all; 
    v.m.mac := r.e.mac;
    if (DBGUNIT and (r.x.rstate = dsu2)) then v.m.ctrl.ld := '1'; end if;
    dci.eenaddr  <= v.m.dci.enaddr;
    dci.eaddress <= ex_add_res[32 : 1];
    dci.edata <= ex_edata2;
    bp_miss_ex(r, r.m.icc, ex_bpmiss, ra_bpannul);
    
-- REGFILE STAGE

    v.e.ctrl := r.a.ctrl; v.e.jmpl := r.a.jmpl and not r.a.ctrl.trap;
    v.e.ctrl.annul := r.a.ctrl.annul or ra_bpannul or v.x.annul_all;
    v.e.ctrl.rett := r.a.ctrl.rett and not r.a.ctrl.annul and not r.a.ctrl.trap;
    v.e.ctrl.wreg := r.a.ctrl.wreg and not (ra_bpannul or v.x.annul_all);    
    v.e.su := r.a.su; v.e.et := r.a.et;
    v.e.ctrl.wicc := r.a.ctrl.wicc and not (ra_bpannul or v.x.annul_all);
    v.e.rfe1 := r.a.rfe1; v.e.rfe2 := r.a.rfe2;
    
    exception_detect(r, wpr, dbgi, r.a.ctrl.trap, r.a.ctrl.tt, 
                     v.e.ctrl.trap, v.e.ctrl.tt);
    op_mux(r, rfo.data1, ex_result3, v.x.result, xc_df_result, zero32, 
        r.a.rsel1, v.e.ldbp1, ra_op1, '0');
    op_mux(r, rfo.data2,  ex_result3, v.x.result, xc_df_result, r.a.imm, 
        r.a.rsel2, ex_ldbp2, ra_op2, '1');
    alu_op(r, ra_op1, ra_op2, v.m.icc, v.m.y(0), ex_ldbp2, v.e.op1, v.e.op2,
           v.e.aluop, v.e.alusel, v.e.aluadd, v.e.shcnt, v.e.sari, v.e.shleft,
           v.e.ymsb, v.e.mul, ra_div, v.e.mulstep, v.e.mac, v.e.ldbp2, v.e.invop2
    );
    cin_gen(r, v.m.icc(0), v.e.alucin);
    bp_miss_ra(r, ra_bpmiss, de_bpannul);
    v.e.bp := r.a.bp and not ra_bpmiss;
    
-- DECODE STAGE

    if ISETS > 1 then de_inst := r.d.inst(conv_integer(r.d.set));
    else de_inst := r.d.inst(0); end if;

    = r.m.icc; v.a.cwp  de_icc =  r.d.cwp;
    su_et_select(r, v.w.s.ps, v.w.s.s, v.w.s.et, v.a.su, v.a.et);
    wicc_y_gen(de_inst, v.a.ctrl.wicc, v.a.ctrl.wy);
    cwp_ctrl(r, v.w.s.wim, de_inst, de_cwp, v.a.wovf, v.a.wunf, de_wcwp);
    if CASAEN and (de_inst[31 : 30] = LDST) and (de_inst[24 : 19] = CASA) then
      case r.d.cnt is
      when "00" | "01" => de_inst[4 : 0] := "00000"; -- rs2=0
      when others =>
      end case;
    end if;
    rs1_gen(r, de_inst, v.a.rs1, de_rs1mod); 
    de_rs2 := de_inst[4 : 0];
    = (others => '0'); de_raddr2  de_raddr1 =  (others => '0');
    
    if RS1OPT then
      if de_rs1mod = '1' then
        regaddr(r.d.cwp, de_inst[29 : 26] & v.a.rs1(0), de_raddr1[RFBITS-1 : 0]);
      else
        regaddr(r.d.cwp, de_inst[18 : 15] & v.a.rs1(0), de_raddr1[RFBITS-1 : 0]);
      end if;
    else
      regaddr(r.d.cwp, v.a.rs1, de_raddr1[RFBITS-1 : 0]);
    end if;
    regaddr(r.d.cwp, de_rs2, de_raddr2[RFBITS-1 : 0]);
    v.a.rfa1 := de_raddr1[RFBITS-1 : 0]; 
    v.a.rfa2 := de_raddr2[RFBITS-1 : 0]; 

    rd_gen(r, de_inst, v.a.ctrl.wreg, v.a.ctrl.ld, de_rd);
    regaddr(de_cwp, de_rd, v.a.ctrl.rd);
    
    fpbranch(de_inst, fpo.cc, de_fbranch);
    fpbranch(de_inst, cpo.cc, de_cbranch);
    v.a.imm := imm_data(r, de_inst);
      de_iperr := '0';
    lock_gen(r, de_rs2, de_rd, v.a.rfa1, v.a.rfa2, v.a.ctrl.rd, de_inst, 
        fpo.ldlock, v.e.mul, ra_div, de_wcwp, v.a.ldcheck1, v.a.ldcheck2, de_ldlock, 
        v.a.ldchkra, v.a.ldchkex, v.a.bp, v.a.nobp, de_fins_hold, de_iperr);
    ic_ctrl(r, de_inst, v.x.annul_all, de_ldlock, branch_true(de_icc, de_inst), 
        de_fbranch, de_cbranch, fpo.ccv, cpo.ccv, v.d.cnt, v.d.pc, de_branch,
        v.a.ctrl.annul, v.d.annul, v.a.jmpl, de_inull, v.d.pv, v.a.ctrl.pv,
        de_hold_pc, v.a.ticc, v.a.ctrl.rett, v.a.mulstart, v.a.divstart, 
        ra_bpmiss, ex_bpmiss, de_iperr);

    v.a.bp := v.a.bp and not v.a.ctrl.annul;
    v.a.nobp := v.a.nobp and not v.a.ctrl.annul;

    v.a.ctrl.inst := de_inst;

    cwp_gen(r, v, v.a.ctrl.annul, de_wcwp, de_cwp, v.d.cwp);    
    
    v.d.inull := ra_inull_gen(r, v);
    
    op_find(r, v.a.ldchkra, v.a.ldchkex, v.a.rs1, v.a.rfa1, 
            false, v.a.rfe1, v.a.rsel1, v.a.ldcheck1);
    op_find(r, v.a.ldchkra, v.a.ldchkex, de_rs2, v.a.rfa2, 
            imm_select(de_inst), v.a.rfe2, v.a.rsel2, v.a.ldcheck2);


    v.a.ctrl.wicc := v.a.ctrl.wicc and (not v.a.ctrl.annul) 
    ;
    v.a.ctrl.wreg := v.a.ctrl.wreg and (not v.a.ctrl.annul) 
    ;
    v.a.ctrl.rett := v.a.ctrl.rett and (not v.a.ctrl.annul) 
    ;
    v.a.ctrl.wy := v.a.ctrl.wy and (not v.a.ctrl.annul) 
    ;

    v.a.ctrl.trap := r.d.mexc 
    ;
    v.a.ctrl.tt := "000000";
      if r.d.mexc = '1' then
        v.a.ctrl.tt := "000001";
      end if;
    v.a.ctrl.pc := r.d.pc;
    v.a.ctrl.cnt := r.d.cnt;
    v.a.step := r.d.step;
    
    if holdn = '0' then 
      de_raddr1[RFBITS-1 : 0] := r.a.rfa1;
      de_raddr2[RFBITS-1 : 0] := r.a.rfa2;
      = r.a.rfe1; de_ren2  de_ren1 =  r.a.rfe2;
    else
      = v.a.rfe1; de_ren2  de_ren1 =  v.a.rfe2;
    end if;

    if DBGUNIT then
      if (dbgi.denable = '1') and (r.x.rstate = dsu2) then        
        de_raddr1[RFBITS-1 : 0] := dbgi.daddr(RFBITS+1 downto 2); de_ren1 := '1';
        = de_raddr1; de_ren2  de_raddr2 =  '1';
      end if;
      v.d.step := dbgi.step and not r.d.annul;      
    end if;

    rfi.wren <= (xc_wreg and holdn) and not dco.scanen;
    rfi.raddr1 <= de_raddr1; rfi.raddr2 <= de_raddr2;
    rfi.ren1 <= de_ren1 and not dco.scanen;
    rfi.ren2 <= de_ren2 and not dco.scanen;
    ici.inull <= de_inull
    ;
    ici.flush <= me_iflush;
    v.d.divrdy := divo.nready;
    ici.fline <= r.x.ctrl.pc[31 : 3];
    dbgo.bpmiss <= bpmiss and holdn;
    if (xc_rstn = '0') then
      v.d.cnt := (others => '0');
      if need_extra_sync_reset(fabtech) /= 0 then 
        v.d.cwp := (others => '0');
      end if;
    end if;

-- FETCH STAGE

    bpmiss := ex_bpmiss or ra_bpmiss;
    = r.f.pc; fe_pc  npc =  r.f.pc;
    if ra_bpmiss = '1' then fe_pc := r.d.pc; end if;
    if ex_bpmiss = '1' then fe_pc := r.a.ctrl.pc; end if;
    fe_npc := zero32[31 : PCLOW];
    fe_npc[31 : 2] := fe_pc[31 : 2] + 1;    -- Address incrementer

    if (xc_rstn = '0') then
      if (not RESET_ALL) then 
        v.f.pc := (others => '0'); v.f.branch := '0';
        if DYNRST then v.f.pc[31 : 12] := irqi.rstvec;
        else
          v.f.pc[31 : 12] :=  20'(rstaddr);
        end if;
      end if;
    elsif xc_exception = '1' then       -- exception
      v.f.branch := '1'; v.f.pc := xc_trap_address;
      npc := v.f.pc;
    elsif de_hold_pc = '1' then
      v.f.pc := r.f.pc; v.f.branch := r.f.branch;
      if bpmiss = '1' then
        v.f.pc := fe_npc; v.f.branch := '1';
        npc := v.f.pc;
      elsif ex_jump = '1' then
        v.f.pc := ex_jump_address; v.f.branch := '1';
        npc := v.f.pc;
      end if;
    elsif (ex_jump and not bpmiss) = '1' then
      v.f.pc := ex_jump_address; v.f.branch := '1';
      npc := v.f.pc;
    elsif (de_branch and not bpmiss
        ) = '1'
    then
      v.f.pc := branch_address(de_inst, r.d.pc); v.f.branch := '1';
      npc := v.f.pc;
    else
      v.f.branch := bpmiss; v.f.pc := fe_npc; npc := v.f.pc;
    end if;

    ici.dpc <= r.d.pc[31 : 2] & "00";
    ici.fpc <= r.f.pc[31 : 2] & "00";
    ici.rpc <= npc[31 : 2] & "00";
    ici.fbranch <= r.f.branch;
    ici.rbranch <= v.f.branch;
    ici.su <= v.a.su;

    
    if (ico.mds and de_hold_pc) = '0' then
      for i in 0 to isets-1 loop
        v.d.inst(i) := ico.data(i);                     -- latch instruction
      end loop;
      v.d.set := ico.set[ISETMSB : 0];             -- latch instruction
      v.d.mexc := ico.mexc;                             -- latch instruction

    end if;


    if DBGUNIT then -- DSU diagnostic read    
      diagread(dbgi, r, dsur, ir, wpr, dco, tbo, diagdata);
      diagrdy(dbgi.denable, dsur, r.m.dci, dco.mds, ico, vdsu.crdy);
    end if;
    
-- OUTPUTS

    rin <= v; wprin <= vwpr; dsuin <= vdsu; irin <= vir;
    muli.start <= r.a.mulstart and not r.a.ctrl.annul and 
        not r.a.ctrl.trap and not ra_bpannul;
    muli.signed <= r.e.ctrl.inst(19);
    muli.op1 <= ex_mulop1; --(ex_op1(31) and r.e.ctrl.inst(19)) & ex_op1;
    muli.op2 <= ex_mulop2; --(mul_op2(31) and r.e.ctrl.inst(19)) & mul_op2;
    muli.mac <= r.e.ctrl.inst(24);
    if MACPIPE then muli.acc[39 : 32] <= r.w.s.y[7 : 0];
    else muli.acc[39 : 32] <= r.x.y[7 : 0]; end if;
    muli.acc[31 : 0] <= r.w.s.asr18;
    muli.flush <= r.x.annul_all;
    divi.start <= r.a.divstart and not r.a.ctrl.annul and 
        not r.a.ctrl.trap and not ra_bpannul;
    divi.signed <= r.e.ctrl.inst(19);
    divi.flush <= r.x.annul_all;
    divi.op1 <= (ex_op1(31) and r.e.ctrl.inst(19)) & ex_op1;
    divi.op2 <= (ex_op2(31) and r.e.ctrl.inst(19)) & ex_op2;
    if (r.a.divstart and not r.a.ctrl.annul) = '1' then 
      dsign :=  r.a.ctrl.inst(19);
    else dsign := r.e.ctrl.inst(19); end if;
    divi.y <= (r.m.y(31) and dsign) & r.m.y;
    rpin <= vp;

    if DBGUNIT then
      dbgo.dsu <= '1'; dbgo.dsumode <= r.x.debug; dbgo.crdy <= dsur.crdy(2);
      dbgo.data <= diagdata;
      if TRACEBUF then tbi <= tbufi; else
        tbi.addr <= (others => '0'); tbi.data <= (others => '0');
        tbi.enable <= '0'; tbi.write <= (others => '0'); tbi.diag <= "0000";
      end if;
    else
      dbgo.dsu <= '0'; dbgo.data <= (others => '0'); dbgo.crdy  <= '0';
      dbgo.dsumode <= '0'; tbi.addr <= (others => '0'); 
      tbi.data <= (others => '0'); tbi.enable <= '0';
      tbi.write <= (others => '0'); tbi.diag <= "0000";
    end if;
    dbgo.error <= dummy and not r.x.nerror;
    dbgo.wbhold <= '0'; --dco.wbhold;
    dbgo.su <= r.w.s.s;
    dbgo.istat <= ('0', '0', '0', '0');
    dbgo.dstat <= ('0', '0', '0', '0');


    if FPEN then
      if (r.x.rstate = dsu2) then vfpi.flush := '1'; else vfpi.flush := v.x.annul_all and holdn; end if;
      vfpi.exack := xc_fpexack; vfpi.a_rs1 := r.a.rs1; vfpi.d.inst := de_inst;
      vfpi.d.cnt := r.d.cnt;
      vfpi.d.annul := v.x.annul_all or de_bpannul or r.d.annul or de_fins_hold
        ;
      vfpi.d.trap := r.d.mexc;
      vfpi.d.pc[1 : 0] := (others => '0'); vfpi.d.pc[31 : PCLOW] := r.d.pc[31 : PCLOW]; 
      vfpi.d.pv := r.d.pv;
      vfpi.a.pc[1 : 0] := (others => '0'); vfpi.a.pc[31 : PCLOW] := r.a.ctrl.pc[31 : PCLOW]; 
      vfpi.a.inst := r.a.ctrl.inst; vfpi.a.cnt := r.a.ctrl.cnt; vfpi.a.trap := r.a.ctrl.trap;
      vfpi.a.annul := r.a.ctrl.annul or (ex_bpmiss and r.e.ctrl.inst(29))
        ;
      vfpi.a.pv := r.a.ctrl.pv;
      vfpi.e.pc[1 : 0] := (others => '0'); vfpi.e.pc[31 : PCLOW] := r.e.ctrl.pc[31 : PCLOW]; 
      vfpi.e.inst := r.e.ctrl.inst; vfpi.e.cnt := r.e.ctrl.cnt; vfpi.e.trap := r.e.ctrl.trap; vfpi.e.annul := r.e.ctrl.annul;
      vfpi.e.pv := r.e.ctrl.pv;
      vfpi.m.pc[1 : 0] := (others => '0'); vfpi.m.pc[31 : PCLOW] := r.m.ctrl.pc[31 : PCLOW]; 
      vfpi.m.inst := r.m.ctrl.inst; vfpi.m.cnt := r.m.ctrl.cnt; vfpi.m.trap := r.m.ctrl.trap; vfpi.m.annul := r.m.ctrl.annul;
      vfpi.m.pv := r.m.ctrl.pv;
      vfpi.x.pc[1 : 0] := (others => '0'); vfpi.x.pc[31 : PCLOW] := r.x.ctrl.pc[31 : PCLOW]; 
      vfpi.x.inst := r.x.ctrl.inst; vfpi.x.cnt := r.x.ctrl.cnt; vfpi.x.trap := xc_trap;
      vfpi.x.annul := r.x.ctrl.annul; vfpi.x.pv := r.x.ctrl.pv;
      if (lddel = 2) then vfpi.lddata := r.x.data(conv_integer(r.x.set)); else vfpi.lddata := r.x.data(0); end if;
      if (r.x.rstate = dsu2)
      then vfpi.dbg.enable := dbgi.denable;
      else vfpi.dbg.enable := '0'; end if;      
      vfpi.dbg.write := fpcdbgwr;
      vfpi.dbg.fsr := dbgi.daddr(22); -- IU reg access
      vfpi.dbg.addr := dbgi.daddr[6 : 2];
      vfpi.dbg.data := dbgi.ddata;      
      fpi <= vfpi;
      cpi <= vfpi;      -- dummy, just to kill some warnings ...
    end if;

  end process;

  preg : process (sclk)
  begin 
    if rising_edge(sclk) then 
      rp <= rpin;
      if rstn = '0' then
        rp.error <= PRES.error;
        if RESET_ALL then
          if (index /= 0) and (irqi.run = '0') then
            rp.pwd <= '1';
          else
            rp.pwd <= '0';
          end if;
        end if;
      end if;
    end if;
  end process;

  reg : process (clk)
  begin
    if rising_edge(clk) then
      if (holdn = '1') then
        r <= rin;
      else
        r.x.ipend <= rin.x.ipend;
        r.m.werr <= rin.m.werr;
        if (holdn or ico.mds) = '0' then
          r.d.inst <= rin.d.inst; r.d.mexc <= rin.d.mexc; 
          r.d.set <= rin.d.set;
        end if;
        if (holdn or dco.mds) = '0' then
          r.x.data <= rin.x.data; r.x.mexc <= rin.x.mexc; 
          r.x.set <= rin.x.set;
        end if;
      end if;
      if rstn = '0' then
        if RESET_ALL then
          r <= RRES;
          if DYNRST then
            r.f.pc[31 : 12] <= irqi.rstvec;
            r.w.s.tba <= irqi.rstvec;
          end if;
          if DBGUNIT then
            if (dbgi.dsuen and dbgi.dbreak) = '1' then
              r.x.rstate <= dsu1; r.x.debug <= '1';
            end if;
          end if;
          if (index /= 0) and irqi.run = '0' then
            r.x.rstate <= dsu1;
          end if;
        else  
          r.w.s.s <= '1'; r.w.s.ps <= '1'; 
          if need_extra_sync_reset(fabtech) /= 0 then 
            r.d.inst <= (others => (others => '0'));
            r.x.mexc <= '0';
          end if; 
        end if;
      end if; 
    end if;
  end process;


  dsugen : if DBGUNIT generate
    dsureg : process(clk) begin
      if rising_edge(clk) then 
        if holdn = '1' then
          dsur <= dsuin;
        else
          dsur.crdy <= dsuin.crdy;
        end if;
        if rstn = '0' then
          if RESET_ALL then
            dsur <= DRES;
          elsif need_extra_sync_reset(fabtech) /= 0 then
            dsur.err <= '0'; dsur.tbufcnt <= (others => '0'); dsur.tt <= (others => '0');
            dsur.asi <= (others => '0'); dsur.crdy <= (others => '0');
          end if;
        end if;
      end if;
    end process;
  end generate;

  nodsugen : if not DBGUNIT generate
    dsur.err <= '0'; dsur.tbufcnt <= (others => '0'); dsur.tt <= (others => '0');
    dsur.asi <= (others => '0'); dsur.crdy <= (others => '0');
  end generate;

  irreg : if DBGUNIT or PWRD2
  generate
    dsureg : process(clk) begin
      if rising_edge(clk) then 
        if holdn = '1' then ir <= irin; end if;
        if RESET_ALL and rstn = '0' then ir <= IRES; end if;
      end if;
    end process;
  end generate;

  nirreg : if not (DBGUNIT or PWRD2
    )
  generate
    ir.pwd <= '0'; ir.addr <= (others => '0');
  end generate;
  
  wpgen : for i in 0 to 3 generate
    wpg0 : if nwp > i generate
      wpreg : process(clk) begin
        if rising_edge(clk) then
          if holdn = '1' then wpr(i) <= wprin(i); end if;
          if rstn = '0' then
            if RESET_ALL then
              wpr(i) <= wpr_none;
            else
              wpr(i).exec <= '0'; wpr(i).load <= '0'; wpr(i).store <= '0';
            end if;
          end if;
        end if;
      end process;
    end generate;
    wpg1 : if nwp <= i generate
      wpr(i) <= wpr_none;
    end generate;
  end generate;

-- pragma translate_off
  trc : process(clk)
    variable valid :  bit  ;
    variable op :  logic  [1 : 0];
    variable op3 :  logic  [5 : 0];
    variable fpins, fpld :  bit  ;    
  begin
    if (fpu /= 0) then
      = r.x.ctrl.inst[31 : 30]; op3  op =  r.x.ctrl.inst[24 : 19];
      fpins := (op = FMT3) and ((op3 = FPOP1) or (op3 = FPOP2));
      fpld := (op = LDST) and ((op3 = LDF) or (op3 = LDDF) or (op3 = LDFSR));
    else
      = false; fpld  fpins =  false;
    end if;
      valid := (((not r.x.ctrl.annul) and r.x.ctrl.pv) = '1') and (not ((fpins or fpld) and (r.x.ctrl.trap = '0')));
      valid := valid and (holdn = '1');
    if (disas = 1) and rising_edge(clk) and (rstn = '1') then
      print_insn (index, r.x.ctrl.pc[31 : 2] & "00", r.x.ctrl.inst, 
                  rin.w.result, valid, r.x.ctrl.trap = '1', rin.w.wreg = '1',
        rin.x.ipmask = '1');
    end if;
  end process;
-- pragma translate_on

  dis0 : if disas < 2 generate dummy <= '1'; end generate;

  dis2 : if disas > 1 generate
      disasen <= '1' when disas /= 0 else '0';
      cpu_index <=  4'(index);
      x0 : cpu_disasx
      port map (clk, rstn, dummy, r.x.ctrl.inst, r.x.ctrl.pc[31 : 2],
        rin.w.result, cpu_index, rin.w.wreg, r.x.ctrl.annul, holdn,
        r.x.ctrl.pv, r.x.ctrl.trap, disasen);
  end generate;

end;
