///////////////////////////////////////////////////////////////////////////////
 // 版权 （C） 2015 -2015 ，北京蒙芯科技有限公司
 //本程序是自由软件；您可以重新分配和/或修改在GNU通用公共许可证的条款公布 
 //自由软件基金会；无论是GPL v2版的许可证，或（在你选择的任何版本）。
 // 
 //这个程序是分布在希望它是有用的，但没有任何担保；甚至没有暗示保证 
 //适销性或针对特定用途的。看到GNU通用公共许可证的更多细节。
 // 
 //你应该已经收到一份GNU通用公共许可证,随着这个程序；如果没有，写信给自由软件基金会 
 // 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
  /////////////////////////////////////////////////////////////////////////////// 
  
 
 /////////////////////////////////////////////////////////////////////////////// 
 // 编写人： 薛晓军  
 // 描述 ： LEON3 7-stage integer pipline 流水线 
 ///////////////////////////////////////////////////////////////////////////////  
 //
//
//

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library grlib;
use grlib.config_types.all;
use grlib.config.all;
use grlib.sparc.all;
use grlib.stdlib.all;
library techmap;
use techmap.gencomp.all;
library gaisler;
use gaisler.leon3.all;
use gaisler.libiu.all;
use gaisler.libfpu.all;
use gaisler.arith.all;
// pragma translate_off
use grlib.sparc_disas.all;
// pragma translate_on

module iu3
#(
parameter     integer  nwin =   8, // range 2 to 32
parameter     integer  isets =   1, // range 1 to 4
parameter     integer  dsets =   1, // range 1 to 4
parameter     integer  fpu =   0, // range 0 to 15
parameter     integer  v8 =   0, // range 0 to 63
parameter    cp,  integer  mac =   0, // range 0 to 1
parameter     integer  dsu =   0, // range 0 to 1
parameter     integer  nwp =   0, // range 0 to 4
parameter     integer  pclow =   2, // range 0 to 2
parameter     integer  notag =   0, // range 0 to 1
parameter     integer  index =   0, // range 0 to 15
parameter     integer  lddel =   2, // range 1 to 2
parameter     integer  irfwt =   0, // range 0 to 1
parameter     integer  disas =   0, // range 0 to 2
parameter     integer  tbuf =   0,  // trace buf size in kB (0 - no trace buffer) // range 0 to 64
parameter     integer  pwd =   0,   // power-down // range 0 to 2
parameter     integer  svt =   0,   // single-vector trapping // range 0 to 1
parameter     integer  rstaddr =  16'h00000,   // reset vector MSB address
parameter     integer  smp =   0,  // support SMP systems // range 0 to 15
parameter     integer  fabtech =   0,     // range 0 to NTECH
parameter     integer  clk2x =  0,
parameter     integer  bp =   1 // range 0 to 2
  )
 (
 input    logic       clk    ,
 input    logic       rstn   ,
 input    logic                 holdn  ,
 output icache_input _type      ici    ,
 input  icache_output_type      ico    ,
 output dcache_input _type      dci    ,
 input  dcache_output_type      dco    ,
 output iregfile_input _type    rfi    ,
 input  iregfile_output_typ     rfo    ,
 input  l3_irq_input _type      irqi   ,
 output l3_irq_output_type      irqo   ,
 input  l3_debug_input _type    dbgi   ,
 output l3_debug_output_type    dbgo   ,
 output mul32_input _type       muli   ,
 input  mul32_output_type       mulo   ,
 output div32_input _type       divi   ,
 input  div32_output_type       divo   ,
 input  fpc_output_type         fpo    ,
 output fpc_input _type         fpi    ,
 input  fpc_output_type         cpo    ,
 output fpc_input _type         cpi    ,
 input  tracebuf_output_type    tbo    ,
 output tracebuf_input _type    tbi    ,
 input  logic                   sclk    
    );


  //attribute sync_set_reset of rstn : signal is "true"; 
end;

//architecture rtl of iu3 ;

   localparam    integer  ISETMSB =  log2x[isets]-1;
   localparam    integer  DSETMSB =  log2x[dsets]-1;
   localparam    integer  RFBITS =   log2(NWIN+1) + 4; // range 6 to 10
   localparam    integer  NWINLOG2 =   log2[NWIN]; // range 1 to 5
   localparam     bit    CWPOPT =  (NWIN == (2**NWINLOG2));
   localparam     logic  [NWINLOG2-1 : 0]  CWPMIN =   `0 ;
   localparam     logic  [NWINLOG2-1 : 0]  CWPMAX =  
         NWINLOG2'(NWIN-1);
   localparam     bit    FPEN =  (fpu != 0);
   localparam     bit    CPEN =  (cp == 1);
   localparam     bit    MULEN =  (v8 != 0);
   localparam    integer  MULTYPE =  (v8 / 16);
   localparam     bit    DIVEN =  (v8 != 0);
   localparam     bit    MACEN =  (mac == 1);
   localparam     bit    MACPIPE =  (mac == 1) && (v8/2 == 1);
   localparam    integer  IMPL =  15;
   localparam    integer  VER =  3;
   localparam     bit    DBGUNIT =  (dsu == 1);
   localparam     bit    TRACEBUF =  (tbuf != 0);
   localparam    integer  TBUFBITS =  10 + log2[tbuf] - 4;
   localparam     bit    PWRD1 =  false; //(pwd == 1) &&  !(index != 0);
   localparam     bit    PWRD2 =  (pwd != 0); //(pwd == 2) || (index != 0);
   localparam     bit    RS1OPT =  (is_fpga[FABTECH] != 0);
   localparam     bit    DYNRST =  (rstaddr == 16#FFFFF#);

   localparam     bit    CASAEN =  (notag == 0) && (lddel == 1);
  signal std_logic BPRED ;

  subtype word is  logic  [31 : 0];
  subtype pctype is  logic  [31 : PCLOW];
  subtype rfatype is  logic  [RFBITS-1 : 0];
  subtype cwptype is  logic  [NWINLOG2-1 : 0];
  type icdtype is array (0 to isets-1) of word;
  type dcdtype is array (0 to dsets-1) of word;
  
  
typedef struct {
  logic       signed, enaddr, read, write, lock, dsuen  ;
  logic  [1 : 0]     size  ;
  logic  [7 : 0]     asi   ;    
 } dc_in_type;
  
typedef struct {
 pctype     pc     ;
 word     inst   ;
  logic  [1 : 0]     cnt    ;
 rfatype     rd     ;
  logic  [5 : 0]     tt     ;
  logic       trap   ;
  logic       annul  ;
  logic       wreg   ;
  logic       wicc   ;
  logic       wy     ;
  logic       ld     ;
  logic       pv     ;
  logic       rett   ;
 } pipeline_ctrl_type;
  
typedef struct {
 pctype     pc      ;
  logic       branch  ;
 } fetch_reg_type;
  
typedef struct {
 pctype     pc     ;
 icdtype     inst   ;
 cwptype     cwp    ;
  logic  [ISETMSB : 0]     set    ;
  logic       mexc   ;
  logic  [1 : 0]     cnt    ;
  logic       pv     ;
  logic       annul  ;
  logic       inull  ;
  logic       step   ;
  logic       divrdy ;
 } decode_reg_type;
  
typedef struct {
 pipeline_ctrl_type     ctrl   ;
  logic  [4 : 0]     rs1    ;
 rfatype     rfa1, rfa2  ;
  logic  [2 : 0]     rsel1, rsel2  ;
  logic       rfe1, rfe2  ;
 cwptype     cwp    ;
 word     imm    ;
  logic       ldcheck1  ;
  logic       ldcheck2  ;
  logic       ldchkra  ;
  logic       ldchkex  ;
  logic       su  ;
  logic       et  ;
  logic       wovf  ;
  logic       wunf  ;
  logic       ticc  ;
  logic       jmpl  ;
  logic       step   ;            
  logic       mulstart  ;            
  logic       divstart  ;
  logic       bp, nobp  ;
 } regacc_reg_type;
  
typedef struct {
 pipeline_ctrl_type     ctrl    ;
 word     op1     ;
 word     op2     ;
  logic  [2 : 0]     aluop   ;      // Alu operation
  logic  [1 : 0]     alusel  ;      // Alu result select
  logic       aluadd  ;
  logic       alucin  ;
  logic       ldbp1, ldbp2  ;
  logic       invop2  ;
  logic  [4 : 0]     shcnt   ;      // shift count
  logic       sari    ;                                // shift msb
  logic       shleft  ;                                // shift left/right
  logic       ymsb    ;                                // shift left/right
  logic  [4 : 0]     rd      ;
  logic       jmpl    ;
  logic       su      ;
  logic       et      ;
 cwptype     cwp     ;
  logic  [3 : 0]     icc     ;
  logic       mulstep ;            
  logic       mul     ;            
  logic       mac     ;
  logic       bp      ;
  logic       rfe1, rfe2  ;
 } execute_reg_type;
  
typedef struct {
 pipeline_ctrl_type     ctrl    ;
 word     result  ;
 word     y       ;
  logic  [3 : 0]     icc     ;
  logic       nalign  ;
 dc_in_type     dci     ;
  logic       werr    ;
  logic       wcwp    ;
  logic       irqen   ;
  logic       irqen2  ;
  logic       mac     ;
  logic       divz    ;
  logic       su      ;
  logic       mul     ;
  logic       casa    ;
  logic       casaz   ;
 } memory_reg_type;
  
  type exception_state is (run, trap, dsu1, dsu2);
  
typedef struct {
 pipeline_ctrl_type     ctrl    ;
 word     result  ;
 word     y       ;
  logic  ( 3 downto 0)     icc     ;
  logic       annul_all  ;
 dcdtype     data    ;
  logic  [DSETMSB : 0]     set     ;
  logic       mexc    ;
 dc_in_type     dci     ;
  logic  [1 : 0]     laddr   ;
 exception_state     rstate  ;
  logic  [2 : 0]     npc     ;
  logic       intack  ;
  logic       ipend   ;
  logic       mac     ;
  logic       debug   ;
  logic       nerror  ;
  logic       ipmask  ;
 } exception_reg_type;
  
typedef struct {
  logic  [7 : 0]     tt       ;
  logic       err      ;
  logic  [TBUFBITS-1 : 0]     tbufcnt  ;
  logic  [7 : 0]     asi      ;
  logic  [2 : 1]     crdy     ;  // diag cache access ready
 } dsu_registers;
  
typedef struct {
 pctype     addr    ;
  logic       pwd     ;
 } irestart_register;
  
 
typedef struct {
  logic       pwd     ;
  logic       error   ;
 } pwd_register_type;

typedef struct {
 cwptype     cwp     ;                                // current window pointer
  logic  [3 : 0]     icc     ;        // integer condition codes
  logic  [7 : 0]     tt      ;        // trap type
  logic  [19 : 0]     tba     ;       // trap base address
  logic  [NWIN-1 : 0]     wim     ;       // window invalid mask
  logic  [3 : 0]     pil     ;        // processor interrupt level
  logic       ec      ;                                  // enable CP 
  logic       ef      ;                                  // enable FP 
  logic       ps      ;                                  // previous supervisor flag
  logic       s       ;                                  // supervisor flag
  logic       et      ;                                  // enable traps
 word     y       ;
 word     asr18   ;
  logic       svt     ;                                  // enable traps
  logic       dwt     ;                           // disable write error trap
  logic       dbp     ;                           // disable branch prediction
 } special_register_type;
  
typedef struct {
 special_register_type     s       ;
 word     result  ;
 rfatype     wa      ;
  logic       wreg    ;
  logic       except  ;
 } write_reg_type;

typedef struct {
 fetch_reg_type     f   ;
 decode_reg_type     d   ;
 regacc_reg_type     a   ;
 execute_reg_type     e   ;
 memory_reg_type     m   ;
 exception_reg_type     x   ;
 write_reg_type     w   ;
 } registers;

typedef struct {
  logic       pri    ;
  logic       ill    ;
  logic       fpdis  ;
  logic       cpdis  ;
  logic       wovf   ;
  logic       wunf   ;
  logic       ticc   ;
 } exception_type;

typedef struct {
  logic  [31 : 2]     addr     ;  // watchpoint address
  logic  [31 : 2]     mask     ;  // watchpoint mask
  logic       exec     ;                           // trap on instruction
  logic       load     ;                           // trap on load
  logic       store    ;                           // trap on store
 } watchpoint_register;

  type watchpoint_registers is array (0 to 3) of watchpoint_register;

  function dbgexc(registers r ; l3_debug_in_type dbgi ; std_ulogic trap ; logic  [7 : 0] tt)  
     logic   dmode ;
  begin
    dmode = 1'b0;
    if ((!r.x.ctrl.annul && trap) == 1'b1) begin 
      if (((tt == {2'b00,TT_WATCH}) && (dbgi.bwatch == 1'b1)) ||
          ((dbgi.bsoft == 1'b1) && (tt == 8'b10000001)) ||
          (dbgi.btrapa == 1'b1) ||
          ((dbgi.btrape == 1'b1) &&  !((tt[5 : 0] == TT_PRIV) || 
            (tt[5 : 0] == TT_FPDIS) || (tt[5 : 0] == TT_WINOF) ||
            (tt[5 : 0] == TT_WINUF) || (tt[5 : 4] == 2'b01) || (tt[7] == 1'b1))) || 
          (( (!r.w.s.et) && dbgi.berror) == 1'b1)) begin 
        dmode = 1'b1;
       end 
     end 
    return[dmode];
 end 
 endfunction                     
  function dbgerr(registers r ; l3_debug_in_type dbgi ;
                  logic  [7 : 0] tt)
   
     logic   err ;
  begin
    err =  !r.w.s.et;
    if (((dbgi.dbreak == 1'b1) && (tt == ({2'b00,TT_WATCH}))) ||
        ((dbgi.bsoft == 1'b1) && (tt == (8'b10000001)))) begin 
      err = 1'b0;
     end 
    return[err];
 end 
 endfunction 

  function diagwr(input  registers r    ;
                   input  dsu_registers dsur ;
                   input  irestart_register ir   ;
                   input  l3_debug_in_type dbg  ;
                   input  watchpoint_registers wpr  ;
                   output  special_register_type s    ;
                   output  watchpoint_registers vwpr ;
                   output   logic  [7 : 0] asi ;
                   output  pctype pc, npc ;
                   output   logic  [TBUFBITS-1 : 0] tbufcnt ;
                   output   logic   wr ;
                   output   logic  [9 : 0] addr ;
                   output  word data ;
                   output   logic   fpcwr ) ;
  integer range 0 to 3 i ;
  begin
    s = r.w.s; pc = r.f.pc; = ir.addr; wr  npc =  1'b0;
    vwpr = wpr; = dsur.asi; addr  asi =   `0 ;
    data = dbg.ddata;
    = dsur.tbufcnt; fpcwr  tbufcnt =  1'b0;
      if ((dbg.dsuen && dbg.denable && dbg.dwrite) == 1'b1) begin 
        case (dbg.daddr[23 : 20]) 
          4'b0001  :
            if ((dbg.daddr[16] == 1'b1) && TRACEBUF) begin  // trace buffer control reg
              tbufcnt = dbg.ddata[TBUFBITS-1 : 0];
             end 
          4'b0011  : // IU reg file
            if (dbg.daddr[12] == 1'b0) begin 
              wr = 1'b1;
              addr =  `0 ;
              addr[RFBITS-1 : 0] = dbg.daddr(RFBITS+1 downto 2);
            else  // FPC
              fpcwr = 1'b1;
             end 
          4'b0100  : // IU special registers
            case (dbg.daddr[7 : 6]) 
              2'b00  : // IU regs Y - TBUF ctrl reg
                case (dbg.daddr[5 : 2]) 
                  4'b0000  : // Y
                    s.y = dbg.ddata;
                  4'b0001  : // PSR
                    s.cwp = dbg.ddata[NWINLOG2-1 : 0];
                    s.icc = dbg.ddata[23 : 20];
                    s.ec  = dbg.ddata[13];
                    if (FPEN) begin  s.ef = dbg.ddata[12];  end 
                    s.pil = dbg.ddata[11 : 8];
                    s.s   = dbg.ddata[7];
                    s.ps  = dbg.ddata[6];
                    s.et  = dbg.ddata[5];
                  4'b0010  : // WIM
                    s.wim = dbg.ddata[NWIN-1 : 0];
                  4'b0011  : // TBR
                    s.tba = dbg.ddata[31 : 12];
                    s.tt  = dbg.ddata[11 : 4];
                  4'b0100  : // PC
                    pc = dbg.ddata[31 : PCLOW];
                  4'b0101  : // NPC
                    npc = dbg.ddata[31 : PCLOW];
                  4'b0110  : //FSR
                    fpcwr = 1'b1;
                  4'b0111  : //CFSR
                  4'b1001  : // ASI reg
                    asi = dbg.ddata[7 : 0];
                  default : 
                endcase 
              2'b01  : // ASR16 - ASR31
                case (dbg.daddr[5 : 2]) 
                4'b0001  :  // %ASR17
                  if (bp == 2) begin  s.dbp = dbg.ddata[27];  end 
                  s.dwt = dbg.ddata[14];
                  s.svt = dbg.ddata[13];
                4'b0010  :  // %ASR18
                  if (MACEN) begin  s.asr18 = dbg.ddata;  end 
                4'b1000  :          // %ASR24 - %ASR31
                  vwpr[0].addr = dbg.ddata[31 : 2];
                  vwpr[0].exec = dbg.ddata[0]; 
                4'b1001  :
                  vwpr[0].mask = dbg.ddata[31 : 2];
                  vwpr[0].load = dbg.ddata[1];
                  vwpr[0].store = dbg.ddata[0];              
                4'b1010  :
                  vwpr[1].addr = dbg.ddata[31 : 2];
                  vwpr[1].exec = dbg.ddata[0]; 
                4'b1011  :
                  vwpr[1].mask = dbg.ddata[31 : 2];
                  vwpr[1].load = dbg.ddata[1];
                  vwpr[1].store = dbg.ddata[0];              
                4'b1100  :
                  vwpr[2].addr = dbg.ddata[31 : 2];
                  vwpr[2].exec = dbg.ddata[0]; 
                4'b1101  :
                  vwpr[2].mask = dbg.ddata[31 : 2];
                  vwpr[2].load = dbg.ddata[1];
                  vwpr[2].store = dbg.ddata[0];              
                4'b1110  :
                  vwpr[3].addr = dbg.ddata[31 : 2];
                  vwpr[3].exec = dbg.ddata[0]; 
                4'b1111  : // 
                  vwpr[3].mask = dbg.ddata[31 : 2];
                  vwpr[3].load = dbg.ddata[1];
                  vwpr[3].store = dbg.ddata[0];              
                default :  // 
                endcase 
// disabled due to bug in XST
//                  i = conv_integer(dbg.daddr[4 : 3]); 
//                  if (dbg.daddr[2] == 1'b0) begin 
//                    vwpr[i].addr = dbg.ddata[31 : 2];
//                    vwpr[i].exec = dbg.ddata[0]; 
//                  else
//                    vwpr[i].mask = dbg.ddata[31 : 2];
//                    vwpr[i].load = dbg.ddata[1];
//                    vwpr[i].store = dbg.ddata[0];              
//                   end                     
              default : 
            endcase 
          default : 
        endcase 
       end 
 end 
 endfunction 
  function asr17_gen ( input  registers r )  
  word asr17 ;
  integer range 0 to 3 fpu2 ;  
  begin
    asr17 = zero32;
    asr17[31 : 28] =  4'(index);
    if (bp == 2) begin  asr17[27] = r.w.s.dbp;  end 
    if (notag == 0) begin  asr17[26] = 1'b1;  end  // CASA && tagged arith
    if (clk2x > 8) begin 
      asr17[16 : 15] =  2'(clk2x-8);
      asr17[17] = 1'b1; 
   end else if (clk2x > 0) begin 
      asr17[16 : 15] =  2'(clk2x);
     end 
    asr17[14] = r.w.s.dwt;
    if (svt == 1) begin  asr17[13] = r.w.s.svt;  end 
    if (lddel == 2) begin  asr17[12] = 1'b1;  end 
    if ((fpu > 0) && (fpu < 8)) begin  fpu2 = 1;
   end else if ((fpu >= 8) && (fpu < 15)) begin  fpu2 = 3;
   end else if (fpu == 15) begin  fpu2 = 2;
    else fpu2 = 0;  end 
    asr17[11 : 10] =  2'(fpu2);                       
    if (mac == 1) begin  asr17[9] = 1'b1;  end 
    if (v8 != 0) begin  asr17[8] = 1'b1;  end 
    asr17[7 : 5] =  3'(nwp);                       
    asr17[4 : 0] =  5'(nwin-1);       
    return[asr17];
 end 
 endfunction 
  function diagread(input  l3_debug_in_type dbgi   ;
                     input  registers r      ;
                     input  dsu_registers dsur   ;
                     input  irestart_register ir     ;
                     input  watchpoint_registers wpr    ;
                     input   dcache_out_type dco   ;                          
                     input  tracebuf_out_type tbufo  ;
                     output  word data ) ;
     logic  [4 : 0] cwp ;
     logic  [4 : 0] rd ;
    integer range 0 to 3 i ;    
  begin
    =  `0 ; cwp  data ==   `0 ;
    cwp[NWINLOG2-1 : 0] = r.w.s.cwp;
      case (dbgi.daddr[22 : 20]) 
        3'b001  : // trace buffer
          if (TRACEBUF) begin 
            if (dbgi.daddr[16] == 1'b1) begin  // trace buffer control reg
              data[TBUFBITS-1 : 0] = dsur.tbufcnt;
            else
              case (dbgi.daddr[3 : 2]) 
              2'b00  : data = tbufo.data[127 : 96];
              2'b01  : data = tbufo.data[95 : 64];
              2'b10  : data = tbufo.data[63 : 32];
              default :  data = tbufo.data[31 : 0];
              endcase 
             end 
           end 
        3'b011  : // IU reg file
          if (dbgi.daddr[12] == 1'b0) begin 
            if (dbgi.daddr[11] == 1'b0) begin 
                data = rfo.data1[31 : 0];
              else data = rfo.data2[31 : 0];  end 
          else
              data = fpo.dbg.data;
           end 
        3'b100  : // IU regs
          case (dbgi.daddr[7 : 6]) 
            2'b00  : // IU regs Y - TBUF ctrl reg
              case (dbgi.daddr[5 : 2]) 
                4'b0000  :
                  data = r.w.s.y;
                4'b0001  :
                  data =  4'(IMPL) &  4'(VER) &
                          r.w.s.icc & 6'b000000 & r.w.s.ec & r.w.s.ef & r.w.s.pil &
                          r.w.s.s & r.w.s.ps & r.w.s.et & cwp;
                4'b0010  :
                  data[NWIN-1 : 0] = r.w.s.wim;
                4'b0011  :
                  data = r.w.s.tba & r.w.s.tt & 4'b0000;
                4'b0100  :
                  data[31 : PCLOW] = r.f.pc;
                4'b0101  :
                  data[31 : PCLOW] = ir.addr;
                4'b0110  : // FSR
                  data = fpo.dbg.data;
                4'b0111  : // CPSR
                4'b1000  : // TT reg
                  data[12 : 4] = dsur.err & dsur.tt;
                4'b1001  : // ASI reg
                  data[7 : 0] = dsur.asi;
                default : 
              endcase 
            2'b01  :
              if (dbgi.daddr[5] == 1'b0) begin  
                if (dbgi.daddr[4 : 2] == 3'b001) begin  // %ASR17
                  data = asr17_gen[r];
               end else if (MACEN &&  dbgi.daddr[4 : 2] == 3'b010) begin  // %ASR18
                  data = r.w.s.asr18;
                 end 
              else  // %ASR24 - %ASR31
                i = conv_integer(dbgi.daddr[4 : 3]);                                           // 
                if (dbgi.daddr[2] == 1'b0) begin 
                  data[31 : 2] = wpr[i].addr;
                  data[0] = wpr[i].exec;
                else
                  data[31 : 2] = wpr[i].mask;
                  data[1] = wpr[i].load;
                  data[0] = wpr[i].store; 
                 end 
               end 
            default : 
          endcase 
        3'b111  :
          data = r.x.data(conv_integer(r.x.set));
        default : 
      endcase 
 end 
 endfunction   

  function itrace(input  registers r    ;
                   input  dsu_registers dsur ;
                   input  dsu_registers vdsu ;
                   input  word res  ;
                   input   logic   exc  ;
                   input  l3_debug_in_type dbgi ;
                   input   logic   error ;
                   input   logic   trap  ;                          
                   output   logic  [TBUFBITS-1 : 0] tbufcnt ; 
                   output  tracebuf_in_type di  ;
                   input   logic   ierr ;
                   derr : in  logic  
                   ) ;
   logic   meminst ;
  begin
    di.addr =  `0 ; di.data =  `0 ;
    di.enable = 1'b0; di.write =  `0 ;
    tbufcnt = vdsu.tbufcnt;
    meminst = r.x.ctrl.inst[31] && r.x.ctrl.inst[30];
    if (TRACEBUF) begin 
      di.addr[TBUFBITS-1 : 0] = dsur.tbufcnt;
      di.data[127] = 1'b0;
      di.data[126] =  !r.x.ctrl.pv;
      di.data[125 : 96] = dbgi.timer[29 : 0];
      di.data[95 : 64] = res;
      di.data[63 : 34] = r.x.ctrl.pc[31 : 2];
      di.data[33] = trap;
      di.data[32] = error;
      di.data[31 : 0] = r.x.ctrl.inst;
      if ((dbgi.tenable == 1'b0) || (r.x.rstate == dsu2)) begin 
        if (((dbgi.dsuen && dbgi.denable) == 1'b1) && (dbgi.daddr[23 : 20] & dbgi.daddr[16] == 5'b00010)) begin 
          di.enable = 1'b1; 
          di.addr[TBUFBITS-1 : 0] = dbgi.daddr(TBUFBITS-1+4 downto 4);
          if (dbgi.dwrite == 1'b1) begin             
            case (dbgi.daddr[3 : 2]) 
              2'b00  : di.write[3] = 1'b1;
              2'b01  : di.write[2] = 1'b1;
              2'b10  : di.write[1] = 1'b1;
              default :  di.write[0] = 1'b1;
            endcase 
            di.data = dbgi.ddata & dbgi.ddata & dbgi.ddata & dbgi.ddata;
           end 
         end 
     end else if ((!r.x.ctrl.annul && (r.x.ctrl.pv || meminst) &&  !r.x.debug) == 1'b1) begin 
        di.enable = 1'b1; di.write =  `1 ;
        tbufcnt = dsur.tbufcnt + 1;
       end       
      di.diag = dco.testen &  dco.scanen & 2'b00;
      if (dco.scanen == 1'b1) begin  di.enable = 1'b0;  end 
     end 
 end 
 endfunction 
  function dbg_cache(input   logic   holdn    ;
                      input  l3_debug_in_type dbgi     ;
                      input  registers r        ;
                      input  dsu_registers dsur     ;
                      input  word mresult  ;
                      input  dc_in_type dci      ;
                      output  word mresult2 ;
                      dci2     : out dc_in_type
                      ) ;
  begin
    mresult2 = mresult; = dci; dci2.dsuen  dci2 =  1'b0; 
    if (DBGUNIT) begin 
      if (r.x.rstate == dsu2)
      begin 
        dci2.asi = dsur.asi;
        if ((dbgi.daddr[22 : 20] == 3'b111) && (dbgi.dsuen == 1'b1)) begin 
          dci2.dsuen = (dbgi.denable || r.m.dci.dsuen) &&  !dsur.crdy[2];
          dci2.enaddr = dbgi.denable;
          dci2.size = 2'b10; dci2.read = 1'b1; dci2.write = 1'b0;
          if ((dbgi.denable &&  !r.m.dci.enaddr) == 1'b1) begin             
            =  `0 ; mresult2[19 : 2]  mresult2 =  dbgi.daddr[19 : 2];
          else
            mresult2 = dbgi.ddata;            
           end 
          if (dbgi.dwrite == 1'b1) begin 
            dci2.read = 1'b0; dci2.write = 1'b1;
           end 
         end 
       end 
     end 
 end 
 endfunction     
  function fpexack(input  registers r ; output   logic   fpexc ) ;
  begin
    fpexc = 1'b0;
    if (FPEN) begin  
      if (r.x.ctrl.tt == TT_FPEXC) begin  fpexc = 1'b1;  end 
     end 
 end 
 endfunction 
  function diagrdy(input   logic   denable ;
                    input  dsu_registers dsur ;
                    input  dc_in_type dci   ;
                    input   logic   mds ;
                    input  icache_out_type ico ;
                    output   logic  [2 : 1] crdy ) ;
  begin
    crdy = dsur.crdy[1] & 1'b0;    
    if (dci.dsuen == 1'b1) begin 
      case (dsur.asi[4 : 0]) 
        ASI_ITAG | ASI_IDATA | ASI_UINST | ASI_SINST  :
          crdy[2] = ico.diagrdy &&  !dsur.crdy[2];
        ASI_DTAG | ASI_MMUSNOOP_DTAG | ASI_DDATA | ASI_UDATA | ASI_SDATA  :
          crdy[1] =  !denable && dci.enaddr &&  !dsur.crdy[1];
        default : 
          crdy[2] = dci.enaddr && denable;
      endcase 
     end 
 end 
 endfunction 

   localparam     bit    RESET_ALL =  GRLIB_CONFIG_ARRAY[grlib_sync_reset_enable_all] = 1;
   localparam    dc_in_type  dc_in_res =  (
    signed => 1'b0,
    enaddr => 1'b0,
    read   => 1'b0,
    write  => 1'b0,
    lock   => 1'b0,
    dsuen  => 1'b0,
    size   =>  `0 ,
    asi    =>  `0 );
   localparam     pipeline_ctrl_type  pipeline_ctrl_res =  (
    pc    =>  `0 ,
    inst  =>  `0 ,
    cnt   =>  `0 ,
    rd    =>  `0 ,
    tt    =>  `0 ,
    trap  => 1'b0,
    annul => 1'b1,
    wreg  => 1'b0,
    wicc  => 1'b0,
    wy    => 1'b0,
    ld    => 1'b0,
    pv    => 1'b0,
    rett  => 1'b0);
   localparam    pctype  fpc_res =   20'(rstaddr) & zero32[11 : PCLOW];
  
   localparam    fetch_reg_type  fetch_reg_res =  (
    pc     => fpc_res,  // Needs special handling
    branch => 1'b0
    );
   localparam    decode_reg_type  decode_reg_res =  (
    pc     =>  `0 ,
    inst   => (others =>  `0 ),
    cwp    =>  `0 ,
    set    =>  `0 ,
    mexc   => 1'b0,
    cnt    =>  `0 ,
    pv     => 1'b0,
    annul  => 1'b1,
    inull  => 1'b0,
    step   => 1'b0,
    divrdy => 1'b0
    );
   localparam    regacc_reg_type  regacc_reg_res =  (
    ctrl     => pipeline_ctrl_res,
    rs1      =>  `0 ,
    rfa1     =>  `0 ,
    rfa2     =>  `0 ,
    rsel1    =>  `0 ,
    rsel2    =>  `0 ,
    rfe1     => 1'b0,
    rfe2     => 1'b0,
    cwp      =>  `0 ,
    imm      =>  `0 ,
    ldcheck1 => 1'b0,
    ldcheck2 => 1'b0,
    ldchkra  => 1'b1,
    ldchkex  => 1'b1,
    su       => 1'b1,
    et       => 1'b0,
    wovf     => 1'b0,
    wunf     => 1'b0,
    ticc     => 1'b0,
    jmpl     => 1'b0,
    step     => 1'b0,
    mulstart => 1'b0,
    divstart => 1'b0,
    bp       => 1'b0,
    nobp     => 1'b0
    );
   localparam    execute_reg_type  execute_reg_res =  (
    ctrl    =>  pipeline_ctrl_res,
    op1     =>  `0 ,
    op2     =>  `0 ,
    aluop   =>  `0 ,
    alusel  => 2'b11,
    aluadd  => 1'b1,
    alucin  => 1'b0,
    ldbp1   => 1'b0,
    ldbp2   => 1'b0,
    invop2  => 1'b0,
    shcnt   =>  `0 ,
    sari    => 1'b0,
    shleft  => 1'b0,
    ymsb    => 1'b0,
    rd      =>  `0 ,
    jmpl    => 1'b0,
    su      => 1'b0,
    et      => 1'b0,
    cwp     =>  `0 ,
    icc     =>  `0 ,
    mulstep => 1'b0,
    mul     => 1'b0,
    mac     => 1'b0,
    bp      => 1'b0,
    rfe1    => 1'b0,
    rfe2    => 1'b0
    );
   localparam    memory_reg_type  memory_reg_res =  (
    ctrl   => pipeline_ctrl_res,
    result =>  `0 ,
    y      =>  `0 ,
    icc    =>  `0 ,
    nalign => 1'b0,
    dci    => dc_in_res,
    werr   => 1'b0,
    wcwp   => 1'b0,
    irqen  => 1'b0,
    irqen2 => 1'b0,
    mac    => 1'b0,
    divz   => 1'b0,
    su     => 1'b0,
    mul    => 1'b0,
    casa   => 1'b0,
    casaz  => 1'b0
    );
  function xnpc_res  
  begin
    if (v8 != 0) begin   
     
  end function xnpc_res;
   localparam    exception_reg_type  exception_reg_res =  (
    ctrl      => pipeline_ctrl_res,
    result    =>  `0 ,
    y         =>  `0 ,
    icc       =>  `0 ,
    annul_all => 1'b1,
    data      => (others =>  `0 ),
    set       =>  `0 ,
    mexc      => 1'b0,
    dci       => dc_in_res,
    laddr     =>  `0 ,
    rstate    => run,                   // Has special handling
    npc       => xnpc_res,
    intack    => 1'b0,
    ipend     => 1'b0,
    mac       => 1'b0,
    debug     => 1'b0,                   // Has special handling
    nerror    => 1'b0,
    ipmask    => 1'b0
    );
   localparam    dsu_registers  DRES =  (
    tt      =>  `0 ,
    err     => 1'b0,
    tbufcnt =>  `0 ,
    asi     =>  `0 ,
    crdy    =>  `0 
    );
   localparam    irestart_register  IRES =  (
    addr =>  `0 , pwd => 1'b0
    );
   localparam    pwd_register_type  PRES =  (
    pwd => 1'b0,                         // Needs special handling
    error => 1'b0
    );
  // localparam    special_register_type  special_register_res =  (
  //  cwp    => zero32[NWINLOG2-1 : 0],
  //  icc    =>  `0 ,
  //  tt     =>  `0 ,
  //  tba    => fpc_res[31 : 12],
  //  wim    =>  `0 ,
  //  pil    =>  `0 ,
  //  ec     => 1'b0,
  //  ef     => 1'b0,
  //  ps     => 1'b1,
  //  s      => 1'b1,
  //  et     => 1'b0,
  //  y      =>  `0 ,
  //  asr18  =>  `0 ,
  //  svt    => 1'b0,
  //  dwt    => 1'b0,
  //  dbp    => 1'b0
  //  );
  //XST workaround:
  function special_register_res  
    special_register_type s ;
  begin
    s.cwp   = zero32[NWINLOG2-1 : 0];
    s.icc   =  `0 ;
    s.tt    =  `0 ;
    s.tba   = fpc_res[31 : 12];
    s.wim   =  `0 ;
    s.pil   =  `0 ;
    s.ec    = 1'b0;
    s.ef    = 1'b0;
    s.ps    = 1'b1;
    s.s     = 1'b1;
    s.et    = 1'b0;
    s.y     =  `0 ;
    s.asr18 =  `0 ;
    s.svt   = 1'b0;
    s.dwt   = 1'b0;
    s.dbp   = 1'b0;
     
  end function special_register_res;
  // localparam    write_reg_type  write_reg_res =  (
  //  s      => special_register_res,
  //  result =>  `0 ,
  //  wa     =>  `0 ,
  //  wreg   => 1'b0,
  //  except => 1'b0
  //  );
  // XST workaround:
  function write_reg_res  
    write_reg_type w ;
  begin
    w.s      = special_register_res;
    w.result =  `0 ;
    w.wa     =  `0 ;
    w.wreg   = 1'b0;
    w.except = 1'b0;
     
  end function write_reg_res;
   localparam    registers  RRES =  (
    f => fetch_reg_res,
    d => decode_reg_res,
    a => regacc_reg_res,
    e => execute_reg_res,
    m => memory_reg_res,
    x => exception_reg_res,
    w => write_reg_res
    );
   localparam    exception_type  exception_res =  (
    pri   => 1'b0,
    ill   => 1'b0,
    fpdis => 1'b0,
    cpdis => 1'b0,
    wovf  => 1'b0,
    wunf  => 1'b0,
    ticc  => 1'b0
    );
   localparam    watchpoint_register  wpr_none =  (
    addr  => zero32[31 : 2],
    mask  => zero32[31 : 2],
    exec  => 1'b0,
    load  => 1'b0,
    store => 1'b0);

  signal r, registers rin ;
  signal wpr, watchpoint_registers wprin ;
  signal dsur, dsu_registers dsuin ;
  signal ir, irestart_register irin ;
  signal rp, pwd_register_type rpin ;

// execute stage operations

   localparam     logic  [2 : 0]  EXE_AND =  3'b000;
   localparam     logic  [2 : 0]  EXE_XOR =  3'b001; // must be equal to EXE_PASS2
   localparam     logic  [2 : 0]  EXE_OR =  3'b010;
   localparam     logic  [2 : 0]  EXE_XNOR =  3'b011;
   localparam     logic  [2 : 0]  EXE_ANDN =  3'b100;
   localparam     logic  [2 : 0]  EXE_ORN =  3'b101;
   localparam     logic  [2 : 0]  EXE_DIV =  3'b110;

   localparam     logic  [2 : 0]  EXE_PASS1 =  3'b000;
   localparam     logic  [2 : 0]  EXE_PASS2 =  3'b001;
   localparam     logic  [2 : 0]  EXE_STB =  3'b010;
   localparam     logic  [2 : 0]  EXE_STH =  3'b011;
   localparam     logic  [2 : 0]  EXE_ONES =  3'b100;
   localparam     logic  [2 : 0]  EXE_RDY =  3'b101;
   localparam     logic  [2 : 0]  EXE_SPR =  3'b110;
   localparam     logic  [2 : 0]  EXE_LINK =  3'b111;

   localparam     logic  [2 : 0]  EXE_SLL =  3'b001;
   localparam     logic  [2 : 0]  EXE_SRL =  3'b010;
   localparam     logic  [2 : 0]  EXE_SRA =  3'b100;

   localparam     logic  [2 : 0]  EXE_NOP =  3'b000;

// EXE result select

   localparam     logic  [1 : 0]  EXE_RES_ADD =  2'b00;
   localparam     logic  [1 : 0]  EXE_RES_SHIFT =  2'b01;
   localparam     logic  [1 : 0]  EXE_RES_LOGIC =  2'b10;
   localparam     logic  [1 : 0]  EXE_RES_MISC =  2'b11;

// Load types

   localparam     logic  [1 : 0]  SZBYTE =  2'b00;
   localparam     logic  [1 : 0]  SZHALF =  2'b01;
   localparam     logic  [1 : 0]  SZWORD =  2'b10;
   localparam     logic  [1 : 0]  SZDBL =  2'b11;

// calculate register file address

  function regaddr(std_logic_vector cwp ; logic  [4 : 0] reg;
         output  rfatype rao ) ;
  rfatype ra ;
   localparam     logic  (RFBITS-5  downto 0)  globals =  
         RFBITS-4'(NWIN);
  begin
    =  `0 ; ra[4 : 0]  ra =  reg;
    if (reg[4 : 3] == 2'b00) begin  ra(RFBITS -1 downto 4) = globals;
    else
      ra(NWINLOG2+3 downto 4) = cwp + ra[4];
      if (ra[RFBITS-1 : 4] == globals) begin 
        ra[RFBITS-1 : 4] =  `0 ;
       end 
     end 
    rao = ra;
 end 
 endfunction 
// branch adder

  function branch_address(word inst ; pctype pc )  
  pctype baddr, caddr, tmp ;
  begin
    =  `0 ; caddr[31 : 2]  caddr =  inst[29 : 0];
    caddr[31 : 2] = caddr[31 : 2] + pc[31 : 2];
    =  `0 ; baddr[31 : 24]  baddr ==  (others => inst[21]); 
    baddr[23 : 2] = inst[21 : 0];
    baddr[31 : 2] = baddr[31 : 2] + pc[31 : 2];
    if (inst[30] == 1'b1) begin  = caddr; else tmp  tmp =  baddr;  end 
    return[tmp];
 end 
 endfunction 
// evaluate branch condition

  function branch_true(logic  [3 : 0] icc; word inst ) 
         
   logic   n, z, v, c, branch ;
  begin
    n = icc[3]; z = icc[2]; == icc[1]; c  v =  icc[0];
    case (inst[27 : 25]) 
    3'b000  :  branch = inst[28] xor 1'b0;                  // bn, ba
    3'b001  :  branch = inst[28] xor z;                    // be, bne
    3'b010  :  branch = inst[28] xor (z || (n xor v));     // ble, bg
    3'b011  :  branch = inst[28] xor (n xor v);            // bl, bge
    3'b100  :  branch = inst[28] xor (c || z);             // bleu, bgu
    3'b101  :  branch = inst[28] xor c;                    // bcs, bcc 
    3'b110  :  branch = inst[28] xor n;                    // bneg, bpos
    default :  branch = inst[28] xor v;                    // bvs, bvc   
    endcase 
    return[branch];
 end 
 endfunction 
// detect RETT instruction in the pipeline && set the local psr.su && psr.et

  function su_et_select(input  registers r ; xc_ps, input   logic   xc_s, xc_et ;
                       output   logic   su, et ) ;
  begin
   if ((r.a.ctrl.rett || r.e.ctrl.rett || r.m.ctrl.rett || r.x.ctrl.rett) == 1'b1)
     && (r.x.annul_all == 1'b0)
   begin  = xc_ps; et  su =  1'b1;
   else = xc_s; et  su =  xc_et;  end 
 end 
 endfunction 
// detect watchpoint trap

  function wphit(registers r ; watchpoint_registers wpr ; l3_debug_in_type debug )
     
   logic   exc ;
  begin
    exc = 1'b0;
    for i in 1 to NWP loop
      if (((wpr(i-1).exec && r.a.ctrl.pv &&  !r.a.ctrl.annul) == 1'b1)) begin 
         if ((((wpr(i-1).addr xor r.a.ctrl.pc[31 : 2]) && wpr(i-1).mask) == Zero32[31 : 2])) begin 
           exc = 1'b1;
          end 
       end 
    end loop;

   if (DBGUNIT) begin 
     if ((debug.dsuen &&  !r.a.ctrl.annul) == 1'b1) begin 
       exc = exc || (r.a.ctrl.pv && ((debug.dbreak && debug.bwatch) || r.a.step));
      end 
    end 
    return[exc];
 end 
 endfunction 
// 32-bit shifter

  function shift3(registers r ; aluin1, word aluin2 )  
  unsigned[63 : 0] shiftin ;
  unsigned[63 : 0] shiftout ;
  natural range 0 to 31 cnt ;
  begin

    cnt = conv_integer(r.e.shcnt);
    if (r.e.shleft == 1'b1) begin 
      shiftin[30 : 0] =  `0 ;
      shiftin[63 : 31] = 1'b0 & unsigned[aluin1];
    else
      shiftin[63 : 32] = (others => r.e.sari);
      shiftin[31 : 0] = unsigned[aluin1];
     end 
    shiftout = SHIFT_RIGHT(shiftin, cnt);
    return( logic  (shiftout[31 : 0]));
     
 end 
 endfunction 
  function shift2(registers r ; aluin1, word aluin2 )  
  unsigned[31 : 0] ushiftin ;
  signed[32 : 0] sshiftin ;
  natural range 0 to 31 cnt ;
  word resleft, resright ;
  begin

    cnt = conv_integer(r.e.shcnt);
    ushiftin = unsigned[aluin1];
    sshiftin = signed({1'b0,aluin1});
    if (r.e.shleft == 1'b1) begin 
      resleft =  logic  (SHIFT_LEFT(ushiftin, cnt));
      return[resleft];
    else
      if (r.e.sari == 1'b1) begin  sshiftin[32] = aluin1[31];  end 
      sshiftin = SHIFT_RIGHT(sshiftin, cnt);
      resright =  logic  (sshiftin[31 : 0]);
      return[resright];
     end 
     
 end 
 endfunction 
  function shift(registers r ; aluin1, word aluin2 ;
                 logic  [4 : 0] shiftcnt; logic    sari )  
   logic  [63 : 0] shiftin ;
  begin
    shiftin = zero32 & aluin1;
    if (r.e.shleft == 1'b1) begin 
      shiftin[31 : 0] = zero32; shiftin[63 : 31] = 1'b0 & aluin1;
    else shiftin[63 : 32] = (others => sari);  end 
    if (shiftcnt [4] == 1'b1) begin  shiftin[47 : 0] = shiftin[63 : 16];  end 
    if (shiftcnt [3] == 1'b1) begin  shiftin[39 : 0] = shiftin[47 : 8];  end 
    if (shiftcnt [2] == 1'b1) begin  shiftin[35 : 0] = shiftin[39 : 4];  end 
    if (shiftcnt [1] == 1'b1) begin  shiftin[33 : 0] = shiftin[35 : 2];  end 
    if (shiftcnt [0] == 1'b1) begin  shiftin[31 : 0] = shiftin[32 : 1];  end 
    return(shiftin[31 : 0]);
 end 
 endfunction 
// Check for illegal && privileged instructions

procedure exception_detect(registers r ; watchpoint_registers wpr ; l3_debug_in_type dbgi ;
        input   logic   trapin ; input   logic  [5 : 0] ttin ; 
        output   logic   trap ; output   logic  [5 : 0] tt ) ;
 logic   illegal_inst, privileged_inst ;
 logic   cp_disabled, fp_disabled, fpop ;
 logic  [1 : 0] op ;
 logic  [2 : 0] op2 ;
 logic  [5 : 0] op3 ;
 logic  [4 : 0] rd  ;
word inst ;
 logic   wph ;
begin
  inst = r.a.ctrl.inst; = trapin; tt  trap =  ttin;
  if (r.a.ctrl.annul == 1'b0) begin 
    = inst[31 : 30]; op2  op =  inst[24 : 22];
    = inst[24 : 19]; rd   op3 =  inst[29 : 25];
    illegal_inst = 1'b0; = 1'b0; cp_disabled  privileged_inst =  1'b0; 
    = 1'b0; fpop  fp_disabled =  1'b0; 
    case (op) 
    null CALL ;
    FMT2  :
      case (op2) 
      SETHI | null BICC ;
      FBFCC  : 
        if (FPEN) begin  =  !r.w.s.ef; else fp_disabled  fp_disabled =  1'b1;  end 
      CBCCC  :
        if ((!CPEN) || (r.w.s.ec == 1'b0)) begin  cp_disabled = 1'b1;  end 
      default :  illegal_inst = 1'b1;
      endcase 
    FMT3  :
      case (op3) 
      when IAND | ANDCC | ANDN | ANDNCC | IOR | ORCC | ORN | ORNCC | IXOR |
        XORCC | IXNOR | XNORCC | ISLL | ISRL | ISRA | MULSCC | IADD | ADDX |
        ADDCC | ADDXCC | ISUB | SUBX | SUBCC | SUBXCC | FLUSH | JMPL | TICC | 
        SAVE | RESTORE | RDY => null;
      TADDCC | TADDCCTV | TSUBCC | TSUBCCTV  : 
        if (notag == 1) begin  illegal_inst = 1'b1;  end 
      UMAC | SMAC  : 
        if (!MACEN) begin  illegal_inst = 1'b1;  end 
      UMUL | SMUL | UMULCC | SMULCC  : 
        if (!MULEN) begin  illegal_inst = 1'b1;  end 
      UDIV | SDIV | UDIVCC | SDIVCC  : 
        if (!DIVEN) begin  illegal_inst = 1'b1;  end 
      RETT  : = r.a.et; privileged_inst  illegal_inst =   !r.a.su;
      RDPSR | RDTBR | RDWIM  : privileged_inst =  !r.a.su;
      WRY  :
        if (rd[4] == 1'b1 && rd[3 : 0] != 4'b0010) begin  // %ASR16-17, %ASR19-31
          privileged_inst =  !r.a.su;
         end 
      WRPSR  : 
        privileged_inst =  !r.a.su; 
      WRWIM | WRTBR   : privileged_inst =  !r.a.su;
      FPOP1 | FPOP2  : 
        if (FPEN) begin  =  !r.w.s.ef; fpop  fp_disabled =  1'b1;
        else = 1'b1; fpop  fp_disabled =  1'b0;  end 
      CPOP1 | CPOP2  :
        if ((!CPEN) || (r.w.s.ec == 1'b0)) begin  cp_disabled = 1'b1;  end 
      default :  illegal_inst = 1'b1;
      endcase 
    default :       // LDST
      case (op3) 
      LDD | ISTD  : illegal_inst = rd[0]; // trap if odd destination register
      LD | LDUB | LDSTUB | LDUH | LDSB | LDSH | ST | STB | STH | SWAP  :
        null;
      LDDA | STDA  :
        = inst[13] || rd[0]; privileged_inst  illegal_inst =   !r.a.su;
      when LDA | LDUBA| LDSTUBA | LDUHA | LDSBA | LDSHA | STA | STBA | STHA |
           SWAPA => 
        = inst[13]; privileged_inst  illegal_inst =   !r.a.su;
      CASA  :
        if (CASAEN) begin 
          illegal_inst = inst[13]; 
          if ((inst[12 : 5] != X"0A")) begin  privileged_inst =  !r.a.su;  end 
        else illegal_inst = 1'b1;  end 
      LDDF | STDF | LDF | LDFSR | STF | STFSR  : 
        if (FPEN) begin  fp_disabled =  !r.w.s.ef;
        else fp_disabled = 1'b1;  end 
      STDFQ  : 
        privileged_inst =  !r.a.su; 
        if ((!FPEN) || (r.w.s.ef == 1'b0)) begin  fp_disabled = 1'b1;  end 
      STDCQ  : 
        privileged_inst =  !r.a.su;
        if ((!CPEN) || (r.w.s.ec == 1'b0)) begin  cp_disabled = 1'b1;  end 
      LDC | LDCSR | LDDC | STC | STCSR | STDC  : 
        if ((!CPEN) || (r.w.s.ec == 1'b0)) begin  cp_disabled = 1'b1;  end 
      default :  illegal_inst = 1'b1;
      endcase 
    endcase 

    wph = wphit(r, wpr, dbgi);
    
    trap = 1'b1;
    if (r.a.ctrl.trap == 1'b1) begin  tt = r.a.ctrl.tt;
   end else if (privileged_inst == 1'b1) begin  tt = TT_PRIV; 
   end else if (illegal_inst == 1'b1) begin  tt = TT_IINST;
   end else if (fp_disabled == 1'b1) begin  tt = TT_FPDIS;
   end else if (cp_disabled == 1'b1) begin  tt = TT_CPDIS;
   end else if (wph == 1'b1) begin  tt = TT_WATCH;
   end else if (r.a.wovf= 1'b1) begin  tt = TT_WINOF;
   end else if (r.a.wunf= 1'b1) begin  tt = TT_WINUF;
   end else if (r.a.ticc= 1'b1) begin  tt = TT_TICC;
    else = 1'b0; tt trap =   `0 ;  end 
   end 
end;

// instructions that write the condition codes (psr.icc)

procedure wicc_y_gen(word inst ; output   logic   wicc, wy ) ;
begin
  = 1'b0; wy  wicc =  1'b0;
  if (inst[31 : 30] == FMT3) begin 
    case (inst[24 : 19]) 
    when SUBCC | TSUBCC | TSUBCCTV | ADDCC | ANDCC | ORCC | XORCC | ANDNCC |
         ORNCC | XNORCC | TADDCC | TADDCCTV | ADDXCC | SUBXCC | WRPSR => 
      wicc = 1'b1;
    WRY  :
      if (r.d.inst(conv_integer(r.d.set))[29 : 25] == 5'b00000) begin  wy = 1'b1;  end 
    MULSCC  :
      = 1'b1; wy  wicc =  1'b1;
    UMAC | SMAC   :
      if (MACEN) begin  wy = 1'b1;  end 
    UMULCC | SMULCC  : 
      if (MULEN && (((mulo.nready == 1'b1) && (r.d.cnt != 2'b00)) || (MULTYPE != 0))) begin 
        = 1'b1; wy  wicc =  1'b1;
       end 
    UMUL | SMUL  : 
      if (MULEN && (((mulo.nready == 1'b1) && (r.d.cnt != 2'b00)) || (MULTYPE != 0))) begin 
        wy = 1'b1;
       end 
    UDIVCC | SDIVCC  : 
      if (DIVEN && (divo.nready == 1'b1) && (r.d.cnt != 2'b00)) begin 
        wicc = 1'b1;
       end 
    default : 
    endcase 
   end 
end;

// select cwp 

procedure cwp_gen(r, registers v ; annul, logic   wcwp ; cwptype ncwp ;
                  output  cwptype cwp ) ;
begin
  if (r.x.rstate == trap) ||
      (r.x.rstate == dsu2) 
     || (rstn == 1'b0) begin  cwp = v.w.s.cwp;                                                                     
 end else if ((wcwp == 1'b1) && (annul == 1'b0)) begin  cwp = ncwp;
 end else if (r.m.wcwp == 1'b1) begin  cwp = r.m.result[NWINLOG2-1 : 0];
  else cwp = r.d.cwp;  end 
end;

// generate wcwp in ex stage

procedure cwp_ex(input   registers r ; output   logic   wcwp ) ;
begin
  if (r.e.ctrl.inst[31 : 30] == FMT3) && 
     (r.e.ctrl.inst[24 : 19] == WRPSR)
  begin  =  !r.e.ctrl.annul; else wcwp  wcwp =  1'b0;  end 
end;

// generate next cwp & window under- && overflow traps

procedure cwp_ctrl(input  registers r ; input   logic  [NWIN-1 : 0] xc_wim ;
        word inst ; output  cwptype de_cwp ; wovf_exc, output   logic   wunf_exc, wcwp ) ;
 logic  [1 : 0] op ;
 logic  [5 : 0] op3 ;
word wim ;
cwptype ncwp ;
begin
  = inst[31 : 30]; op3  op =  inst[24 : 19]; 
  wovf_exc = 1'b0; = 1'b0; wim  wunf_exc =   `0 ; 
  wim[NWIN-1 : 0] = xc_wim; = r.d.cwp; wcwp  ncwp =  1'b0;

  if ((op == FMT3) && ((op3 == RETT) || (op3 == RESTORE) || (op3 == SAVE))) begin 
    wcwp = 1'b1;
    if (op3 == SAVE) begin 
      if ((!CWPOPT) && (r.d.cwp == CWPMIN)) begin  ncwp = CWPMAX;
      else ncwp = r.d.cwp - 1 ;  end 
    else
      if ((!CWPOPT) && (r.d.cwp == CWPMAX)) begin  ncwp = CWPMIN;
      else ncwp = r.d.cwp + 1;  end 
     end 
    if (wim(conv_integer[ncwp]) == 1'b1) begin 
      if (op3 == SAVE) begin  = 1'b1; else wunf_exc  wovf_exc =  1'b1;  end 
     end 
   end 
  de_cwp = ncwp;
end;

// generate register read address 1

procedure rs1_gen(registers r ; word inst ;  output   logic  [4 : 0] rs1 ;
        output   logic   rs1mod ) ;
 logic  [1 : 0] op ;
 logic  [5 : 0] op3 ;
begin
  = inst[31 : 30]; op3  op =  inst[24 : 19]; 
  = inst[18 : 14]; rs1mod  rs1 =  1'b0;
  if (op == LDST) begin 
    if ((r.d.cnt == 2'b01) && ((op3[2] &&  !op3[3]) == 1'b1)) ||
        (r.d.cnt == 2'b10) 
    begin  = 1'b1; rs1  rs1mod =  inst[29 : 25];  end 
    if (((r.d.cnt == 2'b10) && (op3[3 : 0] == 2'b0111))) begin 
      rs1[0] = 1'b1;
     end 
   end 
end;

// load/icc interlock detection

  function icc_valid(registers r )  
  std_logic not_valid ;
  begin
    not_valid = 1'b0;
    if (MULEN || DIVEN) begin  
      not_valid = r.m.ctrl.wicc && (r.m.ctrl.cnt[0] || r.m.mul);
     end 
    not_valid = not_valid || (r.a.ctrl.wicc || r.e.ctrl.wicc);
     
 end 
 endfunction 
  function bp_miss_ex(registers r ; logic  [3 : 0] icc; 
        output  std_logic ex_bpmiss, ra_bpannul ) ;
  std_logic miss ;
  begin
    miss =  (!r.e.ctrl.annul) && r.e.bp &&  !branch_true(icc, r.e.ctrl.inst);
    ra_bpannul = miss && r.e.ctrl.inst[29];
    ex_bpmiss = miss;
 end 
 endfunction 
  function bp_miss_ra(registers r ; output  std_logic ra_bpmiss, de_bpannul ) ;
  std_logic miss ;
  begin
    miss = ( (!r.a.ctrl.annul) && r.a.bp && icc_valid[r] &&  !branch_true(r.m.icc, r.a.ctrl.inst));
    de_bpannul = miss && r.a.ctrl.inst[29];
    ra_bpmiss = miss;
 end 
 endfunction 
  function lock_gen(registers r ; rs2, logic  [4 : 0] rd;
        rfa1, rfa2, rfatype rfrd ; word inst ; fpc_lock, mulinsn, divinsn, logic   de_wcwp ;
        lldcheck1, lldcheck2, lldlock, lldchkra, lldchkex, bp, output   logic   nobp, de_fins_hold ;
        std_logic iperr ) ;
   logic  [1 : 0] op ;
   logic  [2 : 0] op2 ;
   logic  [5 : 0] op3 ;
   logic  [3 : 0] cond ;
   logic  [4 : 0] rs1  ;
   logic   i, ldcheck1, ldcheck2, ldchkra, ldchkex, ldcheck3 ;
  std_logic ldlock, icc_check, bicc_hold, chkmul, y_check ;
   logic   icc_check_bp, y_hold, mul_hold, bicc_hold_bp, fins, call_hold  ;
   logic   de_fins_holdx ;
  begin
    = inst[31 : 30]; op3  op =  inst[24 : 19]; 
    = inst[24 : 22]; cond  op2 =  inst[28 : 25]; 
    = inst[18 : 14]; i  rs1 =  inst[13];
    ldcheck1 = 1'b0; ldcheck2 = 1'b0; = 1'b0; ldlock  ldcheck3 =  1'b0;
    ldchkra = 1'b1; ldchkex = 1'b1; = 1'b0; bicc_hold  icc_check =  1'b0;
    y_check = 1'b0; y_hold = 1'b0; = 1'b0; mul_hold  bp =  1'b0;
    icc_check_bp = 1'b0; nobp = 1'b0; = 1'b0; call_hold  fins =  1'b0;

    if (r.d.annul == 1'b0) 
    begin 
      case (op) 
      CALL  :
        = 1'b1; nobp  call_hold =  BPRED;
      FMT2  :
        if ((op2 == BICC) && (cond[2 : 0] != 3'b000)) begin  
          icc_check_bp = 1'b1;
         end 
        if (op2 == BICC) begin  nobp = BPRED;  end 
      FMT3  :
        = 1'b1; ldcheck2  ldcheck1 =   !i;
        case (op3) 
        TICC  :
          if ((cond[2 : 0] != 3'b000)) begin  icc_check = 1'b1;  end 
          nobp = BPRED;
        RDY  : 
          = 1'b0; ldcheck2  ldcheck1 =  1'b0;
          if (MACPIPE) begin  y_check = 1'b1;  end 
        RDWIM | RDTBR  : 
          = 1'b0; ldcheck2  ldcheck1 =  1'b0;
        RDPSR  : 
          ldcheck1 = 1'b0; = 1'b0; icc_check  ldcheck2 =  1'b1;
        SDIV | SDIVCC | UDIV | UDIVCC  :
          if (DIVEN) begin  = 1'b1; nobp  y_check =  op3[4];  end  // no BP on divcc
        FPOP1 | FPOP2  : ldcheck1:= 1'b0; = 1'b0; fins  ldcheck2 =  BPRED;
        JMPL  : = 1'b1; nobp  call_hold =  BPRED;
        default :  
        endcase 
      LDST  :
        = 1'b1; ldchkra  ldcheck1 =  1'b0;
        case (r.d.cnt) 
        2'b00  :
          if ((lddel == 2) && (op3[2] == 1'b1) && (op3[5] == 1'b0)) begin  ldcheck3 = 1'b1;  end  
          =  !i; ldchkra  ldcheck2 =  1'b1;
        2'b01  :
          ldcheck2 =  !i;
          if ((op3[5] && op3[2] &&  !op3[3]) == 1'b1) begin  = 1'b0; ldcheck2  ldcheck1 =  1'b0;  end   // STF/STC
        default :  ldchkex = 1'b0;
          if (CASAEN && (op3[5 : 3] == 3'b111)) begin 
            ldcheck2 = 1'b1;
         end else if (op3[5] == 1'b1) || ((op3[5] & op3[3 : 1]) == 4'b0110) // LDST
          begin  = 1'b0; ldcheck2  ldcheck1 =  1'b0;  end 
        endcase 
        if (op3[5] == 1'b1) begin  fins = BPRED;  end  // no BP on FPU/CP LD/ST
      null default ;
      endcase 
     end 

    if (MULEN || DIVEN) begin  
      chkmul = mulinsn;
      mul_hold = (r.a.mulstart && r.a.ctrl.wicc) || (r.m.ctrl.wicc && (r.m.ctrl.cnt[0] || r.m.mul));
      if (MULTYPE == 0) && ((icc_check_bp && BPRED && r.a.ctrl.wicc && r.a.ctrl.wy) == 1'b1)
      begin  mul_hold = 1'b1;  end 
    else chkmul = 1'b0;  end 
    if (DIVEN) begin  
      y_hold = y_check && (r.a.ctrl.wy || r.e.ctrl.wy);
      chkmul = chkmul || divinsn;
     end 

    bicc_hold = icc_check &&  !icc_valid[r];
    bicc_hold_bp = icc_check_bp &&  !icc_valid[r];

    if (((r.a.ctrl.ld || chkmul) && r.a.ctrl.wreg && ldchkra) == 1'b1) and
       (((ldcheck1 == 1'b1) && (r.a.ctrl.rd == rfa1)) ||
        ((ldcheck2 == 1'b1) && (r.a.ctrl.rd == rfa2)) ||
        ((ldcheck3 == 1'b1) && (r.a.ctrl.rd == rfrd)))
    begin  ldlock = 1'b1;  end 

    if (((r.e.ctrl.ld || r.e.mac) && r.e.ctrl.wreg && ldchkex) == 1'b1) && 
        ((lddel == 2) || (MACPIPE && (r.e.mac == 1'b1)) || ((MULTYPE == 3) && (r.e.mul == 1'b1))) and
       (((ldcheck1 == 1'b1) && (r.e.ctrl.rd == rfa1)) ||
        ((ldcheck2 == 1'b1) && (r.e.ctrl.rd == rfa2)))
    begin  ldlock = 1'b1;  end 

    de_fins_holdx = BPRED && fins && (r.a.bp || r.e.bp); // skip BP on FPU inst in branch target address
    de_fins_hold = de_fins_holdx;
    ldlock = ldlock || y_hold || fpc_lock || (BPRED && r.a.bp && r.a.ctrl.inst[29] && de_wcwp) || de_fins_holdx;
    if (((icc_check_bp && BPRED) == 1'b1) && ((r.a.nobp || mul_hold) == 1'b0)) begin  
      bp = bicc_hold_bp;
    else ldlock = ldlock || bicc_hold || bicc_hold_bp;  end 
    lldcheck1 = ldcheck1; = ldcheck2; lldlock  lldcheck2 =  ldlock;
    = ldchkra; lldchkex  lldchkra =  ldchkex;
 end 
 endfunction 
  function fpbranch(input  word inst ; input   logic  [1 : 0] fcc  ;
                      output   logic   branch ) ;
   logic  [3 : 0] cond ;
   logic   fbres ;
  begin
    cond = inst[28 : 25];
    case (cond[2 : 0]) 
      3'b000  : fbres = 1'b0;                       // fba, fbn
      3'b001  : fbres = fcc[1] || fcc[0];
      3'b010  : fbres = fcc[1] xor fcc[0];
      3'b011  : fbres = fcc[0];
      3'b100  : fbres =  (!fcc[1]) && fcc[0];
      3'b101  : fbres = fcc[1];
      3'b110  : fbres = fcc[1] &&  !fcc[0];
      default :  fbres = fcc[1] && fcc[0];
    endcase 
    branch = cond[3] xor fbres;     
 end 
 endfunction 
// PC generation

  function ic_ctrl(registers r ; word inst ; annul_all, ldlock, branch_true, 
        fbranch_true, cbranch_true, input   logic   fccv, cccv ; 
        output   logic  [1 : 0] cnt ; 
        output  pctype de_pc ; de_branch, ctrl_annul, de_annul, jmpl_inst, inull, 
        de_pv, ctrl_pv, de_hold_pc, ticc_exception, rett_inst, mulstart,
        output   logic   divstart ; rabpmiss, exbpmiss, std_logic iperr ) ;
   logic  [1 : 0] op ;
   logic  [2 : 0] op2 ;
   logic  [5 : 0] op3 ;
   logic  [3 : 0] cond ;
   logic   hold_pc, annul_current, annul_next, branch, annul, pv ;
   logic   de_jmpl, inhibit_current ;
  begin
    branch = 1'b0; annul_next = 1'b0; = 1'b0; pv  annul_current =  1'b1;
    hold_pc = 1'b0; = 1'b0; rett_inst  ticc_exception =  1'b0;
    = inst[31 : 30]; op3  op =  inst[24 : 19]; 
    = inst[24 : 22]; cond  op2 =  inst[28 : 25]; 
    annul = inst[29]; = 1'b0; cnt  de_jmpl =  2'b00;
    mulstart = 1'b0; = 1'b0; inhibit_current  divstart =  1'b0;
    if (r.d.annul == 1'b0) 
    begin 
      case (inst[31 : 30]) 
      CALL  :
        branch = 1'b1;
        if (r.d.inull == 1'b1) begin  
          = 1'b1; annul_current  hold_pc =  1'b1;
         end 
      FMT2  :
        if ((op2 == BICC) || (FPEN && (op2 == FBFCC)) || (CPEN && (op2 == CBCCC))) begin 
          if ((FPEN && (op2 == FBFCC))) begin  
            branch = fbranch_true;
            if (fccv != 1'b1) begin  = 1'b1; annul_current  hold_pc =  1'b1;  end 
         end else if ((CPEN && (op2 == CBCCC))) begin  
            branch = cbranch_true;
            if (cccv != 1'b1) begin  = 1'b1; annul_current  hold_pc =  1'b1;  end 
          else branch = branch_true || (BPRED && orv[cond] &&  !icc_valid[r]);  end 
          if (hold_pc == 1'b0) begin 
            if (branch == 1'b1) begin 
              if ((cond == BA) && (annul == 1'b1)) begin  annul_next = 1'b1;  end 
            else annul_next = annul_next || annul;  end 
            if (r.d.inull == 1'b1) begin  // contention with JMPL
              hold_pc = 1'b1; = 1'b1; annul_next  annul_current =  1'b0;
             end 
           end 
         end 
      FMT3  :
        case (op3) 
        UMUL | SMUL | UMULCC | SMULCC  :
          if (MULEN && (MULTYPE != 0)) begin  mulstart = 1'b1;  end 
          if (MULEN && (MULTYPE == 0)) begin 
            case (r.d.cnt) 
            2'b00  :
              cnt = 2'b01; hold_pc = 1'b1; = 1'b0; mulstart  pv =  1'b1;
            2'b01  :
              if (mulo.nready == 1'b1) begin  cnt = 2'b00;
              else cnt = 2'b01; = 1'b0; hold_pc  pv =  1'b1;  end 
            null default ;
            endcase 
           end 
        UDIV | SDIV | UDIVCC | SDIVCC  :
          if (DIVEN) begin 
            case (r.d.cnt) 
            2'b00  :
              = 1'b1; pv  hold_pc =  1'b0;
              if (r.d.divrdy == 1'b0) begin 
                = 2'b01; divstart  cnt =  1'b1;
               end 
            2'b01  :
              if (divo.nready == 1'b1) begin  cnt = 2'b00; 
              else cnt = 2'b01; = 1'b0; hold_pc  pv =  1'b1;  end 
            null default ;
            endcase 
           end 
        TICC  :
          if (branch_true == 1'b1) begin  ticc_exception = 1'b1;  end 
        RETT  :
          = 1'b1; //su  rett_inst =  sregs.ps; 
        JMPL  :
          de_jmpl = 1'b1;
        WRY  :
          if (PWRD1) begin  
            if (inst[29 : 25] == 5'b10011) begin  // %ASR19
              case (r.d.cnt) 
              2'b00  :
                pv = 1'b0; = 2'b00; hold_pc  cnt =  1'b1;
                if (r.x.ipend == 1'b1) begin  cnt = 2'b01;  end               
              2'b01  :
                cnt = 2'b00;
              default : 
              endcase 
             end 
           end 
        null default ;
        endcase 
      default :   // LDST 
        case (r.d.cnt) 
        2'b00  :
          if ((op3[2] == 1'b1) || (op3[1 : 0] == 2'b11)) begin  // ST/LDST/SWAP/LDD/CASA
            cnt = 2'b01; = 1'b1; pv  hold_pc =  1'b0;
           end 
        2'b01  :
          if (op3[2 : 0] == 3'b111) || (op3[3 : 0] == 3'b1101) ||
             (CASAEN && (op3[5 : 4] == 2'b11)) ||   // CASA
             ((CPEN || FPEN) && ((op3[5] & op3[2 : 0]) == 4'b1110))
          begin   // LDD/STD/LDSTUB/SWAP
            cnt = 2'b10; = 1'b0; hold_pc  pv =  1'b1;
          else
            cnt = 2'b00;
           end 
        2'b10  :
          cnt = 2'b00;
        null default ;
        endcase 
      endcase 
     end 

    if (ldlock == 1'b1) begin 
      cnt = r.d.cnt; = 1'b0; pv  annul_next =  1'b1;
     end 
    hold_pc = (hold_pc || ldlock) &&  !annul_all;

    if (((exbpmiss && r.a.ctrl.annul && r.d.pv &&  !hold_pc) == 1'b1)) begin 
        = 1'b1; pv  annul_next =  1'b0;
     end 
    if (((exbpmiss &&  !r.a.ctrl.annul && r.d.pv) == 1'b1)) begin 
        annul_next = 1'b1; = 1'b0; annul_current  pv =  1'b1;
     end 
    if (((exbpmiss &&  !r.a.ctrl.annul &&  !r.d.pv &&  !hold_pc) == 1'b1)) begin 
        = 1'b1; pv  annul_next =  1'b0;
     end 
    if ((exbpmiss && r.e.ctrl.inst[29] &&  !r.a.ctrl.annul &&  !r.d.pv ) == 1'b1) 
        && (r.d.cnt == 2'b01) begin 
        annul_next = 1'b1; = 1'b1; pv  annul_current =  1'b0;
     end 
    if ((exbpmiss && r.e.ctrl.inst[29] && r.a.ctrl.annul && r.d.pv) == 1'b1) begin 
      annul_next = 1'b1; = 1'b0; inhibit_current  pv =  1'b1;
     end  
    if ((rabpmiss &&  !r.a.ctrl.inst[29] &&  !r.d.annul && r.d.pv &&  !hold_pc) == 1'b1) begin 
        = 1'b1; pv  annul_next =  1'b0;
     end 
    if ((rabpmiss && r.a.ctrl.inst[29] &&  !r.d.annul && r.d.pv ) == 1'b1) begin 
        annul_next = 1'b1; = 1'b0; inhibit_current  pv =  1'b1;
     end 

    if (hold_pc == 1'b1) begin  = r.d.pc; else de_pc  de_pc =  r.f.pc;  end 

    annul_current = (annul_current || (ldlock &&  !inhibit_current) || annul_all);
    ctrl_annul = r.d.annul || annul_all || annul_current || inhibit_current;
    pv = pv &&  !((r.d.inull &&  !hold_pc) || annul_all);
    jmpl_inst = de_jmpl &&  !annul_current &&  !inhibit_current;
    annul_next = (r.d.inull &&  !hold_pc) || annul_next || annul_all;
    if ((annul_next == 1'b1) || (rstn == 1'b0)) begin 
      cnt =  `0 ; 
     end 

    de_hold_pc = hold_pc; = branch; de_annul  de_branch =  annul_next;
    = pv; ctrl_pv  de_pv =  r.d.pv && 
         !((r.d.annul &&  !r.d.pv) || annul_all || annul_current);
    inull =  (!rstn) || r.d.inull || hold_pc || annul_all;

 end 
 endfunction 
// register write address generation

  function rd_gen(registers r ; word inst ; output   logic   wreg, ld ; 
        output   logic  [4 : 0] rdo ) ;
   logic   write_reg ;
   logic  [1 : 0] op ;
   logic  [2 : 0] op2 ;
   logic  [5 : 0] op3 ;
   logic  [4 : 0] rd  ;
  begin

    op    = inst[31 : 30];
    op2   = inst[24 : 22];
    op3   = inst[24 : 19];

    write_reg = 1'b0; = inst[29 : 25]; ld  rd =  1'b0;

    case (op) 
    CALL  :
        = 1'b1; rd  write_reg =  5'b01111;    // CALL saves PC in r[15] (%o7)
    FMT2  : 
        if (op2 == SETHI) begin  write_reg = 1'b1;  end 
    FMT3  :
        case (op3) 
        UMUL | SMUL | UMULCC | SMULCC  : 
          if (MULEN) begin 
            if ((((mulo.nready == 1'b1) && (r.d.cnt != 2'b00)) || (MULTYPE != 0))) begin 
              write_reg = 1'b1; 
             end 
          else write_reg = 1'b1;  end 
        UDIV | SDIV | UDIVCC | SDIVCC  : 
          if (DIVEN) begin 
            if ((divo.nready == 1'b1) && (r.d.cnt != 2'b00)) begin 
              write_reg = 1'b1; 
             end 
          else write_reg = 1'b1;  end 
        RETT | WRPSR | WRY | WRWIM | WRTBR | TICC | null FLUSH ;
        FPOP1 | null FPOP2 ;
        CPOP1 | null CPOP2 ;
        default :  write_reg = 1'b1;
        endcase 
      default :    // LDST
        ld =  !op3[2];
        if (op3[2] == 1'b0) &&  !((CPEN || FPEN) && (op3[5] == 1'b1)) 
        begin  write_reg = 1'b1;  end 
        case (op3) 
        SWAP | SWAPA | LDSTUB | LDSTUBA | CASA  :
          if (r.d.cnt == 2'b00) begin  = 1'b1; ld  write_reg =  1'b1;  end 
        null default ;
        endcase 
        if (r.d.cnt == 2'b01) begin 
          case (op3) 
          LDD | LDDA | LDDC | LDDF  : rd[0] = 1'b1;
          default : 
          endcase 
         end 
    endcase 

    if (rd == 5'b00000) begin  write_reg = 1'b0;  end 
    = write_reg; rdo  wreg =  rd;
 end 
 endfunction 
// immediate data generation

  function imm_data (registers r ; word insn ) 
         
  word immediate_data, inst ;
  begin
    =  `0 ; inst  immediate_data =  insn;
    case (inst[31 : 30]) 
    FMT2  :
      immediate_data = inst[21 : 0] & 10'b0000000000;
    default :       // LDST
      immediate_data[31 : 13] = (others => inst[12]);
      immediate_data[12 : 0] = inst[12 : 0];
    endcase 
    return[immediate_data];
 end 
 endfunction 
// read special registers
  function get_spr (registers r )  
  word spr ;
  begin
    spr =  `0 ;
      case (r.e.ctrl.inst[24 : 19]) 
      RDPSR  : spr[31 : 5] = 4'(IMPL) &
        4'(VER) & r.m.icc & 6'b000000 & r.w.s.ec & r.w.s.ef & 
        r.w.s.pil & r.e.su & r.w.s.ps & r.e.et;
        spr[NWINLOG2-1 : 0] = r.e.cwp;
      RDTBR  : spr[31 : 4] = r.w.s.tba & r.w.s.tt;
      RDWIM  : spr[NWIN-1 : 0] = r.w.s.wim;
      default : 
      endcase 
    return[spr];
 end 
 endfunction 
// immediate data select

  function imm_select(word inst )  
   bit   imm ;
  begin
    imm = false;
    case (inst[31 : 30]) 
    FMT2  :
      case (inst[24 : 22]) 
      SETHI  : imm = true;
      default :  
      endcase 
    FMT3  :
      case (inst[24 : 19]) 
      RDWIM | RDPSR | RDTBR  : imm = true;
      default :  if ((inst[13] == 1'b1)) begin  imm = true;  end 
      endcase 
    LDST  : 
      if ((inst[13] == 1'b1)) begin  imm = true;  end 
    default :  
    endcase 
    return[imm];
 end 
 endfunction 
// EXE operation

  function alu_op(input  registers r ; input  word iop1, iop2 ; logic  [3 : 0] me_icc;
        my, logic   ldbp ; output  word aop1, aop2 ; output   logic  [2 : 0] aluop  ;
        output   logic  [1 : 0] alusel ; output   logic   aluadd ;
        output   logic  [4 : 0] shcnt ; sari, shleft, ymsb, 
        mulins, divins, mulstep, macins, ldbp2, invop2 : out std_logic
        ) ;
   logic  [1 : 0] op ;
   logic  [2 : 0] op2 ;
   logic  [5 : 0] op3 ;
   logic  [4 : 0] rs1, rs2, rd  ;
   logic  [3 : 0] icc ;
   logic   y0, i  ;
  begin

    op   = r.a.ctrl.inst[31 : 30];
    op2  = r.a.ctrl.inst[24 : 22];
    op3  = r.a.ctrl.inst[24 : 19];
    = r.a.ctrl.inst[18 : 14]; i  rs1 =  r.a.ctrl.inst[13];
    = r.a.ctrl.inst[4 : 0]; rd  rs2 =  r.a.ctrl.inst[29 : 25];
    aop1 = iop1; = iop2; ldbp2  aop2 =  ldbp;
    aluop = EXE_NOP; = EXE_RES_MISC; aluadd  alusel =  1'b1; 
    shcnt = iop2[4 : 0]; sari = 1'b0; = 1'b0; invop2  shleft =  1'b0;
    ymsb = iop1[0]; mulins = 1'b0; = 1'b0; mulstep  divins =  1'b0;
    macins = 1'b0;

    if (r.e.ctrl.wy == 1'b1) begin  y0 = my;
   end else if (r.m.ctrl.wy == 1'b1) begin  y0 = r.m.y[0];
   end else if (r.x.ctrl.wy == 1'b1) begin  y0 = r.x.y[0];
    else y0 = r.w.s.y[0];  end 

    if (r.e.ctrl.wicc == 1'b1) begin  icc = me_icc;
   end else if (r.m.ctrl.wicc == 1'b1) begin  icc = r.m.icc;
   end else if (r.x.ctrl.wicc == 1'b1) begin  icc = r.x.icc;
    else icc = r.w.s.icc;  end 

    case (op) 
    CALL  :
      aluop = EXE_LINK;
    FMT2  :
      case (op2) 
      SETHI  : aluop = EXE_PASS2;
      default : 
      endcase 
    FMT3  :
      case (op3) 
      when IADD | ADDX | ADDCC | ADDXCC | TADDCC | TADDCCTV | SAVE | RESTORE |
           TICC | JMPL | RETT  => alusel = EXE_RES_ADD;
      ISUB | SUBX | SUBCC | SUBXCC | TSUBCC | TSUBCCTV   : 
        alusel = EXE_RES_ADD; aluadd = 1'b0; =  !iop2; invop2  aop2 =  1'b1;
      MULSCC  : alusel = EXE_RES_ADD;
        aop1 = (icc[3] xor icc[1]) & iop1[31 : 1];
        if (y0 == 1'b0) begin  =  `0 ; ldbp2  aop2 =  1'b0;  end 
        mulstep = 1'b1;
      UMUL | UMULCC | SMUL | SMULCC  : 
        if (MULEN) begin  mulins = 1'b1;  end 
      UMAC | SMAC  : 
        if (MACEN) begin  = 1'b1; macins  mulins =  1'b1;  end 
      UDIV | UDIVCC | SDIV | SDIVCC  : 
        if (DIVEN) begin  
          aluop = EXE_DIV; = EXE_RES_LOGIC; divins  alusel =  1'b1;
         end 
      IAND | ANDCC  : = EXE_AND; alusel  aluop =  EXE_RES_LOGIC;
      ANDN | ANDNCC  : = EXE_ANDN; alusel  aluop =  EXE_RES_LOGIC;
      IOR | ORCC   : = EXE_OR; alusel  aluop =  EXE_RES_LOGIC;
      ORN | ORNCC   : = EXE_ORN; alusel  aluop =  EXE_RES_LOGIC;
      IXNOR | XNORCC   : = EXE_XNOR; alusel  aluop =  EXE_RES_LOGIC;
      XORCC | IXOR | WRPSR | WRWIM | WRTBR | WRY   : 
        = EXE_XOR; alusel  aluop =  EXE_RES_LOGIC;
      RDPSR | RDTBR | RDWIM  : aluop = EXE_SPR;
      RDY  : aluop = EXE_RDY;
      ISLL  : aluop = EXE_SLL; = EXE_RES_SHIFT; shleft  alusel =  1'b1; 
                   =  !iop2[4 : 0]; invop2  shcnt =  1'b1;
      ISRL  : = EXE_SRL; alusel  aluop =  EXE_RES_SHIFT; 
      ISRA  : aluop = EXE_SRA; = EXE_RES_SHIFT; sari  alusel =  iop1[31];
      FPOP1 | FPOP2  :
      default : 
      endcase 
    default :       // LDST
      case (r.a.ctrl.cnt) 
      2'b00  :
        alusel = EXE_RES_ADD;
      2'b01  :
        case (op3) 
        LDD | LDDA | LDDC  : alusel = EXE_RES_ADD;
        LDDF  : alusel = EXE_RES_ADD;
        SWAP | SWAPA | LDSTUB | LDSTUBA | CASA  : alusel = EXE_RES_ADD;
        STF | STDF  :
        default : 
          aluop = EXE_PASS1;
          if (op3[2] == 1'b1) begin  
            if (op3[1 : 0] == 2'b01) begin  aluop = EXE_STB;
           end else if (op3[1 : 0] == 2'b10) begin  aluop = EXE_STH;  end 
           end 
        endcase 
      2'b10  :
        aluop = EXE_PASS1;
        if (op3[2] == 1'b1) begin   // ST
          if ((op3[3] &&  !op3[5] &&  !op3[1])= 1'b1) begin  aluop = EXE_ONES;  end  // LDSTUB
         end 
        if (CASAEN && (r.m.casa == 1'b1)) begin 
          alusel = EXE_RES_ADD; aluadd = 1'b0; =  !iop2; invop2  aop2 =  1'b1;
         end 
      default : 
      endcase 
    endcase 
 end 
 endfunction 
  function ra_inull_gen(r, registers v )  
   logic   de_inull ;
  begin
    de_inull = 1'b0;
    if (((v.e.jmpl || v.e.ctrl.rett) &&  !v.e.ctrl.annul &&  !(r.e.jmpl &&  !r.e.ctrl.annul)) == 1'b1) begin  de_inull = 1'b1;  end 
    if (((v.a.jmpl || v.a.ctrl.rett) &&  !v.a.ctrl.annul &&  !(r.a.jmpl &&  !r.a.ctrl.annul)) == 1'b1) begin  de_inull = 1'b1;  end 
    return[de_inull];    
 end 
 endfunction 
// operand generation

  function op_mux(input  registers r ; rfd, ed, md, input  word xd, im ; 
        input   logic  [2 : 0] rsel ; 
        output   logic   ldbp ; output  word d ; std_logic id ) ;
  begin
    ldbp = 1'b0;
    case (rsel) 
    3'b000  : d = rfd;
    3'b001  : d = ed;
    3'b010  : = md; if (lddel == 1) begin  ldbp  d =  r.m.ctrl.ld;  end 
    3'b011  : d = xd;
    3'b100  : d = im;
    3'b101  : d =  `0 ;
    3'b110  : d = r.w.result;
    default :  d = (others => '-');
    endcase 
    if (CASAEN && (r.a.ctrl.cnt == 2'b10) && ((r.m.casa &&  !id) == 1'b1)) begin  ldbp = 1'b1;  end 
 end 
 endfunction 
  function op_find(input  registers r ; std_ulogic ldchkra ; logic   ldchkex ;
         logic  [4 : 0] rs1; rfatype ra ; bit   im ; output   logic   rfe ; 
        output   logic  [2 : 0] osel ; logic   ldcheck ) ;
  begin
    rfe = 1'b0;
    if (im) begin  osel = 3'b100;
   end else if (rs1 == 5'b00000) begin  osel = 5'b101;     // %g0
   end else if (((r.a.ctrl.wreg && ldchkra) == 1'b1) && (ra == r.a.ctrl.rd)) begin  osel = 3'b001;
   end else if (((r.e.ctrl.wreg && ldchkex) == 1'b1) && (ra == r.e.ctrl.rd)) begin  osel = 3'b010;                                        
   end else if ((r.m.ctrl.wreg == 1'b1) && (ra == r.m.ctrl.rd)) begin  osel = 3'b011;             
   end else if ((irfwt == 0) && (r.x.ctrl.wreg == 1'b1) && (ra == r.x.ctrl.rd)) begin  osel = 3'b110; 
    else  = 3'b000; rfe  osel =  ldcheck;  end 
 end 
 endfunction 
// generate carry-in for alu

  function cin_gen(registers r ; input  std_ulogic me_cin ; output   logic   cin ) ;
   logic  [1 : 0] op ;
   logic  [5 : 0] op3 ;
   logic   ncin ;
  begin

    = r.a.ctrl.inst[31 : 30]; op3  op =  r.a.ctrl.inst[24 : 19];
    if (r.e.ctrl.wicc == 1'b1) begin  ncin = me_cin;
    else ncin = r.m.icc[0];  end 
    cin = 1'b0;
    case (op) 
    FMT3  :
      case (op3) 
      ISUB | SUBCC | TSUBCC | TSUBCCTV  : cin = 1'b1;
      ADDX | ADDXCC  : cin = ncin; 
      SUBX | SUBXCC  : cin =  !ncin; 
      null default ;
      endcase 
    LDST  :
      if (CASAEN && (r.m.casa == 1'b1) && (r.a.ctrl.cnt == 2'b10)) begin 
        cin = 1'b1;
       end 
    null default ;
    endcase 
 end 
 endfunction 
  function logic_op(registers r ; aluin1, aluin2, word mey ; 
        logic   ymsb ; output  word logicres, y ) ;
  word logicout ;
  begin
    case (r.e.aluop) 
    EXE_AND    : logicout = aluin1 && aluin2;
    EXE_ANDN   : logicout = aluin1 &&  !aluin2;
    EXE_OR     : logicout = aluin1 || aluin2;
    EXE_ORN    : logicout = aluin1 ||  !aluin2;
    EXE_XOR    : logicout = aluin1 xor aluin2;
    EXE_XNOR   : logicout = aluin1 xor  !aluin2;
    EXE_DIV    : 
      if (DIVEN) begin  logicout = aluin2;
      else logicout = (others => '-');  end 
    default :  logicout = (others => '-');
    endcase 
    if ((r.e.ctrl.wy && r.e.mulstep) == 1'b1) begin  
      y = ymsb & r.m.y[31 : 1]; 
   end else if (r.e.ctrl.wy == 1'b1) begin  y = logicout;
   end else if (r.m.ctrl.wy == 1'b1) begin  y = mey; 
   end else if (MACPIPE && (r.x.mac == 1'b1)) begin  y = mulo.result[63 : 32];
   end else if (r.x.ctrl.wy == 1'b1) begin  y = r.x.y; 
    else y = r.w.s.y;  end 
    logicres = logicout;
 end 
 endfunction 
  function st_align(logic  [1 : 0] size; word bpdata )  
  word edata ;
  begin
    case (size) 
    2'b01    : edata = bpdata[7 : 0] & bpdata[7 : 0] &
                             bpdata[7 : 0] & bpdata[7 : 0];
    2'b10    : edata = bpdata[15 : 0] & bpdata[15 : 0];
    default :  edata = bpdata;
    endcase 
    return[edata];
 end 
 endfunction 
  function misc_op(registers r ; watchpoint_registers wpr ; 
        aluin1, aluin2, ldata, word mey ; 
        output  word mout, edata ) ;
  word miscout, bpdata, stdata ;
  integer wpi ;
  begin
    = 0; miscout  wpi =  r.e.ctrl.pc[31 : 2] & 2'b00; 
    = aluin1; bpdata  edata =  aluin1;
    if ((r.x.ctrl.wreg && r.x.ctrl.ld &&  !r.x.ctrl.annul) == 1'b1) and
       (r.x.ctrl.rd == r.e.ctrl.rd) && (r.e.ctrl.inst[31 : 30] == LDST) and
        (r.e.ctrl.cnt != 2'b10)
    begin  bpdata = ldata;  end 

    case (r.e.aluop) 
    EXE_STB    : miscout = bpdata[7 : 0] & bpdata[7 : 0] &
                             bpdata[7 : 0] & bpdata[7 : 0];
                      edata = miscout;
    EXE_STH    : miscout = bpdata[15 : 0] & bpdata[15 : 0];
                      edata = miscout;
    EXE_PASS1  : = bpdata; edata  miscout =  miscout;
    EXE_PASS2  : miscout = aluin2;
    EXE_ONES   : miscout =  `1 ;
                      edata = miscout;
    EXE_RDY   : 
      if (MULEN && (r.m.ctrl.wy == 1'b1)) begin  miscout = mey;
      else miscout = r.m.y;  end 
      if ((NWP > 0) && (r.e.ctrl.inst[18 : 17] == 2'b11)) begin 
        wpi = conv_integer(r.e.ctrl.inst[16 : 15]);
        if (r.e.ctrl.inst[14] == 1'b0) begin  miscout = wpr[wpi].addr & 1'b0 & wpr[wpi].exec;
        else miscout = wpr[wpi].mask & wpr[wpi].load & wpr[wpi].store;  end 
       end 
      if ((r.e.ctrl.inst[18 : 17] == 2'b10) && (r.e.ctrl.inst[14] == 1'b1)) begin  //%ASR17
        miscout = asr17_gen[r];
       end 

      if (MACEN) begin 
        if ((r.e.ctrl.inst[18 : 14] == 5'b10010)) begin  //%ASR18
          if (((r.m.mac == 1'b1) &&  !MACPIPE) || ((r.x.mac == 1'b1) && MACPIPE)) begin 
            miscout = mulo.result[31 : 0];        // data forward of asr18
          else miscout = r.w.s.asr18;  end 
        else
          if (((r.m.mac == 1'b1) &&  !MACPIPE) || ((r.x.mac == 1'b1) && MACPIPE)) begin 
            miscout = mulo.result[63 : 32];   // data forward Y
           end 
         end 
       end 
    EXE_SPR   : 
      miscout = get_spr[r];
    null default ;
    endcase 
    mout = miscout;
 end 
 endfunction 
  function alu_select(registers r ; logic  [32 : 0] addout;
        op1, word op2 ; shiftout, logicout, word miscout ; output  word res ; 
        logic  [3 : 0] me_icc;
        output   logic  [3 : 0] icco ; output   logic   divz, mzero ) ;
   logic  [1 : 0] op ;
   logic  [5 : 0] op3 ;
   logic  [3 : 0] icc ;
  word aluresult ;
  std_logic azero ;
  begin
    = r.e.ctrl.inst[31 : 30]; op3   op =  r.e.ctrl.inst[24 : 19];
    icc =  `0 ;
    if (addout[32 : 1] == zero32) begin  = 1'b1; else azero  azero =  1'b0;  end 
    mzero = azero;
    case (r.e.alusel) 
    EXE_RES_ADD  : 
      aluresult = addout[32 : 1];
      if (r.e.aluadd == 1'b0) begin 
        icc[0] = ( (!op1[31]) &&  !op2[31]) ||    // Carry
                 (addout[32] && ( (!op1[31]) ||  !op2[31]));
        icc[1] = (op1[31] && (op2[31]) &&  !addout[32]) ||         // Overflow
                 (addout[32] &&  (!op1[31]) &&  !op2[31]);
      else
        icc[0] = (op1[31] && op2[31]) ||      // Carry
                 ( (!addout[32]) && (op1[31] || op2[31]));
        icc[1] = (op1[31] && op2[31] &&  !addout[32]) ||   // Overflow
                 (addout[32] &&  (!op1[31]) &&  (!op2[31]));
       end 
      if (notag == 0) begin 
        case (op)  
        FMT3  :
          case (op3) 
          TADDCC | TADDCCTV  :
            icc[1] = op1[0] || op1[1] || op2[0] || op2[1] || icc[1];
          TSUBCC | TSUBCCTV  :
            icc[1] = op1[0] || op1[1] ||  (!op2[0]) ||  (!op2[1]) || icc[1];
          null default ;
          endcase 
        null default ;
        endcase 
       end 

//      if (aluresult == zero32) begin  icc[2] = 1'b1;  end 
      icc[2] = azero;
    EXE_RES_SHIFT  : aluresult = shiftout;
    EXE_RES_LOGIC  : aluresult = logicout;
      if (aluresult == zero32) begin  icc[2] = 1'b1;  end 
    default :  aluresult = miscout;
    endcase 
    if (r.e.jmpl == 1'b1) begin  aluresult = r.e.ctrl.pc[31 : 2] & 2'b00;  end 
    icc[3] = aluresult[31]; divz = icc[2];
    if (r.e.ctrl.wicc == 1'b1) begin 
      if ((op == FMT3) && (op3 == WRPSR)) begin  icco = logicout[23 : 20];
      else icco = icc;  end 
   end else if (r.m.ctrl.wicc == 1'b1) begin  icco = me_icc;
   end else if (r.x.ctrl.wicc == 1'b1) begin  icco = r.x.icc;
    else icco = r.w.s.icc;  end 
    res = aluresult;
 end 
 endfunction 
  function dcache_gen(r, registers v ; output  dc_in_type dci ; 
        link_pc, jump, force_a2, output   logic   load, mcasa ) ;
   logic  [1 : 0] op ;
   logic  [5 : 0] op3 ;
   logic   su, lock ;
  begin
    = r.e.ctrl.inst[31 : 30]; op3  op =  r.e.ctrl.inst[24 : 19];
    dci.signed = 1'b0; dci.lock = 1'b0; dci.dsuen = 1'b0; dci.size = SZWORD;
    mcasa = 1'b0;
    if (op == LDST) begin 
    case (op3) 
      LDUB | LDUBA  : dci.size = SZBYTE;
      LDSTUB | LDSTUBA  : dci.size = SZBYTE; dci.lock = 1'b1; 
      LDUH | LDUHA  : dci.size = SZHALF;
      LDSB | LDSBA  : dci.size = SZBYTE; dci.signed = 1'b1;
      LDSH | LDSHA  : dci.size = SZHALF; dci.signed = 1'b1;
      LD | LDA | LDF | LDC  : dci.size = SZWORD;
      SWAP | SWAPA  : dci.size = SZWORD; dci.lock = 1'b1; 
      CASA  : if (CASAEN) begin  dci.size = SZWORD; dci.lock = 1'b1;  end 
      LDD | LDDA | LDDF | LDDC  : dci.size = SZDBL;
      STB | STBA  : dci.size = SZBYTE;
      STH | STHA  : dci.size = SZHALF;
      ST | STA | STF  : dci.size = SZWORD;
      ISTD | STDA  : dci.size = SZDBL;
      STDF | STDFQ  : if (FPEN) begin  dci.size = SZDBL;  end 
      STDC | STDCQ  : if (CPEN) begin  dci.size = SZDBL;  end 
      default :  dci.size = SZWORD; dci.lock = 1'b0; dci.signed = 1'b0;
    endcase 
     end 

    link_pc = 1'b0; jump:= 1'b0; = 1'b0; load  force_a2 =  1'b0;
    dci.write = 1'b0; dci.enaddr = 1'b0; dci.read =  !op3[2];

// load/store control decoding

    if ((r.e.ctrl.annul || r.e.ctrl.trap) == 1'b0) begin 
      case (op) 
      CALL  : link_pc = 1'b1;
      FMT3  :
        if (r.e.ctrl.trap == 1'b0) begin 
          case (op3) 
          JMPL  : = 1'b1; link_pc  jump =  1'b1; 
          RETT  : jump = 1'b1;
          null default ;
          endcase 
         end 
      LDST  :
          case (r.e.ctrl.cnt) 
          2'b00  :
            dci.read = op3[3] ||  !op3[2];   // LD/LDST/SWAP/CASA
            load = op3[3] ||  !op3[2];
            //dci.enaddr = 1'b1;
            dci.enaddr =  (!op3[2]) || op3[2]
                          || (op3[3] && op3[2]);
          2'b01  :
            force_a2 =  !op3[2];     // LDD
            =  !op3[2]; dci.enaddr  load ==   !op3[2];
            if (op3[3 : 2] == 2'b01) begin               // ST/STD
              dci.write = 1'b1;              
             end 
            if (CASAEN && (op3[5 : 4] == 2'b11)) || // CASA
                (op3[3 : 2] == 2'b11) begin            // LDST/SWAP
              dci.enaddr = 1'b1;
             end 
          2'b10  :                                  // STD/LDST/SWAP/CASA
            dci.write = 1'b1;
          null default ;
          endcase 
          if ((r.e.ctrl.trap || (v.x.ctrl.trap &&  !v.x.ctrl.annul)) == 1'b1) begin  
            dci.enaddr = 1'b0;
           end 
          if ((CASAEN && (op3[5 : 4] == 2'b11))) begin  mcasa = 1'b1;  end 
      null default ;
      endcase 
     end 

    if (((r.x.ctrl.rett &&  !r.x.ctrl.annul) == 1'b1)) begin  su = r.w.s.ps;
    else su = r.w.s.s;  end 
    if (su == 1'b1) begin  dci.asi = 8'b00001011; else dci.asi = 8'b00001010;  end 
    if ((op3[4] == 1'b1) && ((op3[5] == 1'b0) ||  !CPEN)) begin 
      dci.asi = r.e.ctrl.inst[12 : 5];
     end 

 end 
 endfunction 
  function fpstdata(input  registers r ; input  word edata, eres ; input   logic  [31 : 0] fpstdata ;
                       output  word edata2, eres2 ) ;
     logic  [1 : 0] op ;
     logic  [5 : 0] op3 ;
  begin
    = edata; eres2  edata2 =  eres;
    = r.e.ctrl.inst[31 : 30]; op3  op =  r.e.ctrl.inst[24 : 19];
    if (FPEN) begin 
      if (FPEN && (op == LDST) &&  ((op3[5 : 4] & op3[2]) == 3'b101) && (r.e.ctrl.cnt != 3'b00)) begin 
        = fpstdata; eres2  edata2 =  fpstdata;
       end 
     end 
    if (CASAEN && (r.m.casa == 1'b1) && (r.e.ctrl.cnt == 2'b10)) begin 
      = r.e.op1; eres2  edata2 =  r.e.op1;
     end 
 end 
 endfunction   
  function ld_align(dcdtype data ; logic  [DSETMSB : 0] set;
        size, logic  [1 : 0] laddr; logic   signed )  
  word align_data, rdata ;
  begin
    = data(conv_integer[set]); rdata  align_data ==   `0 ;
    case (size) 
    2'b00  :                        // byte read
      case (laddr) 
      2'b00  : 
        rdata[7 : 0] = align_data[31 : 24];
        if (signed == 1'b1) begin  rdata[31 : 8] = (others => align_data[31]);  end 
      2'b01  : 
        rdata[7 : 0] = align_data[23 : 16];
        if (signed == 1'b1) begin  rdata[31 : 8] = (others => align_data[23]);  end 
      2'b10  : 
        rdata[7 : 0] = align_data[15 : 8];
        if (signed == 1'b1) begin  rdata[31 : 8] = (others => align_data[15]);  end 
      default :  
        rdata[7 : 0] = align_data[7 : 0];
        if (signed == 1'b1) begin  rdata[31 : 8] = (others => align_data[7]);  end 
      endcase 
    2'b01  :                        // half-word read
      if (laddr[1] == 1'b1) begin  
        rdata[15 : 0] = align_data[15 : 0];
        if (signed == 1'b1) begin  rdata[31 : 15] = (others => align_data[15]);  end 
      else
        rdata[15 : 0] = align_data[31 : 16];
        if (signed == 1'b1) begin  rdata[31 : 15] = (others => align_data[31]);  end 
       end 
    default :                       // single && double word read
      rdata = align_data;
    endcase 
    return[rdata];
 end 
 endfunction 
  
  function mem_trap(registers r ; watchpoint_registers wpr ;
                     input   logic   annul, holdn ;
                     trapout, iflush, output   logic   nullify, werrout ;
                     output   logic  [5 : 0] tt ) ;
   logic  [NWINLOG2-1 : 0] cwp   ;
   logic  [5 : NWINLOG2] cwpx  ;
   logic  [1 : 0] op ;
   logic  [2 : 0] op2 ;
   logic  [5 : 0] op3 ;
   logic   nalign_d ;
   logic   trap, werr ;
  begin
    = r.m.ctrl.inst[31 : 30]; op2   op =  r.m.ctrl.inst[24 : 22];
    op3 = r.m.ctrl.inst[24 : 19];
    = r.m.result[5 : NWINLOG2]; cwpx[5]  cwpx =  1'b0;
    iflush = 1'b0; = r.m.ctrl.trap; nullify  trap =  annul;
    = r.m.ctrl.tt; werr  tt =  (dco.werr || r.m.werr) &&  !r.w.s.dwt;
    nalign_d = r.m.nalign || r.m.result[2]; 
    if ((trap == 1'b1) && (r.m.ctrl.pv == 1'b1)) begin 
      if (op == LDST) begin  nullify = 1'b1;  end 
     end 
    if (((annul || trap) != 1'b1) && (r.m.ctrl.pv == 1'b1)) begin 
      if ((werr && holdn) == 1'b1) begin 
        trap = 1'b1; = TT_DSEX; werr  tt =  1'b0;
        if (op == LDST) begin  nullify = 1'b1;  end 
       end 
     end 
    if (((annul || trap) != 1'b1)) begin       
      case (op) 
      FMT2  :
        case (op2) 
        FBFCC  : 
          if (FPEN && (fpo.exc == 1'b1)) begin  = 1'b1; tt  trap =  TT_FPEXC;  end 
        CBCCC  :
          if (CPEN && (cpo.exc == 1'b1)) begin  = 1'b1; tt  trap =  TT_CPEXC;  end 
        null default ;
        endcase 
      FMT3  :
        case (op3) 
        WRPSR  :
          if ((orv[cwpx] == 1'b1)) begin  = 1'b1; tt  trap =  TT_IINST;  end 
        UDIV | SDIV | UDIVCC | SDIVCC  :
          if (DIVEN) begin  
            if (r.m.divz == 1'b1) begin  = 1'b1; tt  trap =  TT_DIV;  end 
           end 
        JMPL | RETT  :
          if (r.m.nalign == 1'b1) begin  = 1'b1; tt  trap =  TT_UNALA;  end 
        TADDCCTV | TSUBCCTV  :
          if ((notag == 0) && (r.m.icc[1] == 1'b1)) begin 
            = 1'b1; tt  trap =  TT_TAG;
           end 
        FLUSH  : iflush = 1'b1;
        FPOP1 | FPOP2  :
          if (FPEN && (fpo.exc == 1'b1)) begin  = 1'b1; tt  trap =  TT_FPEXC;  end 
        CPOP1 | CPOP2  :
          if (CPEN && (cpo.exc == 1'b1)) begin  = 1'b1; tt  trap =  TT_CPEXC;  end 
        null default ;
        endcase 
      LDST  :
        if (r.m.ctrl.cnt == 2'b00) begin 
          case (op3) 
            LDDF | STDF | STDFQ  :
            if (FPEN) begin 
              if (nalign_d == 1'b1) begin 
                trap = 1'b1; = TT_UNALA; nullify  tt =  1'b1;
             end else if (fpo.exc && r.m.ctrl.pv) = 1'b1 
              begin  trap = 1'b1; = TT_FPEXC; nullify  tt =  1'b1;  end 
             end 
          LDDC | STDC | STDCQ  :
            if (CPEN) begin 
              if (nalign_d == 1'b1) begin 
                trap = 1'b1; = TT_UNALA; nullify  tt =  1'b1;
             end else if ((cpo.exc && r.m.ctrl.pv) == 1'b1) 
              begin  trap = 1'b1; = TT_CPEXC; nullify  tt =  1'b1;  end 
             end 
          LDD | ISTD | LDDA | STDA  :
            if (r.m.result[2 : 0] != 3'b000) begin 
              trap = 1'b1; = TT_UNALA; nullify  tt =  1'b1;
             end 
          LDF | LDFSR | STFSR | STF  :
            if (FPEN && (r.m.nalign == 1'b1)) begin 
              trap = 1'b1; = TT_UNALA; nullify  tt =  1'b1;
           end else if FPEN && ((fpo.exc && r.m.ctrl.pv) == 1'b1)
            begin  trap = 1'b1; = TT_FPEXC; nullify  tt =  1'b1;  end 
          LDC | LDCSR | STCSR | STC  :
            if (CPEN && (r.m.nalign == 1'b1)) begin  
              trap = 1'b1; = TT_UNALA; nullify  tt =  1'b1;
           end else if CPEN && ((cpo.exc && r.m.ctrl.pv) == 1'b1) 
            begin  trap = 1'b1; = TT_CPEXC; nullify  tt =  1'b1;  end 
          LD | LDA | ST | STA | SWAP | SWAPA | CASA  :
            if (r.m.result[1 : 0] != 2'b00) begin 
              trap = 1'b1; = TT_UNALA; nullify  tt =  1'b1;
             end 
          LDUH | LDUHA | LDSH | LDSHA | STH | STHA  :
            if (r.m.result[0] != 1'b0) begin 
              trap = 1'b1; = TT_UNALA; nullify  tt =  1'b1;
             end 
          null default ;
          endcase 
          for i in 1 to NWP loop
            if ((((wpr(i-1).load &&  !op3[2]) || (wpr(i-1).store && op3[2])) == 1'b1) and
                (((wpr(i-1).addr xor r.m.result[31 : 2]) && wpr(i-1).mask) == zero32[31 : 2]))
            begin  trap = 1'b1; = TT_WATCH; nullify  tt =  1'b1;  end 
          end loop;
         end 
      null default ;
      endcase 
     end 
    if ((rstn == 1'b0) || (r.x.rstate == dsu2)) begin  werr = 1'b0;  end 
    = trap; werrout  trapout =  werr;
 end 
 endfunction 
  function irq_trap(input  registers r       ;
                     input  irestart_register ir      ;
                     input   logic  [3 : 0] irl     ;
                     input   logic   annul   ;
                     input   logic   pv      ;
                     input   logic   trap    ;
                     input   logic  [5 : 0] tt      ;
                     input   logic   nullify ;
                     output   logic   irqen   ;
                     output   logic   irqen2  ;
                     output   logic   nullify2 ;
                     output   logic   trap2, ipend ;
                     output   logic  [5 : 0] tt2      ) ;
     logic  [1 : 0] op ;
     logic  [5 : 0] op3 ;
     logic   pend ;
  begin
    nullify2 = nullify; = trap; tt2  trap2 =  tt; 
    = r.m.ctrl.inst[31 : 30]; op3  op =  r.m.ctrl.inst[24 : 19];
    = 1'b1; irqen2  irqen =  r.m.irqen;

    if ((annul || trap) == 1'b0) begin 
      if (((op == FMT3) && (op3 == WRPSR))) begin  irqen = 1'b0;  end     
     end 

    if ((irl == 4'b1111) || (irl > r.w.s.pil)) begin 
      pend = r.m.irqen && r.m.irqen2 && r.w.s.et &&  !ir.pwd
      ;
    else pend = 1'b0;  end 
    ipend = pend;

    if (( (!annul) && pv &&  (!trap) && pend) == 1'b1) begin 
      = 1'b1; tt2  trap2 =  2'b01 & irl;
      if (op == LDST) begin  nullify2 = 1'b1;  end 
     end 
 end 
 endfunction 
  function irq_intack(input  registers r ; input  std_ulogic holdn ; intack: out  logic  ) ;
  begin
    intack = 1'b0;
    if (r.x.rstate == trap) begin  
      if (r.w.s.tt[7 : 4] == 4'b0001) begin  intack = 1'b1;  end 
     end 
 end 
 endfunction   
// write special registers

  function sp_write (registers r ; watchpoint_registers wpr ;
        output  special_register_type s ; output  watchpoint_registers vwpr ) ;
   logic  [1 : 0] op ;
   logic  [2 : 0] op2 ;
   logic  [5 : 0] op3 ;
   logic  [4 : 0] rd  ;
  integer range 0 to 3 i   ;
  begin

    op  = r.x.ctrl.inst[31 : 30];
    op2 = r.x.ctrl.inst[24 : 22];
    op3 = r.x.ctrl.inst[24 : 19];
    s   = r.w.s;
    rd  = r.x.ctrl.inst[29 : 25];
    vwpr = wpr;
    
      case (op) 
      FMT3  :
        case (op3) 
        WRY  :
          if (rd == 5'b00000) begin 
            s.y = r.x.result;
         end else if (MACEN && (rd == 5'b10010)) begin 
            s.asr18 = r.x.result;
         end else if (rd == 5'b10001) begin 
            if (bp == 2) begin  s.dbp = r.x.result[27];  end 
            s.dwt = r.x.result[14];
            if (svt == 1) begin  s.svt = r.x.result[13];  end 
         end else if (rd[4 : 3] == 2'b11) begin  // %ASR24 - %ASR31
            case (rd[2 : 0]) 
            3'b000  : 
              vwpr[0].addr = r.x.result[31 : 2];
              vwpr[0].exec = r.x.result[0]; 
            3'b001  : 
              vwpr[0].mask = r.x.result[31 : 2];
              vwpr[0].load = r.x.result[1];
              vwpr[0].store = r.x.result[0];              
            3'b010  : 
              vwpr[1].addr = r.x.result[31 : 2];
              vwpr[1].exec = r.x.result[0]; 
            3'b011  : 
              vwpr[1].mask = r.x.result[31 : 2];
              vwpr[1].load = r.x.result[1];
              vwpr[1].store = r.x.result[0];              
            3'b100  : 
              vwpr[2].addr = r.x.result[31 : 2];
              vwpr[2].exec = r.x.result[0]; 
            3'b101  : 
              vwpr[2].mask = r.x.result[31 : 2];
              vwpr[2].load = r.x.result[1];
              vwpr[2].store = r.x.result[0];              
            3'b110  : 
              vwpr[3].addr = r.x.result[31 : 2];
              vwpr[3].exec = r.x.result[0]; 
            default :    // 3'b111
              vwpr[3].mask = r.x.result[31 : 2];
              vwpr[3].load = r.x.result[1];
              vwpr[3].store = r.x.result[0];              
            endcase 
           end 
        WRPSR  :
          s.cwp = r.x.result[NWINLOG2-1 : 0];
          s.icc = r.x.result[23 : 20];
          s.ec  = r.x.result[13];
          if (FPEN) begin  s.ef  = r.x.result[12];  end 
          s.pil = r.x.result[11 : 8];
          s.s   = r.x.result[7];
          s.ps  = r.x.result[6];
          s.et  = r.x.result[5];
        WRWIM  :
          s.wim = r.x.result[NWIN-1 : 0];
        WRTBR  :
          s.tba = r.x.result[31 : 12];
        SAVE  :
          if ((!CWPOPT) && (r.w.s.cwp == CWPMIN)) begin  s.cwp = CWPMAX;
          else s.cwp = r.w.s.cwp - 1 ;  end 
        RESTORE  :
          if ((!CWPOPT) && (r.w.s.cwp == CWPMAX)) begin  s.cwp = CWPMIN;
          else s.cwp = r.w.s.cwp + 1;  end 
        RETT  :
          if ((!CWPOPT) && (r.w.s.cwp == CWPMAX)) begin  s.cwp = CWPMIN;
          else s.cwp = r.w.s.cwp + 1;  end 
          s.s = r.w.s.ps;
          s.et = 1'b1;
        null default ;
        endcase 
      null default ;
      endcase 
      if (r.x.ctrl.wicc == 1'b1) begin  s.icc = r.x.icc;  end 
      if (r.x.ctrl.wy == 1'b1) begin  s.y = r.x.y;  end 
      if (MACPIPE && (r.x.mac == 1'b1)) begin  
        s.asr18 = mulo.result[31 : 0];
        s.y = mulo.result[63 : 32];
       end 
 end 
 endfunction 
  function npc_find (registers r )  
   logic  [2 : 0] npc ;
  begin
    npc = 3'b011;
    if (r.m.ctrl.pv == 1'b1) begin  npc = 3'b000;
   end else if (r.e.ctrl.pv == 1'b1) begin  npc = 3'b001;
   end else if (r.a.ctrl.pv == 1'b1) begin  npc = 3'b010;
   end else if (r.d.pv == 1'b1) begin  npc = 3'b011;
   end else if (v8 != 0) begin  npc = 3'b100;  end 
    return[npc];
 end 
 endfunction 
  function npc_gen (registers r )  
   logic  [31 : 0] npc ;
  begin
    npc =  r.a.ctrl.pc[31 : 2] & 2'b00;
    case (r.x.npc) 
    3'b000  : npc[31 : 2] = r.x.ctrl.pc[31 : 2];
    3'b001  : npc[31 : 2] = r.m.ctrl.pc[31 : 2];
    3'b010  : npc[31 : 2] = r.e.ctrl.pc[31 : 2];
    3'b011  : npc[31 : 2] = r.a.ctrl.pc[31 : 2];
    default :  
        if (v8 != 0) begin  npc[31 : 2] = r.d.pc[31 : 2];  end 
    endcase 
    return[npc];
 end 
 endfunction 
  function mul_res(registers r ; word asr18in ; result, output  word y, asr18 ; 
          output   logic  [3 : 0] icc ) ;
   logic  [1 : 0] op  ;
   logic  [5 : 0] op3 ;
  begin
    = r.m.ctrl.inst[31 : 30]; op3    op =  r.m.ctrl.inst[24 : 19];
    result = r.m.result; y = r.m.y; = r.m.icc; asr18  icc =  asr18in;
    case (op) 
    FMT3  :
      case (op3) 
      UMUL | SMUL  :
        if (MULEN) begin  
          result = mulo.result[31 : 0];
          y = mulo.result[63 : 32];
         end 
      UMULCC | SMULCC  :
        if (MULEN) begin  
          = mulo.result[31 : 0]; icc  result =  mulo.icc;
          y = mulo.result[63 : 32];
         end 
      UMAC | SMAC  :
        if (MACEN &&  !MACPIPE) begin 
          result = mulo.result[31 : 0];
          asr18  = mulo.result[31 : 0];
          y = mulo.result[63 : 32];
         end 
      UDIV | SDIV  :
        if (DIVEN) begin  
          result = divo.result[31 : 0];
         end 
      UDIVCC | SDIVCC  :
        if (DIVEN) begin  
          = divo.result[31 : 0]; icc  result =  divo.icc;
         end 
      null default ;
      endcase 
    null default ;
    endcase 
 end 
 endfunction 
  function powerdwn(registers r ; std_ulogic trap ; pwd_register_type rp )  
     logic  [1 : 0] op ;
     logic  [5 : 0] op3 ;
     logic  [4 : 0] rd  ;
     logic   pd  ;
  begin
    op = r.x.ctrl.inst[31 : 30];
    op3 = r.x.ctrl.inst[24 : 19];
    rd  = r.x.ctrl.inst[29 : 25];    
    pd = 1'b0;
    if ((!(r.x.ctrl.annul || trap) && r.x.ctrl.pv) == 1'b1) begin 
      if (((op == FMT3) && (op3 == WRY) && (rd == 5'b10011))) begin  pd = 1'b1;  end 
      pd = pd || rp.pwd;
     end 
    return[pd];
 end 
 endfunction 
  
  signal logic   dummy ;
  signal logic  [3 : 0] cpu_index;
  signal logic   disasen ;



  assign BPRED <= bp == 0 ? 1'b0  : bp == 1 ? 1'b1 :  !r.w.s.dbp;
 
 
 
  registers v    ;
  pwd_register_type vp  ;
  watchpoint_registers vwpr ;
  dsu_registers vdsu ;
    logic  [31 : PCLOW] fe_pc, fe_npc ;
   logic  [31 : PCLOW] npc  ;
   logic  [9 : 0] de_raddr1, de_raddr2 ;
   logic  [4 : 0] de_rs2, de_rd ;
   logic   de_hold_pc, de_branch, de_ldlock ;
  cwptype de_cwp, de_cwp2 ;
   logic   de_inull ;
   logic   de_ren1, de_ren2 ;
   logic   de_wcwp ;
  word de_inst ;
   logic  [3 : 0] de_icc ;
   logic   de_fbranch, de_cbranch ;
   logic   de_rs1mod ;
   logic   de_bpannul ;
   logic   de_fins_hold ;
   logic   de_iperr ;

  word ra_op1, ra_op2 ;
   logic   ra_div ;
   logic   ra_bpmiss ;
   logic   ra_bpannul ;

   logic   ex_jump, ex_link_pc ;
  pctype ex_jump_address ;
   logic  [32 : 0] ex_add_res ;
  word ex_shift_res, ex_logic_res, ex_misc_res ;
  word ex_edata, ex_edata2 ;
  dc_in_type ex_dci ;
   logic   ex_force_a2, ex_load, ex_ymsb ;
  word ex_op1, ex_op2, ex_result, ex_result2, ex_result3, mul_op2 ;
   logic  [4 : 0] ex_shcnt ;
   logic   ex_dsuen ;
   logic   ex_ldbp2 ;
   logic   ex_sari ;
   logic   ex_bpmiss ;

   logic  [31 : 0] ex_cdata ;
   logic  [32 : 0] ex_mulop1, ex_mulop2 ;
  
  word me_bp_res ;
   logic   me_inull, me_nullify, me_nullify2 ;
   logic   me_iflush ;
   logic  [5 : 0] me_newtt ;
  word me_asr18 ;
   logic   me_signed ;
   logic  [1 : 0] me_size, me_laddr ;
   logic  [3 : 0] me_icc ;

  
  word xc_result ;
  word xc_df_result ;
   logic  [9 : 0] xc_waddr ;
   logic   xc_exception, xc_wreg ;
  pctype xc_trap_address ;
   logic  [7 : 0] xc_newtt, xc_vectt ;
   logic   xc_trap ;
   logic   xc_fpexack ;  
   logic   xc_rstn, xc_halt ;
  
  word diagdata ;
  tracebuf_in_type tbufi ;
   logic   dbgm ;
   logic   fpcdbgwr ;
  fpc_in_type vfpi ;
   logic   dsign ;
   logic   pwrd, sidle ;
  irestart_register vir ;
   logic   xc_dflushl  ;
   logic   xc_dcperr ;
   logic   st ;
   logic   icnt, fcnt ;
   logic  [TBUFBITS-1 : 0] tbufcntx ;
   logic   bpmiss ;
  
  

//  comb : process(ico, dco, rfo, r, wpr, ir, dsur, rstn, holdn, irqi, dbgi, fpo, cpo, tbo,
//                 mulo, divo, dummy, rp, BPRED)

always_comb 
  begin
    v = r; 
    vwpr = wpr; 
    vdsu= dsur;   
    vp =  rp;
    sidle= 1'b0;  
    xc_fpexack =  1'b0;
    fpcdbgwr = 1'b0; 
    vir = ir; 
    xc_rstn =  rstn;
    
// EXCEPTION STAGE

    xc_exception = 1'b0; 
    xc_halt = 1'b0; 
    icnt = 1'b0; 
    fcnt   =  1'b0;
    xc_waddr =  `0 ;
    xc_waddr[RFBITS-1 : 0] = r.x.ctrl.rd[RFBITS-1 : 0];
    xc_trap = r.x.mexc || r.x.ctrl.trap;
    v.x.nerror = rp.error; xc_dflushl = 1'b0;

    if (r.x.mexc == 1'b1) begin  
      xc_vectt = {2'b00, TT_DAEX};
    end else if (r.x.ctrl.tt == TT_TICC) begin 
      xc_vectt = {1'b1 , r.x.result[6 : 0]};
    end else begin
      xc_vectt = {2'b00 , r.x.ctrl.tt};
    end 

    if (r.w.s.svt == 1'b0) begin 
      xc_trap_address[31 : 2] = {r.w.s.tba , xc_vectt , 2'b00}; 
    end else begin
      xc_trap_address[31 : 2] = {r.w.s.tba , 8'b00000000 , 8'b00}; 
    end 
      xc_trap_address[2 : PCLOW] =  `0 ;
     v.x.annul_all= 1'b0; 
     xc_wreg =  1'b0; 

    if ((!r.x.ctrl.annul && r.x.ctrl.ld) == 1'b1) begin  
      if (lddel == 2) begin  
        xc_result = ld_align(r.x.data, r.x.set, r.x.dci.size, r.x.laddr, r.x.dci.signed);
      end else begin
        xc_result = r.x.data[0]; 
      end 
   end else if (MACEN && MACPIPE && ( (!r.x.ctrl.annul && r.x.mac) == 1'b1)) begin 
      xc_result = mulo.result[31 : 0];
   end else begin 
      xc_result = r.x.result; 
   end 
      xc_df_result = xc_result;

    
  if (DBGUNIT)
    begin  
      dbgm = dbgexc(r, dbgi, xc_trap, xc_vectt);
      if ((dbgi.dsuen && dbgi.dbreak) == 1'b0) begin
        v.x.debug = 1'b0;  
      end 
    end else begin
      v.x.debug= 1'b0;   
      dbgm =  1'b0;  
    end 
    if (PWRD2) begin  
        pwrd = powerdwn(r, xc_trap, rp); 
     end else begin  pwrd =  1'b0;  end 
    
    case (r.x.rstate) 
    run  :
      if (dbgm != 1'b0) begin         
        v.x.annul_all = 1'b1; 
        vir.addr = r.x.ctrl.pc;
        v.x.rstate = dsu1;
        v.x.debug = 1'b1; 
        v.x.npc = npc_find[r];
        vdsu.tt = xc_vectt; 
        vdsu.err = dbgerr(r, dbgi, xc_vectt);
     end else if ((pwrd == 1'b1) && (ir.pwd == 1'b0)) begin 
        v.x.annul_all = 1'b1; 
        vir.addr = r.x.ctrl.pc;
        v.x.rstate = dsu1; 
        v.x.npc = npc_find[r]; 
        vp.pwd = 1'b1;
     end else if ((r.x.ctrl.annul || xc_trap) == 1'b0) begin 
        xc_wreg = r.x.ctrl.wreg;
        sp_write (r, wpr, v.w.s, vwpr);
        vir.pwd = 1'b0;
        if ((r.x.ctrl.pv &&  !r.x.debug) == 1'b1) begin 
          icnt = holdn;
          if (r.x.ctrl.inst[31 : 30] == FMT3) && 
                ((r.x.ctrl.inst[24 : 19] == FPOP1) || 
                 (r.x.ctrl.inst[24 : 19] == FPOP2))
          begin  fcnt = holdn;  end 
         end 
     end else if (( (!r.x.ctrl.annul) && xc_trap) == 1'b1) begin 
        xc_exception = 1'b1; 
        xc_result   =  {r.x.ctrl.pc[31 : 2] , 2'b00};
        xc_wreg = 1'b1; 
        v.w.s.tt = xc_vectt; 
        v.w.s.ps  =  r.w.s.s;
        v.w.s.s = 1'b1; 
        v.x.annul_all = 1'b1; 
        v.x.rstate = trap;
        xc_waddr =  `0 ;
        xc_waddr[NWINLOG2 + 3  : 0] =  {r.w.s.cwp , 4'b0001};
        v.x.npc = npc_find[r];
        fpexack(r, xc_fpexack);
        if (r.w.s.et == 1'b0) begin 
//        v.x.rstate = dsu1; = 1'b0; vp.error  xc_wreg =  1'b1;
          xc_wreg = 1'b0;
         end 
       end 
    trap  :
      xc_result= npc_gen[r]; 
      xc_wreg   =  1'b1;
      xc_waddr =  `0 ;
      xc_waddr[NWINLOG2 + 3 : 0] =  {r.w.s.cwp , 4'b0010};
      if (r.w.s.et == 1'b1) begin 
        v.w.s.et = 1'b0; 
        v.x.rstate = run;
        if ((!CWPOPT) && (r.w.s.cwp == CWPMIN)) begin 
            v.w.s.cwp = CWPMAX;
        end else begin v.w.s.cwp = r.w.s.cwp - 1 ;  end 
      end else begin
        v.x.rstate = dsu1; 
        xc_wreg = 1'b0; 
        vp.error   =  1'b1;
      end 
    dsu1  :
      xc_exception = 1'b1; 
      v.x.annul_all   =  1'b1;
      xc_trap_address[31 : PCLOW] = r.f.pc;
      if (DBGUNIT || PWRD2 || (smp != 0))
      begin  
        xc_trap_address[31 : PCLOW] = ir.addr; 
        vir.addr = npc_gen[r][31 : PCLOW];
        v.x.rstate = dsu2;
       end 
      if (DBGUNIT) begin  v.x.debug = r.x.debug;  end 
    dsu2  :      
      xc_exception = 1'b1; 
      v.x.annul_all   =  1'b1;
      xc_trap_address[31 : PCLOW] = r.f.pc;
      if (DBGUNIT || PWRD2 || (smp != 0))
      begin 
        sidle = (rp.pwd || rp.error) && ico.idle && dco.idle &&  !r.x.debug;
        if (DBGUNIT) begin 
          if (dbgi.reset == 1'b1) begin  
            if (smp !=0) begin  vp.pwd =  !irqi.run; 
            end else  begin vp.pwd = 1'b0;  end 
            vp.error = 1'b0;
          end 
          if ((dbgi.dsuen && dbgi.dbreak) == 1'b1)  begin
            v.x.debug = 1'b1;  end 
          diagwr(r, dsur, ir, dbgi, wpr, v.w.s, vwpr, vdsu.asi, xc_trap_address,
               vir.addr, vdsu.tbufcnt, xc_wreg, xc_waddr, xc_result, fpcdbgwr);
          xc_halt = dbgi.halt;
         end 
        if (r.x.ipend == 1'b1) begin  vp.pwd = 1'b0;  end 
        if ((rp.error || rp.pwd || r.x.debug || xc_halt) == 1'b0) begin 
          v.x.rstate = run; 
          v.x.annul_all = 1'b0;
          vp.error = 1'b0;
          xc_trap_address[31 : PCLOW] = ir.addr; 
          v.x.debug = 1'b0;
          vir.pwd = 1'b1;
         end 
        if ((smp != 0) && (irqi.rst == 1'b1)) begin  
          vp.pwd = 1'b0; vp.error = 1'b0; 
         end 
       end 
    default : 
    endcase 

    dci.flushl = xc_dflushl;

    
    irq_intack(r, holdn, v.x.intack);          
    itrace(r, dsur, vdsu, xc_result, xc_exception, dbgi, rp.error, xc_trap, tbufcntx, tbufi, 1'b0, xc_dcperr);    
    vdsu.tbufcnt = tbufcntx;
    
    v.w.except = xc_exception; 
    v.w.result = xc_result;
    if ((r.x.rstate == dsu2)) begin  v.w.except = 1'b0;  end 
    v.w.wa = xc_waddr[RFBITS-1 : 0]; v.w.wreg = xc_wreg && holdn;

    rfi.diag = dco.testen & dco.scanen & 2'b00;
    rfi.wdata = xc_result; 
    rfi.waddr = xc_waddr;

    irqo.intack = r.x.intack && holdn;
    irqo.irl   = r.w.s.tt[3 : 0];
    irqo.pwd   = rp.pwd;
    irqo.fpen  = r.w.s.ef;
    irqo.idle  = 1'b0;
    dbgo.halt  = xc_halt;
    dbgo.pwd   = rp.pwd;
    dbgo.idle  = sidle;
    dbgo.icnt  = icnt;
    dbgo.fcnt  = fcnt;
    dbgo.optype = {r.x.ctrl.inst[31 : 30] , r.x.ctrl.inst[24 : 21]};
    dci.intack = r.x.intack && holdn;    
    
    if ((!RESET_ALL) && (xc_rstn == 1'b0)) begin  
      v.w.except = RRES.w.except; 
      v.w.s.et = RRES.w.s.et;
      v.w.s.svt = RRES.w.s.svt; 
      v.w.s.dwt = RRES.w.s.dwt;
      v.w.s.ef = RRES.w.s.ef;
      if (need_extra_sync_reset[fabtech] != 0) begin  
        v.w.s.cwp = RRES.w.s.cwp;
        v.w.s.icc = RRES.w.s.icc;
      end 
      v.w.s.dbp = RRES.w.s.dbp;
      v.x.ipmask = RRES.x.ipmask;
      v.w.s.tba = RRES.w.s.tba;
      v.x.annul_all = RRES.x.annul_all;
      v.x.rstate = RRES.x.rstate; 
      vir.pwd = IRES.pwd; 
      vp.pwd = PRES.pwd; 
      v.x.debug = RRES.x.debug; 
      v.x.nerror = RRES.x.nerror;
      if (svt == 1) begin  v.w.s.tt = RRES.w.s.tt;  end 
      if (DBGUNIT) begin 
        if ((dbgi.dsuen && dbgi.dbreak) == 1'b1) begin 
          v.x.rstate = dsu1; 
          v.x.debug = 1'b1;
         end 
       end 
      if ((index != 0) && (irqi.run == 1'b0) && (rstn == 1'b0)) begin  
        v.x.rstate = dsu1; 
        vp.pwd = 1'b1; 
       end 
      v.x.npc = 3'b100;
     end 
    
    // kill off unused regs
    if (!FPEN) begin  v.w.s.ef = 1'b0;  end 
    if (!CPEN) begin  v.w.s.ec = 1'b0;  end 

    
// MEMORY STAGE

    v.x.ctrl = r.m.ctrl; v.x.dci = r.m.dci;
    v.x.ctrl.rett = r.m.ctrl.rett &&  !r.m.ctrl.annul;
    v.x.mac = r.m.mac; v.x.laddr = r.m.result[1 : 0];
    v.x.ctrl.annul = r.m.ctrl.annul || v.x.annul_all; 
    st = 1'b0; 
    
    if (CASAEN && (r.m.casa == 1'b1) && (r.m.ctrl.cnt == 2'b00)) begin 
      v.x.ctrl.inst[4 : 0] = r.a.ctrl.inst[4 : 0]; // restore rs2 for trace log
     end 

    mul_res(r, v.w.s.asr18, v.x.result, v.x.y, me_asr18, me_icc);


    mem_trap(r, wpr, v.x.ctrl.annul, holdn, v.x.ctrl.trap, me_iflush,
             me_nullify, v.m.werr, v.x.ctrl.tt);
    me_newtt = v.x.ctrl.tt;

    irq_trap(r, ir, irqi.irl, v.x.ctrl.annul, v.x.ctrl.pv, v.x.ctrl.trap, me_newtt, me_nullify,
             v.m.irqen, v.m.irqen2, me_nullify2, v.x.ctrl.trap,
             v.x.ipend, v.x.ctrl.tt);   

      
    if ((r.m.ctrl.ld || st ||  !dco.mds) == 1'b1) begin           
      for i in 0 to dsets-1 loop
        v.x.data[i] = dco.data[i];
      end loop;
      v.x.set = dco.set[DSETMSB : 0]; 
      if (dco.mds == 1'b0) begin 
        me_size = r.x.dci.size; = r.x.laddr; me_signed  me_laddr =  r.x.dci.signed;
      else
        me_size = v.x.dci.size; = v.x.laddr; me_signed  me_laddr =  v.x.dci.signed;
       end 
      if ((lddel != 2)) begin 
        v.x.data[0] = ld_align(v.x.data, v.x.set, me_size, me_laddr, me_signed);
       end 
     end 
    if ((!RESET_ALL) && (is_fpga[fabtech] == 0) && (xc_rstn == 1'b0)) begin 
      v.x.data = (others =>  `0 ); //v.x.ldc = 1'b0;
     end 
    v.x.mexc = dco.mexc;

    v.x.icc = me_icc;
    v.x.ctrl.wicc = r.m.ctrl.wicc &&  !v.x.annul_all;
    
    if (MACEN && ((v.x.ctrl.annul || v.x.ctrl.trap) == 1'b0)) begin 
      v.w.s.asr18 = me_asr18;
     end 

    if (r.x.rstate == dsu2)
    begin       
      = 1'b0; v.x.set  me_nullify2 =  dco.set[DSETMSB : 0];
     end 


    if ((!RESET_ALL) && (xc_rstn == 1'b0)) begin  
        v.x.ctrl.trap = 1'b0; 
        v.x.ctrl.annul = 1'b1;
     end 
    
    dci.maddress <= r.m.result;
    dci.enaddr   <= r.m.dci.enaddr;
    dci.asi      <= r.m.dci.asi;
    dci.size     <= r.m.dci.size;
    dci.lock     <= (r.m.dci.lock &&  !r.m.ctrl.annul);
    dci.read     <= r.m.dci.read;
    dci.write    <= r.m.dci.write;
    dci.flush    <= me_iflush;
    dci.dsuen    <= r.m.dci.dsuen;
    dci.msu    <= r.m.su;
    dci.esu    <= r.e.su;
    dbgo.ipend <= v.x.ipend;
    
// EXECUTE STAGE

    v.m.ctrl = r.e.ctrl; = r.e.op1; ex_op2  ex_op1 =  r.e.op2;
    v.m.ctrl.rett = r.e.ctrl.rett &&  !r.e.ctrl.annul;
    v.m.ctrl.wreg = r.e.ctrl.wreg &&  !v.x.annul_all;
    ex_ymsb = r.e.ymsb; = ex_op2; ex_shcnt  mul_op2 =  r.e.shcnt;
    v.e.cwp = r.a.cwp; ex_sari = r.e.sari;
    v.m.su = r.e.su;
    if (MULTYPE == 3) begin  v.m.mul = r.e.mul; else v.m.mul = 1'b0;  end 
    if (lddel == 1) begin 
      if (r.e.ldbp1 == 1'b1) begin  
        ex_op1 = r.x.data[0]; 
        ex_sari = r.x.data[0][31] && r.e.ctrl.inst[19] && r.e.ctrl.inst[20];
       end 
      if (r.e.ldbp2 == 1'b1) begin  
        = r.x.data[0]; ex_ymsb  ex_op2 ==  r.x.data[0][0]; 
        = ex_op2; ex_shcnt  mul_op2 =  r.x.data[0][4 : 0];
        if (r.e.invop2 == 1'b1) begin  
          =  !ex_op2; ex_shcnt  ex_op2 =   !ex_shcnt;
         end 
       end 
     end 


    ex_add_res = ({ex_op1,1'b1}) + (ex_op2 & r.e.alucin);

    if (ex_add_res[2 : 1] == 2'b00) begin  v.m.nalign = 1'b0;
    else v.m.nalign = 1'b1;  end 

    dcache_gen(r, v, ex_dci, ex_link_pc, ex_jump, ex_force_a2, ex_load, v.m.casa);
    ex_jump_address = ex_add_res(32 downto PCLOW+1);
    logic_op(r, ex_op1, ex_op2, v.x.y, ex_ymsb, ex_logic_res, v.m.y);
    ex_shift_res = shift(r, ex_op1, ex_op2, ex_shcnt, ex_sari);
    misc_op(r, wpr, ex_op1, ex_op2, xc_df_result, v.x.y, ex_misc_res, ex_edata);
    ex_add_res[3]:= ex_add_res[3] || ex_force_a2;    
    alu_select(r, ex_add_res, ex_op1, ex_op2, ex_shift_res, ex_logic_res,
        ex_misc_res, ex_result, me_icc, v.m.icc, v.m.divz, v.m.casaz);    
    dbg_cache(holdn, dbgi, r, dsur, ex_result, ex_dci, ex_result2, v.m.dci);
    fpstdata(r, ex_edata, ex_result2, fpo.data, ex_edata2, ex_result3);
    v.m.result = ex_result3;
    cwp_ex(r, v.m.wcwp);    

    if (CASAEN && (r.e.ctrl.cnt == 2'b10) && ((r.m.casa &&  !v.m.casaz) == 1'b1)) begin 
      me_nullify2 = 1'b1;
     end 
    dci.nullify  <= me_nullify2;

    ex_mulop1 = (ex_op1[31] && r.e.ctrl.inst[19]) & ex_op1;
    ex_mulop2 = (mul_op2[31] && r.e.ctrl.inst[19]) & mul_op2;

    if (is_fpga[fabtech] == 0 && (r.e.mul == 1'b0)) begin      // power-save for mul
//    if ((r.e.mul == 1'b0)) begin 
        =  `0 ; ex_mulop2  ex_mulop1 ==   `0 ;
     end 

      
    v.m.ctrl.annul = v.m.ctrl.annul || v.x.annul_all;
    v.m.ctrl.wicc = r.e.ctrl.wicc &&  !v.x.annul_all; 
    v.m.mac = r.e.mac;
    if ((DBGUNIT && (r.x.rstate == dsu2))) begin  v.m.ctrl.ld = 1'b1;  end 
    dci.eenaddr  <= v.m.dci.enaddr;
    dci.eaddress <= ex_add_res[32 : 1];
    dci.edata <= ex_edata2;
    bp_miss_ex(r, r.m.icc, ex_bpmiss, ra_bpannul);
    
// REGFILE STAGE

    v.e.ctrl = r.a.ctrl; v.e.jmpl = r.a.jmpl &&  !r.a.ctrl.trap;
    v.e.ctrl.annul = r.a.ctrl.annul || ra_bpannul || v.x.annul_all;
    v.e.ctrl.rett = r.a.ctrl.rett &&  !r.a.ctrl.annul &&  !r.a.ctrl.trap;
    v.e.ctrl.wreg = r.a.ctrl.wreg &&  !(ra_bpannul || v.x.annul_all);    
    v.e.su = r.a.su; v.e.et = r.a.et;
    v.e.ctrl.wicc = r.a.ctrl.wicc &&  !(ra_bpannul || v.x.annul_all);
    v.e.rfe1 = r.a.rfe1; v.e.rfe2 = r.a.rfe2;
    
    exception_detect(r, wpr, dbgi, r.a.ctrl.trap, r.a.ctrl.tt, 
                     v.e.ctrl.trap, v.e.ctrl.tt);
    op_mux(r, rfo.data1, ex_result3, v.x.result, xc_df_result, zero32, 
        r.a.rsel1, v.e.ldbp1, ra_op1, 1'b0);
    op_mux(r, rfo.data2,  ex_result3, v.x.result, xc_df_result, r.a.imm, 
        r.a.rsel2, ex_ldbp2, ra_op2, 1'b1);
    alu_op(r, ra_op1, ra_op2, v.m.icc, v.m.y[0], ex_ldbp2, v.e.op1, v.e.op2,
           v.e.aluop, v.e.alusel, v.e.aluadd, v.e.shcnt, v.e.sari, v.e.shleft,
           v.e.ymsb, v.e.mul, ra_div, v.e.mulstep, v.e.mac, v.e.ldbp2, v.e.invop2
    );
    cin_gen(r, v.m.icc[0], v.e.alucin);
    bp_miss_ra(r, ra_bpmiss, de_bpannul);
    v.e.bp = r.a.bp &&  !ra_bpmiss;
    
// DECODE STAGE

    if (ISETS > 1) begin 
      de_inst = r.d.inst[int`(r.d.set)];
    end else begin
      de_inst = r.d.inst[0];  
    end 

    = r.m.icc; v.a.cwp  de_icc =  r.d.cwp;
    su_et_select(r, v.w.s.ps, v.w.s.s, v.w.s.et, v.a.su, v.a.et);
    wicc_y_gen(de_inst, v.a.ctrl.wicc, v.a.ctrl.wy);
    cwp_ctrl(r, v.w.s.wim, de_inst, de_cwp, v.a.wovf, v.a.wunf, de_wcwp);
    if (CASAEN && (de_inst[31 : 30] == LDST) && (de_inst[24 : 19] == CASA)) begin 
      case (r.d.cnt) 
      2'b00 | 2'b01  : de_inst[4 : 0] = 2'b00000; // rs2=0
      default : 
      endcase 
     end 
    rs1_gen(r, de_inst, v.a.rs1, de_rs1mod); 
    de_rs2 = de_inst[4 : 0];
    =  `0 ; de_raddr2  de_raddr1 ==   `0 ;
    
    if (RS1OPT) begin 
      if (de_rs1mod == 1'b1) begin 
        regaddr(r.d.cwp, de_inst[29 : 26] & v.a.rs1[0], de_raddr1[RFBITS-1 : 0]);
      else
        regaddr(r.d.cwp, de_inst[18 : 15] & v.a.rs1[0], de_raddr1[RFBITS-1 : 0]);
       end 
    else
      regaddr(r.d.cwp, v.a.rs1, de_raddr1[RFBITS-1 : 0]);
     end 
    regaddr(r.d.cwp, de_rs2, de_raddr2[RFBITS-1 : 0]);
    v.a.rfa1 = de_raddr1[RFBITS-1 : 0]; 
    v.a.rfa2 = de_raddr2[RFBITS-1 : 0]; 

    rd_gen(r, de_inst, v.a.ctrl.wreg, v.a.ctrl.ld, de_rd);
    regaddr(de_cwp, de_rd, v.a.ctrl.rd);
    
    fpbranch(de_inst, fpo.cc, de_fbranch);
    fpbranch(de_inst, cpo.cc, de_cbranch);
    v.a.imm = imm_data(r, de_inst);
      de_iperr = 1'b0;
    lock_gen(r, de_rs2, de_rd, v.a.rfa1, v.a.rfa2, v.a.ctrl.rd, de_inst, 
        fpo.ldlock, v.e.mul, ra_div, de_wcwp, v.a.ldcheck1, v.a.ldcheck2, de_ldlock, 
        v.a.ldchkra, v.a.ldchkex, v.a.bp, v.a.nobp, de_fins_hold, de_iperr);
    ic_ctrl(r, de_inst, v.x.annul_all, de_ldlock, branch_true(de_icc, de_inst), 
        de_fbranch, de_cbranch, fpo.ccv, cpo.ccv, v.d.cnt, v.d.pc, de_branch,
        v.a.ctrl.annul, v.d.annul, v.a.jmpl, de_inull, v.d.pv, v.a.ctrl.pv,
        de_hold_pc, v.a.ticc, v.a.ctrl.rett, v.a.mulstart, v.a.divstart, 
        ra_bpmiss, ex_bpmiss, de_iperr);

    v.a.bp = v.a.bp &&  !v.a.ctrl.annul;
    v.a.nobp = v.a.nobp &&  !v.a.ctrl.annul;

    v.a.ctrl.inst = de_inst;

    cwp_gen(r, v, v.a.ctrl.annul, de_wcwp, de_cwp, v.d.cwp);    
    
    v.d.inull = ra_inull_gen(r, v);
    
    op_find(r, v.a.ldchkra, v.a.ldchkex, v.a.rs1, v.a.rfa1, 
            false, v.a.rfe1, v.a.rsel1, v.a.ldcheck1);
    op_find(r, v.a.ldchkra, v.a.ldchkex, de_rs2, v.a.rfa2, 
            imm_select[de_inst], v.a.rfe2, v.a.rsel2, v.a.ldcheck2);


    v.a.ctrl.wicc = v.a.ctrl.wicc &&  (!v.a.ctrl.annul) 
    ;
    v.a.ctrl.wreg = v.a.ctrl.wreg &&  (!v.a.ctrl.annul) 
    ;
    v.a.ctrl.rett = v.a.ctrl.rett &&  (!v.a.ctrl.annul) 
    ;
    v.a.ctrl.wy = v.a.ctrl.wy &&  (!v.a.ctrl.annul) 
    ;

    v.a.ctrl.trap = r.d.mexc 
    ;
    v.a.ctrl.tt = 6'b000000;
      if (r.d.mexc == 1'b1) begin 
        v.a.ctrl.tt = 6'b000001;
       end 
    v.a.ctrl.pc = r.d.pc;
    v.a.ctrl.cnt = r.d.cnt;
    v.a.step = r.d.step;
    
    if (holdn == 1'b0) begin  
      de_raddr1[RFBITS-1 : 0] = r.a.rfa1;
      de_raddr2[RFBITS-1 : 0] = r.a.rfa2;
      = r.a.rfe1; de_ren2  de_ren1 =  r.a.rfe2;
    else
      = v.a.rfe1; de_ren2  de_ren1 =  v.a.rfe2;
     end 

    if (DBGUNIT) begin 
      if ((dbgi.denable == 1'b1) && (r.x.rstate == dsu2)) begin         
        de_raddr1[RFBITS-1 : 0] = dbgi.daddr(RFBITS+1 downto 2); de_ren1 = 1'b1;
        = de_raddr1; de_ren2  de_raddr2 =  1'b1;
       end 
      v.d.step = dbgi.step &&  !r.d.annul;      
     end 

    rfi.wren <= (xc_wreg && holdn) &&  !dco.scanen;
    rfi.raddr1 <= de_raddr1; rfi.raddr2 <= de_raddr2;
    rfi.ren1 <= de_ren1 &&  !dco.scanen;
    rfi.ren2 <= de_ren2 &&  !dco.scanen;
    ici.inull <= de_inull
    ;
    ici.flush <= me_iflush;
    v.d.divrdy = divo.nready;
    ici.fline <= r.x.ctrl.pc[31 : 3];
    dbgo.bpmiss <= bpmiss && holdn;
    if (xc_rstn == 1'b0) begin 
      v.d.cnt =  `0 ;
      if (need_extra_sync_reset[fabtech] != 0) begin  
        v.d.cwp =  `0 ;
       end 
     end 

// FETCH STAGE
  always_comb begin
    bpmiss = ex_bpmiss || ra_bpmiss;
    npc    = r.f.pc; 
    fe_pc  =  r.f.pc;
    if (ra_bpmiss == 1'b1) begin  
      fe_pc = r.d.pc;  end 
    if (ex_bpmiss == 1'b1) begin  
      fe_pc = r.a.ctrl.pc;  end 
    fe_npc = zero32[31 : PCLOW];
    fe_npc[31 : 2] = fe_pc[31 : 2] + 1;    // Address incrementer

    if (xc_rstn == 1'b0) begin 
      if ((!RESET_ALL)) begin  
        v.f.pc =  `0 ; 
        v.f.branch = 1'b0;
        if (DYNRST) begin  
          v.f.pc[31 : 12] = irqi.rstvec;
        end else begin
          v.f.pc[31 : 12] =  20'(rstaddr);
        end 
      end 
    end else if (xc_exception == 1'b1) begin        // exception
      v.f.branch = 1'b1; 
      v.f.pc = xc_trap_address;
      npc = v.f.pc;
    end else if (de_hold_pc == 1'b1) begin 
      v.f.pc = r.f.pc; 
      v.f.branch = r.f.branch;
      if (bpmiss == 1'b1) begin 
        v.f.pc = fe_npc; 
        v.f.branch = 1'b1;
        npc = v.f.pc;
      end else if (ex_jump == 1'b1) begin 
        v.f.pc = ex_jump_address; 
        v.f.branch = 1'b1;
        npc = v.f.pc;
      end 
    end else if ((ex_jump &&  !bpmiss) == 1'b1) begin 
      v.f.pc = ex_jump_address; 
      v.f.branch = 1'b1;
      npc = v.f.pc;
    end else if ((de_branch &&  !bpmiss) = 1'b1) begin 
      v.f.pc = branch_address(de_inst, r.d.pc); 
      v.f.branch = 1'b1;
      npc = v.f.pc;
    end else begin
      v.f.branch = bpmiss; 
      v.f.pc = fe_npc; 
      npc = v.f.pc;
    end 
    ici.dpc = {r.d.pc[31 : 2] , 2'b00};
    ici.fpc = {r.f.pc[31 : 2] , 2'b00};
    ici.rpc = {npc[31 : 2]    , 2'b00};
    ici.fbranch = r.f.branch;
    ici.rbranch = v.f.branch;
    ici.su = v.a.su;
 
    if ((ico.mds && de_hold_pc) == 1'b0) begin 
      for (int i=0;  i<= isets-1; i++) begin: loop
        v.d.inst[i] = ico.data[i];                     // latch instruction
      end //loop;
      v.d.set = ico.set[ISETMSB : 0];             // latch instruction
      v.d.mexc = ico.mexc;                             // latch instruction
     end 


    if (DBGUNIT) begin  // DSU diagnostic read    
      diagread(dbgi, r, dsur, ir, wpr, dco, tbo, diagdata);
      diagrdy(dbgi.denable, dsur, r.m.dci, dco.mds, ico, vdsu.crdy);
     end 
    
// OUTPUTS

  always_comb begin
    rin    = v; 
    wprin  = vwpr; 
    dsuin  = vdsu; 
    irin   = vir;
    muli.start = r.a.mulstart &&  !r.a.ctrl.annul && 
         !r.a.ctrl.trap &&  !ra_bpannul;
    muli.signed = r.e.ctrl.inst[19];
    muli.op1 = ex_mulop1; //(ex_op1[31] && r.e.ctrl.inst[19]) & ex_op1;
    muli.op2 = ex_mulop2; //(mul_op2[31] && r.e.ctrl.inst[19]) & mul_op2;
    muli.mac = r.e.ctrl.inst[24];
    if (MACPIPE) begin  
      muli.acc[39 : 32] = r.w.s.y[7 : 0];
    end else begin
      muli.acc[39 : 32] = r.x.y[7 : 0];  
    end 
      muli.acc[31 : 0] = r.w.s.asr18;
    muli.flush = r.x.annul_all;
    divi.start = r.a.divstart &&  !r.a.ctrl.annul && 
         !r.a.ctrl.trap &&  !ra_bpannul;
    divi.signed = r.e.ctrl.inst[19];
    divi.flush  = r.x.annul_all;
    divi.op1    = (ex_op1[31] && r.e.ctrl.inst[19]) & ex_op1;
    divi.op2    = (ex_op2[31] && r.e.ctrl.inst[19]) & ex_op2;
    if ((r.a.divstart &&  !r.a.ctrl.annul) == 1'b1) begin  
      dsign =  r.a.ctrl.inst[19];
    end else begin
      dsign = r.e.ctrl.inst[19];  
    end 
    divi.y = (r.m.y[31] && dsign) & r.m.y;
    rpin = vp;

    if (DBGUNIT) begin 
      dbgo.dsu <= 1'b1; 
      dbgo.dsumode <= r.x.debug; 
      dbgo.crdy <= dsur.crdy[2];
      dbgo.data <= diagdata;
      if (TRACEBUF) begin  
        tbi <= tbufi; 
      end else begin
        tbi.addr <=  `0 ; 
        tbi.data <=  `0 ;
        tbi.enable <= 1'b0; 
        tbi.write <=  `0 ; 
        tbi.diag <= 4'b0000;
      end 
    end else begin
      dbgo.dsu <= 1'b0; 
      dbgo.data <=  `0 ; 
      dbgo.crdy  <= 1'b0;
      dbgo.dsumode <= 1'b0; 
      tbi.addr <=  `0 ; 
      tbi.data <=  `0 ; 
      tbi.enable <= 1'b0;
      tbi.write <=  `0 ; 
      tbi.diag <= 4'b0000;
    end 
    dbgo.error <= dummy &&  !r.x.nerror;
    dbgo.wbhold <= 1'b0; //dco.wbhold;
    dbgo.su <= r.w.s.s;
    dbgo.istat <= (1'b0, 1'b0, 1'b0, 1'b0);
    dbgo.dstat <= (1'b0, 1'b0, 1'b0, 1'b0);

    if (FPEN) begin 
      if ((r.x.rstate == dsu2)) begin  
        vfpi.flush = 1'b1; 
       end else  begin 
        vfpi.flush = v.x.annul_all && holdn;  
       end 
      vfpi.exack = xc_fpexack; 
      vfpi.a_rs1 = r.a.rs1; 
      vfpi.d.inst = de_inst;
      vfpi.d.cnt = r.d.cnt;
      vfpi.d.annul = v.x.annul_all || de_bpannul || r.d.annul || de_fins_hold;
      vfpi.d.trap = r.d.mexc;
      vfpi.d.pc[1 : 0] =  `0 ; 
      vfpi.d.pc[31 : PCLOW] = r.d.pc[31 : PCLOW]; 
      vfpi.d.pv = r.d.pv;
      vfpi.a.pc[1 : 0] =  `0 ; 
      vfpi.a.pc[31 : PCLOW] = r.a.ctrl.pc[31 : PCLOW]; 
      vfpi.a.inst = r.a.ctrl.inst; 
      vfpi.a.cnt = r.a.ctrl.cnt; 
      vfpi.a.trap = r.a.ctrl.trap;
      vfpi.a.annul = r.a.ctrl.annul || (ex_bpmiss && r.e.ctrl.inst[29]) ;
      vfpi.a.pv = r.a.ctrl.pv;
      vfpi.e.pc[1 : 0] =  `0 ; 
      vfpi.e.pc[31 : PCLOW] = r.e.ctrl.pc[31 : PCLOW]; 
      vfpi.e.inst = r.e.ctrl.inst; 
      vfpi.e.cnt = r.e.ctrl.cnt; 
      vfpi.e.trap = r.e.ctrl.trap; 
      vfpi.e.annul = r.e.ctrl.annul;
      vfpi.e.pv = r.e.ctrl.pv;
      vfpi.m.pc[1 : 0] =  `0 ; 
      vfpi.m.pc[31 : PCLOW] = r.m.ctrl.pc[31 : PCLOW]; 
      vfpi.m.inst = r.m.ctrl.inst; 
      vfpi.m.cnt = r.m.ctrl.cnt; 
      vfpi.m.trap = r.m.ctrl.trap; 
      vfpi.m.annul = r.m.ctrl.annul;
      vfpi.m.pv = r.m.ctrl.pv;
      vfpi.x.pc[1 : 0] =  `0 ; 
      vfpi.x.pc[31 : PCLOW] = r.x.ctrl.pc[31 : PCLOW]; 
      vfpi.x.inst = r.x.ctrl.inst; 
      vfpi.x.cnt = r.x.ctrl.cnt; 
      vfpi.x.trap = xc_trap;
      vfpi.x.annul = r.x.ctrl.annul; 
      vfpi.x.pv = r.x.ctrl.pv;
      if (lddel == 2) begin  
        vfpi.lddata = r.x.data(conv_integer(r.x.set)); 
      end else begin
        vfpi.lddata = r.x.data[0];  
      end 
      if (r.x.rstate == dsu2)  begin  
        vfpi.dbg.enable = dbgi.denable;
      end else begin 
         vfpi.dbg.enable = 1'b0;  
      end       
      vfpi.dbg.write = fpcdbgwr;
      vfpi.dbg.fsr = dbgi.daddr[22]; // IU reg access
      vfpi.dbg.addr = dbgi.daddr[6 : 2];
      vfpi.dbg.data = dbgi.ddata;      
      fpi <= vfpi;
      cpi <= vfpi;      // dummy, just to kill some warnings ...
     end 
  end

  always_ff @(posedge clk ) begin : preg 
    rp <= rpin;
    if (rstn == 1'b0) begin 
      rp.error <= PRES.error;
      if (RESET_ALL) begin 
        if ((index != 0) && (irqi.run == 1'b0)) begin 
          rp.pwd <= 1'b1;
        end else begin
          rp.pwd <= 1'b0;
         end 
       end 
     end 
    end 


  always_ff @(posedge clk ) begin : reg 
    if (holdn == 1'b1) begin 
      r <= rin;
    end else begin
      r.x.ipend <= rin.x.ipend;
      r.m.werr <= rin.m.werr;
      if ((holdn || ico.mds) == 1'b0) begin 
        r.d.inst <= rin.d.inst; 
        r.d.mexc <= rin.d.mexc; 
        r.d.set <= rin.d.set;
       end 
      if ((holdn || dco.mds) == 1'b0) begin 
        r.x.data <= rin.x.data; 
        r.x.mexc <= rin.x.mexc; 
        r.x.set <= rin.x.set;
       end 
    end 
    if (rstn == 1'b0) begin 
      if (RESET_ALL) begin 
        r <= RRES;
        if (DYNRST) begin 
          r.f.pc[31 : 12] <= irqi.rstvec;
          r.w.s.tba <= irqi.rstvec;
        end 
        if (DBGUNIT) begin 
          if ((dbgi.dsuen && dbgi.dbreak) == 1'b1) begin 
            r.x.rstate <= dsu1; r.x.debug <= 1'b1;
          end 
        end 
        if ((index != 0) && irqi.run == 1'b0) begin 
          r.x.rstate <= dsu1;
        end 
      end else begin 
        r.w.s.s <= 1'b1; 
        r.w.s.ps <= 1'b1; 
        if (need_extra_sync_reset[fabtech] != 0) begin  
          r.d.inst <= (others =>  `0 );
          r.x.mexc <= 1'b0;
        end  
      end 
    end  
  end 


generate
 if (DBGUNIT ) :  dsugen  
 begin
    always_ff @(posedge clk ) begin : dsureg 
      if (holdn == 1'b1) begin 
        dsur <= dsuin;
      end else begin
        dsur.crdy <= dsuin.crdy;
      end 
      if (rstn == 1'b0) begin 
        if (RESET_ALL) begin 
          dsur <= DRES;
        end else if (need_extra_sync_reset[fabtech] != 0) begin 
          dsur.err <= 1'b0; dsur.tbufcnt <=  `0 ; dsur.tt <=  `0 ;
          dsur.asi <=  `0 ; dsur.crdy <=  `0 ;
        end 
      end 
    end 
  end
endgenerate

generate
 if ( !DBGUNIT  ) :  nodsugen  
 begin
    assign dsur.err = 1'b0; 
    assign dsur.tbufcnt =  `0 ; 
    assign dsur.tt   =  `0 ;
    assign dsur.asi  =  `0 ; 
    assign dsur.crdy =  `0 ;
  end
 endgenerate

  generate
    if (DBGUNIT || PWRD2) : irreg
      always_ff @(posedge clk ) begin :dsureg 
        if (holdn == 1'b1) begin  ir <= irin;  end 
        if (RESET_ALL && rstn == 1'b0) begin  ir <= IRES;  end 
      end 
    end
  endgenerate

 generate
    if  !(DBGUNIT || PWRD2 ) begin:   nirreg : 
      ir.pwd <= 1'b0;
      ir.addr <=  `0 ;
  end
endgenerate
  
generate
 for (  i=0;i<=  3;i++  )
 begin :  wpgen  
//generate
 if (nwp > i  )
 begin :    wpg0  
    always_ff @(posedge clk ) begin : wpreg
          if (holdn == 1'b1) begin  wpr[i] <= wprin[i];  end 
          if (rstn == 1'b0) begin 
            if (RESET_ALL) begin 
              wpr[i] <= wpr_none;
            end else begin
              wpr[i].exec <= 1'b0; wpr[i].load <= 1'b0; wpr[i].store <= 1'b0;
            end 
          end 
    end 
  end
//   endgenerate
// generate
  if (nwp <= i  )
  begin :    wpg1  
      wpr[i] <= wpr_none;
  end
//  endgenerate
  end
endgenerate

// pragma translate_off
     bit   valid ;
     logic  [1 : 0] op ;
     logic  [5 : 0] op3 ;
     bit   fpins, fpld ;  
always @(posedge clk) :   trc 
  begin
    if ((fpu != 0)) begin 
      op3 = r.x.ctrl.inst[31 : 30];  
      op =  r.x.ctrl.inst[24 : 19];
      fpins = (op == FMT3) && ((op3 == FPOP1) || (op3 == FPOP2));
      fpld = (op == LDST) && ((op3 == LDF) || (op3 == LDDF) || (op3 == LDFSR));
    end else begin
      fpld= false;
      fpins =  false;
    end 
      valid = (( (!r.x.ctrl.annul) && r.x.ctrl.pv) == 1'b1) &&  (!((fpins || fpld) && (r.x.ctrl.trap == 1'b0)));
      valid = valid && (holdn == 1'b1);
    if ((disas == 1) && (rstn == 1'b1)) begin 
      print_insn (index, r.x.ctrl.pc[31 : 2] & 2'b00, r.x.ctrl.inst, 
                  rin.w.result, valid, r.x.ctrl.trap == 1'b1, rin.w.wreg = 1'b1,
        rin.x.ipmask == 1'b1);
     end 
  end
// pragma translate_on

generate
  if (disas < 2  )
  begin  :  dis0  
    dummy <= 1'b1; 
  end
 endgenerate

generate
  if (disas > 1  )  
  begin :  dis2 
    disasen <= disas != 0  ? 1'b1 : 1'b0;
      cpu_index <=  4'(index);
      cpu_disasx x0
      ( .clk    (clk                )
        .rstn   (rstn               )
        .dummy  (dummy              )
        .inst   (r.x.ctrl.inst      )
        .pc     (r.x.ctrl.pc[31 : 2])
        .result (rin.w.result       )
        .index  (cpu_index          )
        .wreg   (rin.w.wreg         )
        .annul  (r.x.ctrl.annul     )
        .holdn  (holdn              )
        .pv     (r.x.ctrl.pv        )
        .trap   (r.x.ctrl.trap      )
        .disas  (disasen);          )
  end
 endgenerate

endmodule
