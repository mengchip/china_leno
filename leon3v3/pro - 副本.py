import os;
import re;

os.chdir ("F:\\work\\leno\\meng_leno\\hardware\\leon3v3")

new_dir = "new"
fold = open('iu3.vhd', 'r+')
fnew = open('./'+new_dir+'/iu3.sv', 'w+')


#for line in f

#line_text =f.readline()

head_line = "///////////////////////////////////////////////////////////////////////////////\r \
// 版权 （C） 2015 -2015 ，北京蒙芯科技有限公司\r \
//本程序是自由软件；您可以重新分配和/或修改在GNU通用公共许可证的条款公布 \r \
//自由软件基金会；无论是GPL v2版的许可证，或（在你选择的任何版本）。\r \
// \r \
//这个程序是分布在希望它是有用的，但没有任何担保；甚至没有暗示保证 \r \
//适销性或针对特定用途的。看到GNU通用公共许可证的更多细节。\r \
// \r \
//你应该已经收到一份GNU通用公共许可证,随着这个程序；如果没有，写信给自由软件基金会 \r \
// 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA \r  \
/////////////////////////////////////////////////////////////////////////////// \r  \
\r \
\r \
/////////////////////////////////////////////////////////////////////////////// \r \
// 编写人： 薛晓军  \r \
// 描述 ： LEON3 7-stage integer pipline 流水线 \r \
///////////////////////////////////////////////////////////////////////////////  \r "


fnew.write(head_line)



head1=re.compile(r"--  This file is a part of the GRLIB VHDL IP LIBRARY\n");
head2=re.compile(r"--  Copyright \(C\) 2003 - 2008, Gaisler Research\n");
head3=re.compile(r"--  Copyright \(C\) 2008 - 2014, Aeroflex Gaisler\n");
head4=re.compile(r"--  This program is free software; you can redistribute it and/or modify\n");
head5=re.compile(r"--  it under the terms of the GNU General Public License as published by\n");
head6=re.compile(r"--  the Free Software Foundation; either version 2 of the License, or\n");
head7=re.compile(r"--  \(at your option\) any later version.\n");
head8=re.compile(r"--  This program is distributed in the hope that it will be useful,\n");
head9=re.compile(r"--  but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
head10=re.compile(r"--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
head11=re.compile(r"--  GNU General Public License for more details.\n");
head12=re.compile(r"--  You should have received a copy of the GNU General Public License\n");
head13=re.compile(r"--  along with this program; if not, write to the Free Software\n");
head14=re.compile(r"--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA \n");
head15=re.compile(r"------------------------------------------------------[-]+\n");
head16=re.compile(r"-- Entity:[\w\d\s.]+\n");
head17=re.compile(r"-- File:[\w\d\s.,]+\n");
head18=re.compile(r"-- Author:[\w\d\s.,]+\n");
head19=re.compile("-- Description\: [\w\d\s.,-]+\n");
head20=re.compile(r"[-]+\n");

define_logicvec=re.compile(r'(\w+)\s:\std_logic_vector\((\w+|\d+)\sdownto\s(\w+|\d+)\);');
define_struct=re.compile(r'(\w+)\s:\s(\w+);');
define_logic=re.compile(r'(\w+)\s:\sstd_ulogic;');
define_type=re.compile(r'\s*type (\w+) is record');
define_typend=re.compile(r'\s+end record;');
line_re=re.compile(r'\((\w+|\d+)\sdownto\s(\w+|\d+)\)'); #DSETMSB downto 0
annotation=re.compile(r'--');
module_name=re.compile(r'entity (\w+) is');
par_start=re.compile(r'\s+generic \(');
par_def=re.compile(r'\s*([\w, ]+)\s+:\s+integer([\s\w\d]+):=\s(\d+);');
par_def2=re.compile(r'\s*([\w, ]+)\s+:\s+integer([\s\w\d]+):=\s(\d+)#(\d+)#;');
par_def3=re.compile(r'\s*([\w, ]+)\s+:\s+integer([\s\w\d]+):=\s(\d+)');
par_def4=re.compile(r'\s*([\w, ]+)\s+:\s+integer([\s\w\d]+):=\s(\d+)#(\d+)#');
define_end=re.compile(r'^\s+);');

port_start=re.compile(r'\s+port \(');
inport=re.compile(r'\s+(\w+)\s+:\s+in\s+(\w+);*');
outport=re.compile(r'\s+(\w+)\s+:\s+out\s+(\w+);*');

#constant_line=re.compile(r'\s+constant\s(\w+)\s:\s([\w\s]+):=\s([\w\s\(\-)])+;');
constant=re.compile(r'(.*)constant(.*)');
downto=re.compile(r'(.*)\(([\w-]+) downto ([\w-]+)\)(.*)');
std_logic_vector=re.compile(r'(.*)std_logic_vector(.*)');
boolean=re.compile(r'(.*)boolean(.*)');
std_ulogic=re.compile(r'(.*)std_ulogic(.*)');
range_annota=re.compile(r'(.*)?range(.*)?:=(.*)');
define_realign=re.compile(r'(.*)\s(\w+)\s+?:(.*)?:=(.*)');

for line in fold :
     line_new=line;
     if downto.search(line_new) :
        line_new=downto.sub(r"\1[\2 : \3]\4",line_new);
        # (NWINLOG2-1 downto 0)->
     if constant.search(line_new) :
        line_new=constant.sub(r"\1 localparam  \2",line_new);
     if std_logic_vector.search(line_new) :
        line_new=std_logic_vector.sub(r"\1 logic  \2",line_new);
     if boolean.search(line_new) :
        line_new=boolean.sub(r"\1 bit  \2",line_new);
     if std_ulogic.search(line_new) :
        line_new=std_ulogic.sub(r"\1 logic  \2",line_new);
     if define_realign.search(line_new) :
        line_new=define_realign.sub(r"\1 \3 \2 = \4",line_new);
     if range_annota.search(line_new) :
        line_new=range_annota.sub(r"\1 := \3  range \2",line_new);        
     for head_num in range(1,20) :
         head_name='head'+str(head_num);
         line_new=eval(head_name).sub(r'',line_new);
     if module_name.match(line_new) :
        line_new=module_name.sub(r"module \1",line_new);
        fnew.write(line_new)
        continue
     if par_start.match(line_new) :
        line_new=par_start.sub(r"#(",line_new);
        fnew.write(line_new)
		par_tag=1;
        continue
 #    if par_tag :
	if par_def.match(line_new) :
		line_new=par_def.sub(r"parameter int \1    = \3, // \2",line_new);
		fnew.write(line_new)
		continue
	if par_def2.match(line_new) :
		line_new=par_def2.sub(r"parameter int \1    = \3'b\4 , // \2",line_new);
		fnew.write(line_new)
		continue
	if par_def3.match(line_new) :
		line_new=par_def3.sub(r"parameter int \1    = \3 // \2",line_new);
		fnew.write(line_new)
		continue
	if par_def4.match(line_new) :
		line_new=par_def4.sub(r"parameter int \1    = \3'b\4  // \2",line_new);
		fnew.write(line_new);
		continue
#	 if define_end.match(line_new) and par_tag :
#		par_tag=0
#	 if define_end.match(line_new) and port_tag :
#		port_tag=0;
     if port_start.match(line_new) :
        line_new=port_start.sub(r" (",line_new)
        fnew.write(line_new)
#		port_tag=1;
        continue
  #   if port_tag :
	 if define_logicvec.match(line_new) :
	 	line_new=define_logicvec.sub(r"logic [\2:\3] \1;",line_new)
	 	fnew.write(line_new)
	 	continue
	 if define_struct.match(line_new) :
	 	line_new=define_struct.sub(r"\2 : \1 ;",line_new)
	 	fnew.write(line_new)
	 	continue
	 if define_type.match(line_new) :
	 	struct_name=define_type.match(line_new).group(1)
	 	line_new=define_type.sub(r"typedef struct {",line_new)
	 	fnew.write(line_new)
	 	continue
	 if define_typend.match(line_new) :
	 	line_new=define_typend.sub(r" } "+struct_name+";",line_new)
	 	fnew.write(line_new)
	 	continue
     if line_re.match(line_new) :
        line_new=line_re.sub(r"[\1:\2]",line_new)
        fnew.write(line_new)
        continue
     if annotation.match(line_new) :
        line_new=annotation.sub(r"//",line_new)
        fnew.write(line_new)
        continue
     if inport.match(line_new) :
        porttype_name=inport.match(line_new).group(2)
        if (porttype_name == 'std_ulogic') :
            if re.search(";",line_new) :
              line_new=inport.sub(r"input   logic  \1 ,",line_new)
            else :
              line_new=inport.sub(r"input   logic  \1 ",line_new)
        else :
            if re.search(";",line_new) :
                line_new=inport.sub(r"input   \2  \1 ,",line_new)
            else :
                line_new=inport.sub(r"input   \2  \1 ",line_new)
        fnew.write(line_new)
        continue
     if outport.match(line_new) :
        porttype_name=outport.match(line_new).group(2)
        if (porttype_name == 'std_ulogic') :
            if re.search(";",line_new) :
                line_new=outport.sub(r"output  logic  \1 ,",line_new)
            else :
                line_new=outport.sub(r"output  logic  \1 ",line_new)
        else :
            if re.search(";",line_new) :
                line_new=outport.sub(r"output  \2  \1 ,",line_new)
            else :
                line_new=outport.sub(r"output  \2  \1 ",line_new)
        fnew.write(line_new)
        continue 
     fnew.write(line_new)     
fold.close();
fnew.close();
