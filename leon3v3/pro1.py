import os;
import re;

os.chdir ("F:\\work\\leno\\meng_leno\\hardware\\leon3v3")

new_dir = "new"
fold = open('./'+new_dir+'/iu3.sv', 'r+')
fnew = open('./'+new_dir+'/iu3_1.sv', 'w+')

define_annota=re.compile(r'--');
noequal_tag=re.compile(r' \/= ');
equal_tag=re.compile(r'\((.*?) = (.*?)\)');
bit_val=re.compile(r'"(\d+?)"');
bit_quotation=re.compile(r'\'(\d)\'');
and_combination=re.compile(r'([\w\']*?) & ([\w\']*?)\)');
and_modify=re.compile(r' and ');
or_modify=re.compile(r' or([ \n])');
not_modify=re.compile(r'([\( ])not ');
num_brackets_modfiy=re.compile(r'\((\d+)\)');
when_modfiy=re.compile(r'when\s+(.*?)=>');
maodenghao=re.compile(r' := ');
case_modify=re.compile(r'case\s+(.*?) is');
endcase_modify=re.compile(r' end case;');
other_default0=re.compile(r'\(others => 1\'b0\)');
other_default1=re.compile(r'\(others => 1\'b1\)');
others_modify=re.compile(r'others\s+:');
elsif_modify=re.compile(r' elsif ');
then_modify=re.compile(r' then');
end_if=re.compile(r'end if;');
if_bracket_del=re.compile(r'if\s+\(([\w\s\d>=<\']*?)\) begin');
if_brackets=re.compile(r'if\s+(.*?) begin');
if_brackets_end=re.compile(r'if\s+(?!.*?begin)$');
if_equal_modify=re.compile(r'(if.*?) = (.*begin)');

function_start=re.compile(r'function ');
function_tag=0;
function_in_var=re.compile(r'(\w+\,*\s*\w*)\s+:\s+in(.*?)([;\)])');
#dsur : in dsu_registers;
function_out_var=re.compile(r'(\w+\,*\s*\w*)\s+:\s+out(.*?)([;\)])');
#vwpr : out watchpoint_registers;
#pc, npc  : out pctype;
end_tag=re.compile(r'\s+end;\n');

var_modfiy=re.compile(r'variable (.*?): (.*);');
procedure_modi=re.compile(r' procedure ');
sel_vec_modi=re.compile(r'(\w+?)\((\w+?)\)');#is_fpga(FABTECH)

fun_var_def=re.compile(r'(\w*?)\s+:\s+(\w*\s*?)([;\)])');
#function dbgexc(registers r; l3_debug_in_type dbgi; std_ulogic trap; logic tt  [0 7]) return  logic   is'
return_del=re.compile(r'return .*$');
fun_logic_define=re.compile(r'(\w+)\s*:\s*(\w+\s+\[\w+\s+:\s+\w+\])')
#tt :  logic  [7 : 0]
del_is=re.compile(r'is\s*$');

end_generate_modi=re.compile(r'end generate;');
start_generate_if_modi=re.compile(r'^(.*?): if (.*?)generate');
start_generate_for_modi=re.compile(r'^(.*?): for (.*?)generate');
#dis2 : if disas > 1 generate

for line in fold :
     line_new=line;
     if procedure_modi.search(line_new) :
        line_new=procedure_modi.sub(r" function ",line_new);  
     if define_annota.search(line_new) :
        line_new=define_annota.sub(r"//",line_new);
     line_new=noequal_tag.sub(r" != ",line_new);
     line_new=equal_tag.sub(r"(\1 == \2)",line_new);
     if bit_val.search(line_new) :
        bit_val_len=len(bit_val.search(line_new).group(0))-2;
        bit_val_len_s='%d' %bit_val_len
        line_new=bit_val.sub(r""+bit_val_len_s+"'b\\1",line_new);
     if bit_quotation.search(line_new) :
        line_new=bit_quotation.sub(r"1'b\1",line_new);
     if and_combination.search(line_new) :
        line_new=and_combination.sub(r"{\1,\2})",line_new);
     if and_modify.search(line_new) :
        line_new=and_modify.sub(r" && ",line_new);
     if or_modify.search(line_new) :
        line_new=or_modify.sub(r" ||\1",line_new);
     if not_modify.search(line_new) :
        line_new=not_modify.sub(r" \1!",line_new);        
     if num_brackets_modfiy.search(line_new) :
        line_new=num_brackets_modfiy.sub(r"[\1]",line_new);
     if when_modfiy.search(line_new) :
        line_new=when_modfiy.sub(r"\1 :",line_new);   
     if maodenghao.search(line_new) :
        line_new=maodenghao.sub(r" = ",line_new);      
     if case_modify.search(line_new) :
        line_new=case_modify.sub(r"case (\1) ",line_new);         
     if endcase_modify.search(line_new) :
        line_new=endcase_modify.sub(r" endcase ",line_new);
     if other_default0.search(line_new) :
        line_new=other_default0.sub(r" `0 ",line_new);
     if other_default1.search(line_new) :
        line_new=other_default1.sub(r" `1 ",line_new);
     if others_modify.search(line_new) :
        line_new=others_modify.sub(r"default : ",line_new);        
     if elsif_modify.search(line_new) :
        line_new=elsif_modify.sub(r"end else if ",line_new); 
     if then_modify.search(line_new) :
        line_new=then_modify.sub(r" begin ",line_new); 
     if end_if.search(line_new) :
        line_new=end_if.sub(r" end ",line_new); 
     if if_bracket_del.search(line_new) :
        line_new=if_bracket_del.sub(r"if \1 begin",line_new);  
     if if_brackets.search(line_new) :
        line_new=if_brackets.sub(r"if (\1) begin",line_new); 
     if if_brackets_end.search(line_new) :
        line_new=if_brackets_end.sub(r"if (\1)",line_new); 
     if if_equal_modify.search(line_new) :
        line_new=if_equal_modify.sub(r"\1 == \2",line_new);         
     if function_start.search(line_new) :
        function_tag=1;
     if function_tag and end_tag.search(line_new) :
        line_new=end_tag.sub(r' end \n endfunction ',line_new);
     if var_modfiy.search(line_new) :
        line_new=var_modfiy.sub(r"\2 \1;",line_new);   
     if function_in_var.search(line_new) :
        line_new=function_in_var.sub(r"input \2 \1 \3",line_new);
     if function_out_var.search(line_new) :
        line_new=function_out_var.sub(r"output \2 \1 \3",line_new);
     if sel_vec_modi.search(line_new) :
        line_new=sel_vec_modi.sub(r"\1[\2]",line_new);
     if fun_var_def.search(line_new) :
        line_new=fun_var_def.sub(r"\2 \1 \3",line_new);
     if return_del.search(line_new) :
        line_new=return_del.sub(r" ",line_new);
     if fun_logic_define.search(line_new) :
        line_new=fun_logic_define.sub(r"\2 \1",line_new);
     if del_is.search(line_new) :
        line_new=del_is.sub(r";\n",line_new); 
     if end_generate_modi.search(line_new) :
        line_new=end_generate_modi.sub(r"end\n   endgenerate",line_new);
     if start_generate_if_modi.search(line_new) :
        line_new=start_generate_if_modi.sub(r"generate\n if (\2 ) :\1 \n begin",line_new);
     if start_generate_for_modi.search(line_new) :
        line_new=start_generate_for_modi.sub(r"generate\n for (\2 ) :\1 \n begin",line_new);     
     fnew.write(line_new);
fold.close();
fnew.close();